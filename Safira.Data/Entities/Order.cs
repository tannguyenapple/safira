﻿namespace Safira.Data.Entities
{
    using Safira.Domain.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(50)]
        public string OrderNumber { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public short StatusId { get; set; }

        public string Note { get; set; }

        public DateTime CreatedDate { get; set; }

        //[ForeignKey("UserId")]
        //public ApplicationUser User { get; set; }

        [InverseProperty("Order")]
        public ICollection<Cart> Cart { get; set; }

        [InverseProperty("Order")]
        public EmailHistory EmailHistory { get; set; }
    }
}
