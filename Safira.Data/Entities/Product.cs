﻿namespace Safira.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Product
    {
        public Product()
        {
            ProductImage = new HashSet<ProductImage>();
            Review = new HashSet<Review>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Proccessing { get; set; }

        public string Useful { get; set; }

        public int CategoryId { get; set; }

        public int? OldPrice { get; set; }

        public int Price { get; set; }

        public short StatusId { get; set; }

        public short ProductType { get; set; }

        public DateTime? SaleUp {get; set; }

        public short Order { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

        [InverseProperty("Product")]
        public ICollection<ProductImage> ProductImage { get; set; }

        [InverseProperty("Product")]
        public ICollection<Review> Review { get; set; }
    }
}
