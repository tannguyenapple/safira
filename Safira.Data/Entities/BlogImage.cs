﻿namespace Safira.Data.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class BlogImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long BlogId { get; set; }

        public long PictureId { get; set; }

        [ForeignKey("BlogId")]
        [InverseProperty("BlogImage")]
        public Blog Blog { get; set; }

        [ForeignKey("PictureId")]
        public Picture Picture { get; set; }
    }
}
