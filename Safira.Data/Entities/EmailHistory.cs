﻿namespace Safira.Data.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class EmailHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(256)]
        public string Subject { get; set; }

        public string Content { get; set; }

        public short StatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        [MaxLength(200)]
        public string RecipientName { get; set; }

        [MaxLength(200)]
        public string RecipientEmail { get; set; }

        public int OrderId { get; set; }

        [ForeignKey("OrderId")]
        [InverseProperty("EmailHistory")]
        public Order Order { get; set; }
    }
}
