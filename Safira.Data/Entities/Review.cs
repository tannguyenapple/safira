﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Safira.Data.Entities
{
    public class Review
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public short Rating { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }   

        public short Status { get; set; }

        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        [InverseProperty("Review")]
        public Product Product { get; set; }
    }
}
