﻿namespace Safira.Data.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Picture
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        //public byte[] Binary { get; set; }

        public string MimeType { get; set; }

        public string SeoFileName { get; set; }

        public string AltAttribute { get; set; }

        public string TitleAttribute { get; set; }
    }
}
