﻿namespace Safira.Data.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ProductImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long ProductId { get; set; }

        public long PictureId { get; set; }

        [ForeignKey("ProductId")]
        [InverseProperty("ProductImage")]
        public Product Product { get; set; }

        [ForeignKey("PictureId")]
        public Picture Picture { get; set; }
    }
}
