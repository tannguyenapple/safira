﻿namespace Safira.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Blog
    {
        public Blog()
        {
            BlogImage = new HashSet<BlogImage>();
            BlogReview = new HashSet<BlogReview>();
        }

        public long Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string TitleUrl { get; set; }

        public string Description { get; set; }

        public short StatusId { get; set; }

        public short BlogType { get; set; }

        public short Order { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        [InverseProperty("Blog")]
        public ICollection<BlogImage> BlogImage { get; set; }

        [InverseProperty("Blog")]
        public ICollection<BlogReview> BlogReview { get; set; }
    }
}
