﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Safira.Data.Repository;
using Microsoft.EntityFrameworkCore;

namespace Safira.Data.UnitOfWork
{
    public class UnitOfWork<TContext> :  IUnitOfWork
        where TContext : DbContext
	{
        private Dictionary<Type, object> _repositories;
		private SafiraContext Context { get; }

		public UnitOfWork(SafiraContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }		

		public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (_repositories == null) _repositories = new Dictionary<Type, object>();

            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type)) _repositories[type] = new EfRepository<TEntity>(Context);
            return (IRepository<TEntity>)_repositories[type];
        }        

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Context?.Dispose();
        }
    }
}
