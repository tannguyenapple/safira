﻿using System;
using System.Threading.Tasks;
using Safira.Data.Repository;

namespace Safira.Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
	{
		IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

		int SaveChanges();

		Task<int> SaveChangesAsync();
	}
}
