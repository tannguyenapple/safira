﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Safira.Domain.Const;
using Safira.Domain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.Data
{
    public partial class SafiraContextSeed
    {
        public static async Task SeedAsync(SafiraContext context, UserManager<ApplicationUser> userManager)
        {
            try
            {
                AddRoles(context);
                AddUsers(context, userManager);
                await context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                var temp = e.Message;
            }
        }

        private static void AddRoles(SafiraContext context)
        {
            if (!context.Roles.Any(t => t.Name.Equals(MembershipRole.Admin)))
            {
                context.Roles.AddRange(new List<IdentityRole>()
                {
                    new IdentityRole()
                    {
                        Id = "8314d7d8-ec91-4c54-8c56-4111cea43b50",
                        Name = MembershipRole.Admin,
                        NormalizedName = MembershipRole.Admin.ToUpper()
                    },
                    new IdentityRole()
                    {
                        Id = "443a8d39-e032-41c1-94ef-7e25045dd4bd",
                        Name = MembershipRole.Supplier,
                        NormalizedName = MembershipRole.Supplier.ToUpper()
                    },
                    new IdentityRole()
                    {
                        Id = "49c1f897-b1b8-44b3-8133-73c1fd812577",
                        Name = MembershipRole.Customer,
                        NormalizedName = MembershipRole.Customer.ToUpper()
                    }
                });
            }
        }

        private static void AddUsers(SafiraContext context, UserManager<ApplicationUser> userManager)
        {
            var admin = "admin@doque.com";
            var adminUser = context.Users.FirstOrDefault(t => admin.Equals(t.Email));
            if (adminUser == null)
            {
                var now = DateTime.UtcNow;
                var user = new ApplicationUser
                {
                    UserName = admin,
                    Email = admin,
                    FirstName = "Tan",
                    LastName = "Nguyen",
                    CreateDate = now,
                    ModifyDate = now,
                    StatusId = 1

                };
                var result = userManager.CreateAsync(user, "pa$$w0rd").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, MembershipRole.Admin).Wait();
                }
            }

            var cs = "customer@gmail.com";
            var customer = context.Users.FirstOrDefault(t => cs.Equals(t.Email));
            if (customer == null)
            {
                var now = DateTime.UtcNow;
                customer = new ApplicationUser
                {
                    UserName = cs,
                    Email = cs,
                    FirstName = "Tan",
                    LastName = "Nguyen",
                    CreateDate = now,
                    ModifyDate = now,
                    StatusId = 1

                };
                var result = userManager.CreateAsync(customer, "pa$$w0rd").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(customer, MembershipRole.Customer).Wait();
                }
            }
        }

        private static void ExecuteSqlUpdate(SafiraContext context)
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "wwwroot", "data", "scripts", "inventory");
            if (Directory.Exists(path))
            {
                var sqlFiles = Directory.GetFiles(path, "*.sql").OrderBy(x => x);
                foreach (var file in sqlFiles)
                {
                    context.Database.ExecuteSqlCommand(File.ReadAllText(file));
                }
            }
        }
    }
}
