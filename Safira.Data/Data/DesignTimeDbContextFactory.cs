﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Safira.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SafiraContext>
    {
        public SafiraContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<SafiraContext>();

            var connectionString = configuration.GetConnectionString("safira_connection");

            builder.UseSqlServer(connectionString, x => x.MigrationsAssembly("Safira.Data"));

            return new SafiraContext(builder.Options);
        }
    }
}
