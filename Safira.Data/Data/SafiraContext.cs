﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Safira.Data.Entities;
using Safira.Domain.Models;

namespace Safira.Data
{
    public partial class SafiraContext : IdentityDbContext<ApplicationUser>
    {
        public SafiraContext(DbContextOptions<SafiraContext> options)
            : base(options)
        { }

        public virtual DbSet<Category> Category { get; set; }

        public virtual DbSet<Product> Product { get; set; }

        public virtual DbSet<ProductImage> ProductImage { get; set; }

        public virtual DbSet<Picture> Picture { get; set; }

        public virtual DbSet<Cart> Cart { get; set; }

        public virtual DbSet<Wish> Wish { get; set; }

        public virtual DbSet<Order> Order { get; set; }

        public virtual DbSet<Message> Message { get; set; }

        public virtual DbSet<Review> Review { get; set; }

        public virtual DbSet<BlogType> BlogType { get; set; }

        public virtual DbSet<Blog> Blog { get; set; }

        public virtual DbSet<BlogReview> BlogReview { get; set; }

        public virtual DbSet<BlogImage> BlogImage { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            CreateIdentityModel(modelBuilder);
        }

        private void CreateIdentityModel(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //// Customize the ASP.NET Identity model and override the defaults if needed.
            //// For example, you can rename the ASP.NET Identity table names and more.
            //// Add your customizations after calling base.OnModelCreating(builder);
            modelBuilder.Entity<ApplicationUser>(entity => { entity.ToTable(name: "Users"); });
            modelBuilder.Entity<IdentityRole>(entity => { entity.ToTable(name: "UserGroupRoles"); });
            modelBuilder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("UserRoles"); });
            modelBuilder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("UserClaims"); });
            modelBuilder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("UserLogins"); });
            modelBuilder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("UserGroupRoleClaims"); });
            modelBuilder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("UserTokens"); });
        }
    }
}
