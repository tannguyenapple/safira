﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Safira.Data.Data.Migrations
{
    public partial class Add_EmailHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrderNumber",
                table: "Order",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EmailHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subject = table.Column<string>(maxLength: 256, nullable: true),
                    Content = table.Column<string>(nullable: true),
                    StatusId = table.Column<short>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    RecipientName = table.Column<string>(maxLength: 200, nullable: true),
                    RecipientEmail = table.Column<string>(maxLength: 200, nullable: true),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailHistory_Order_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailHistory_OrderId",
                table: "EmailHistory",
                column: "OrderId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailHistory");

            migrationBuilder.DropColumn(
                name: "OrderNumber",
                table: "Order");
        }
    }
}
