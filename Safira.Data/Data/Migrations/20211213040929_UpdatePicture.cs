﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Safira.Data.Data.Migrations
{
    public partial class UpdatePicture : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Binary",
                table: "Picture");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Binary",
                table: "Picture",
                type: "varbinary(max)",
                nullable: true);
        }
    }
}
