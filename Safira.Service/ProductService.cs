﻿namespace Safira.Service
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;
    using OMH.API.Core.Common;
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.Caching;
    using Safira.Domain.Const;
    using Safira.Domain.Enums;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Product;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;
        private readonly IFileService _fileService;
        private readonly ICacheManager _cacheManager;

        public ProductService(
            IUnitOfWork unitOfWork,
            IFileService fileService,
            ICacheManager cacheManager,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _fileService = fileService;
            _cacheManager = cacheManager;
            _messages = messages;
        }

        public async Task<ProductModel> GetByIdAsync(long id)
        {
            var product = await _unitOfWork.GetRepository<Product>().Table//.GetByIdAsync(id);
                .Include(t => t.ProductImage)
                .Include(t => t.Review)
                .FirstOrDefaultAsync(t => t.Id == id);

            if (product != null)
            {
                //var images = _unitOfWork.GetRepository<ProductImage>().Table
                //    .Include(t => t.Picture)
                //    .Where(t => t.ProductId == product.Id)
                //    .Select(t => new FileModel
                //    {
                //        Id = t.PictureId,
                //        SeoFileName = t.Picture.SeoFileName
                //    }).ToList();
                return new ProductModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    Processing = product.Proccessing,
                    Useful = product.Useful,
                    CategoryId = product.CategoryId,
                    StatusId = (StatusEnum)product.StatusId,
                    TypeId = (ProductTypeEnum)product.ProductType,
                    Price = product.Price,
                    OldPrice = product.OldPrice,
                    SaleUp = product.SaleUp,
                    Files = product.ProductImage.Select(t => new FileModel
                    {
                        Id = t.PictureId
                    }).ToList(),
                    Reviews = product.Review.Select(t => new ReviewModel
                    {
                        FullName = t.FullName,
                        Email = t.Email,
                        Content = t.Content,
                        Rating = t.Rating,
                        CreatedDate = t.CreatedDate
                    }).ToList(),
                    ImageUrls = product.ProductImage.Select(t => GetPictureUrl(t.PictureId)).ToList()
                };
            }
            return null;
        }

        public async Task<IPagedList<ProductModel>> GetListAsync(ProductFilteringModel filtering, bool isAdmin = false)
        {
            if (isAdmin)
            {
                var rawQuery = _unitOfWork.GetRepository<Product>().Table;

                if (filtering.CategoryId > 0)
                {
                    rawQuery = rawQuery.Where(t => t.CategoryId == filtering.CategoryId);
                }
                if (!string.IsNullOrEmpty(filtering.SearchKeyword))
                {
                    string keyword = filtering.SearchKeyword.ToLower();
                    rawQuery = rawQuery.Where(t => t.Name.ToLower().Contains(keyword)
                                    || t.Description.ToLower().Contains(keyword));
                }

                var query = rawQuery.Select(t => new ProductModel
                {
                    Id = t.Id,
                    Name = t.Name,
                    Description = t.Description,
                    Processing = t.Proccessing,
                    Useful = t.Useful,
                    CategoryId = t.CategoryId,
                    StatusId = (StatusEnum)t.StatusId,
                    TypeId = (ProductTypeEnum)t.ProductType,
                    Price = t.Price,
                    OldPrice = t.OldPrice,
                    SaleUp = t.SaleUp,
                });

                //Set default sort;
                if (!string.IsNullOrEmpty(filtering.SortBy))
                {
                    query = query.GenericSort(filtering.SortBy, filtering.SortDirection);
                }
                else
                {
                    //query = query.OrderByDescending(s => s.Id);
                }

                var products = await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
                var ids = products.Select(t => t.Id).ToList();
                var images = _unitOfWork.GetRepository<ProductImage>().Table
                    .Where(t => ids.Contains(t.ProductId))
                    .ToList();
                foreach (var item in products)
                {
                    item.Files = images.Where(t => t.ProductId == item.Id)
                        .Select(t => new FileModel
                        {
                            Id = t.PictureId
                        }).ToList();
                    item.ImageUrls = images.Where(t => t.ProductId == item.Id).Select(t => GetPictureUrl(t.PictureId)).ToList();
                }

                return products;
            }
            else
            {
                var rawProduct = await GetProductsFromCache();
                if (filtering.CategoryId > 0)
                {
                    rawProduct = rawProduct.Where(t => t.CategoryId == filtering.CategoryId).ToList();
                }
                if (!string.IsNullOrEmpty(filtering.SearchKeyword))
                {
                    string keyword = filtering.SearchKeyword.ToLower();
                    rawProduct = rawProduct.Where(t => t.Name.ToLower().Contains(keyword)
                                    || t.Description.ToLower().Contains(keyword)).ToList();
                }
                return rawProduct.ToPagedList(filtering.PageNumber, filtering.PageSize);
            }
        }

        public async Task<List<ProductModel>> GetBriefAllAsync()
        {
            return (await GetProductsFromCache()).Select(t => new ProductModel
            {
                Id = t.Id,
                Name = t.Name,
                Price = t.Price
            }).ToList();
        }

        public async Task<List<ProductModel>> GetAllAsync(int category = 0, string keyword = "")
        {
            var rawProduct = await GetProductsFromCache();
            if (category != 0)
            {
                rawProduct = rawProduct.Where(t => t.CategoryId == category).ToList();
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                rawProduct = rawProduct.Where(t => t.Name.ToLower().Contains(keyword)
                                || t.Description.ToLower().Contains(keyword)).ToList();
            }
            return rawProduct;

            //var rawQuery = _unitOfWork.GetRepository<Product>().Table
            //    .Include(t => t.ProductImage)
            //    .Where(t => t.StatusId == (short)StatusEnum.Active);

            //var query = rawQuery.Select(t => new ProductModel
            //{
            //    Id = t.Id,
            //    Name = t.Name,
            //    Description = t.Description,
            //    Processing = t.Proccessing,
            //    Useful = t.Useful,
            //    CategoryId = t.CategoryId,
            //    StatusId = (StatusEnum)t.StatusId,
            //    TypeId = (ProductTypeEnum)t.ProductType,
            //    Price = t.Price,
            //    OldPrice = t.OldPrice,
            //    SaleUp = t.SaleUp,
            //    Files = t.ProductImage.Select(i => new FileModel
            //    {
            //        Id = i.PictureId
            //    }).ToList()
            //});

            //if (category != 0)
            //{
            //    query = query.Where(t => t.CategoryId == category);
            //}

            //if (!string.IsNullOrEmpty(keyword))
            //{
            //    query = query.Where(t => t.Name.ToLower().Contains(keyword)
            //                    || t.Description.ToLower().Contains(keyword));
            //}

            //return await query.ToListAsync();
        }

        public async Task<Result<long>> SaveAsync(ProductModel model)
        {
            var result = new Result<long>();
            if (model.Id > 0)
            {
                result = await EditAsync(model);
            }
            else
            {
                result = await CreateAsync(model);
            }
            if (result.ResultType == ResultType.Success)
                RefreshCache();
            return result;
        }

        public async Task<Result<long>> DeleteAsync(long id)
        {
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var product = await _unitOfWork.GetRepository<Product>().GetByIdAsync(id);
            if (product != null)
            {
                var productImages = await _unitOfWork.GetRepository<ProductImage>().Table
                    .Where(t => t.ProductId == product.Id)
                    .ToListAsync();
                if (productImages.Any())
                {
                    _unitOfWork.GetRepository<ProductImage>().DeleteRange(productImages);
                    foreach (var image in productImages)
                    {
                        await _fileService.DeleteAsync(image.PictureId);
                    }
                }
                //Product.Status = (short)StatusEnum.Delete;
                _unitOfWork.GetRepository<Product>().Delete(product);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Product_Successful;
                result.Value = product.Id;
            }

            if (result.ResultType == ResultType.Success)
                RefreshCache();
            return result;
        }

        #region Utilities
        private string GetPictureUrl(long id)
        {
            string fileName = $"{id:0000000}_0.jpeg";
            return $"/images/{fileName}";
        }

        private async Task<Result<long>> CreateAsync(ProductModel model)
        {
            var now = DateTime.UtcNow;
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };
            var product = new Product
            {
                Name = model.Name,
                Description = model.Description,
                Proccessing = model.Processing,
                Useful = model.Useful,
                CategoryId = model.CategoryId,
                ProductType = (short)model.TypeId,
                StatusId = (short)model.StatusId,
                SaleUp = model.SaleUp,
                Price = (int)model.Price,
                OldPrice = (int?)model.OldPrice,
                CreatedDate = now,
                ModifiedDate = now
            };
            _unitOfWork.GetRepository<Product>().Add(product);
            _unitOfWork.SaveChanges();

            if (model.Files.Any())
            {
                var producImages = model.Files.Select(t => new ProductImage()
                {
                    ProductId = product.Id,
                    PictureId = t.Id
                }).ToList();
                _unitOfWork.GetRepository<ProductImage>().AddRange(producImages);
                await _unitOfWork.SaveChangesAsync();
            }

            result.ResultType = ResultType.Success;
            result.Message = _messages.Added_Product_Successful;
            result.Value = product.Id;
            return result;
        }

        private async Task<Result<long>> EditAsync(ProductModel model)
        {
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };
            var product = await _unitOfWork.GetRepository<Product>().GetByIdAsync(model.Id);
            if (product != null)
            {
                var now = DateTime.UtcNow;
                product.Name = model.Name;
                product.Name = model.Name;
                product.Description = model.Description;
                product.Proccessing = model.Processing;
                product.Useful = model.Useful;
                product.CategoryId = model.CategoryId;
                product.ProductType = (short)model.TypeId;
                product.StatusId = (short)model.StatusId;
                product.SaleUp = model.SaleUp;
                product.Price = (int)model.Price;
                product.OldPrice = (int?)model.OldPrice;
                product.ModifiedDate = now;
                _unitOfWork.GetRepository<Product>().Update(product);
                await _unitOfWork.SaveChangesAsync();

                var productImages = _unitOfWork.GetRepository<ProductImage>().Table
                    .Where(t => t.ProductId == product.Id)
                    .ToList();

                //Update product image
                var newItems = (from nItem in model.Files
                                join oItem in productImages on nItem.Id equals oItem.PictureId into existItem
                                from eItem in existItem.DefaultIfEmpty()
                                where eItem == null
                                select nItem).ToList();

                //var updateItems = (from oItem in productImages
                //                   join nItem in model.Files on oItem.PictureId equals nItem.Id 
                //                   select oItem).ToList();

                var deleteItems = (from oItem in productImages
                                   join nItem in model.Files on oItem.PictureId equals nItem.Id into existItem
                                   from eItem in existItem.DefaultIfEmpty()
                                   where eItem == null
                                   select oItem).ToList();

                if (deleteItems.Any())
                {
                    _unitOfWork.GetRepository<ProductImage>().DeleteRange(deleteItems);
                    foreach (var item in deleteItems)
                    {
                        await _fileService.DeleteAsync(item.PictureId);
                    }
                }

                if (newItems.Any())
                {
                    _unitOfWork.GetRepository<ProductImage>().AddRange(newItems.Select(t => new ProductImage
                    {
                        ProductId = product.Id,
                        PictureId = t.Id
                    }).ToList());
                }

                if (deleteItems.Any() || newItems.Any())
                {
                    await _unitOfWork.SaveChangesAsync();
                }

                result.ResultType = ResultType.Success;
                result.Message = _messages.Updated_Product_Successful;
                result.Value = product.Id;
            }
            return result;
        }

        private async Task<List<ProductModel>> GetProductsFromCache()
        {
            var products = await _cacheManager.Get(CacheKeys.PRODUCTS_LIST, async () =>
            {
                var products = await _unitOfWork.GetRepository<Product>().Table
                    .Where(t => t.StatusId == (short)StatusEnum.Active)
                    .OrderBy(t => t.Order)
                    .Select(t => new ProductModel
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Description = t.Description,
                        Processing = t.Proccessing,
                        Useful = t.Useful,
                        CategoryId = t.CategoryId,
                        StatusId = (StatusEnum)t.StatusId,
                        TypeId = (ProductTypeEnum)t.ProductType,
                        Price = t.Price,
                        OldPrice = t.OldPrice,
                        SaleUp = t.SaleUp,
                        //Files = t.ProductImage.Select(i => new FileModel
                        //{
                        //    Id = i.PictureId
                        //}).ToList()
                    })
                    .ToListAsync();
                var images = _unitOfWork.GetRepository<ProductImage>().Table.ToList();
                foreach(var product in products)
                {
                    product.ImageUrls = images.Where(t => t.ProductId == product.Id)
                                            .Select(t => GetPictureUrl(t.PictureId))
                                            .ToList();
                }
                return products;
            });

            return products;
        }

        private void RefreshCache()
        {
            _cacheManager.Remove(CacheKeys.PRODUCTS_LIST);
            _cacheManager.Remove(CacheKeys.PICTURE_LIST);
        }

        #endregion
    }
}
