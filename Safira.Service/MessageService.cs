﻿namespace Safira.Service
{
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Home;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;

        public MessageService(
            IUnitOfWork unitOfWork,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _messages = messages;
        }

        public async Task<MessageViewModel> GetByIdAsync(int id)
        {
            var message = await _unitOfWork.GetRepository<Message>().GetByIdAsync(id);
            if(message != null)
            {
                return new MessageViewModel
                {
                    FullName = message.FullName,
                    Email = message.Email,
                    Subject = message.Subject,
                    Content = message.Content
                };
            }
            return null;
        }

        public async Task<IPagedList<MessageViewModel>> GetListAsync(MessageFilteringModel filtering)
        {
            var rawQuery = _unitOfWork.GetRepository<Message>().Table;

            var query = rawQuery.Select(t => new MessageViewModel
            {
                FullName = t.FullName,
                Email = t.Email,
                Subject = t.Subject,
                Content = t.Content
            });

            return await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
        }

        public async Task<Result<int>> SaveAsync(MessageViewModel model)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            if (model.Id > 0)
            {
                var message = await _unitOfWork.GetRepository<Message>().GetByIdAsync(model.Id);
                if (message != null)
                {
                    message.FullName = model.FullName;
                    message.Email = model.Email;
                    message.Subject = model.Subject;
                    message.Content = model.Content;
                    _unitOfWork.GetRepository<Message>().Update(message);
                    _unitOfWork.SaveChanges();
                    result.ResultType = ResultType.Success;
                    result.Message = _messages.Updated_Message_Successful;
                    result.Value = message.Id;
                }
            }
            else
            {
                var message = new Message
                {
                    FullName = model.FullName,
                    Email = model.Email,
                    Subject = model.Subject,
                    Content = model.Content,
                    CreatedDate = DateTime.UtcNow
                };
                _unitOfWork.GetRepository<Message>().Add(message);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Added_Message_Successful;
                result.Value = message.Id;
            }

            return result;
        }

        public async Task<Result<int>> DeleteAsync(int id)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var message = await _unitOfWork.GetRepository<Message>().GetByIdAsync(id);
            if (message != null)
            {
                _unitOfWork.GetRepository<Message>().Delete(message);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Message_Successful;
                result.Value = message.Id;
            }

            return result;
        }
    }
}
