﻿using Microsoft.EntityFrameworkCore;
using Safira.Data.Entities;
using Safira.Data.UnitOfWork;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Order;
using Safira.Domain.Models.Product;
using Safira.Domain.PagedList;
using Safira.Domain.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;

        public OrderService(
            IUnitOfWork unitOfWork,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _messages = messages;
        }

        public async Task<CheckoutViewModel> GetByIdAsync(int id)
        {
            var order = await _unitOfWork.GetRepository<Order>().Table
                .Include(t => t.Cart).ThenInclude(t => t.Product)
                .FirstOrDefaultAsync(t => t.Id == id);

            if (order != null)
            {
                return new CheckoutViewModel
                {
                    Id = order.Id,
                    FullName = order.FullName,
                    Email = order.Email,
                    PhoneNumber = order.PhoneNumber,
                    Address = order.Address,
                    Note = order.Note,
                    StatusId = (OrderStatusEnum)order.StatusId,
                    CreatedDate = order.CreatedDate,
                    OrderNumber = order.OrderNumber,
                    Products = order.Cart.Select(t => new ProductCartViewModel
                    {
                        ProductId = t.ProductId,
                        Name = t.Product.Name,
                        Price = t.Product.Price,
                        Quantity = t.Quantity
                    }).ToList()
                };
            }
            return null;
        }

        public async Task<IPagedList<CheckoutViewModel>> GetListAsync(OrderFilteringModel filtering)
        {
            var rawQuery = _unitOfWork.GetRepository<Order>().Table
                .Include(t => t.Cart).ThenInclude(t => t.Product)
                .Where(t => filtering.StatusId == 0 || t.StatusId == filtering.StatusId)
                .OrderByDescending(t => t.CreatedDate);

            var query = rawQuery.Select(t => new CheckoutViewModel
            {
                Id = t.Id,
                FullName = t.FullName,
                Email = t.Email,
                PhoneNumber = t.PhoneNumber,
                Address = t.Address,
                Note = t.Note,
                StatusId = (OrderStatusEnum)t.StatusId,
                CreatedDate = t.CreatedDate,
                Products = t.Cart.Select(t => new Domain.Models.Product.ProductCartViewModel
                {
                    ProductId = t.ProductId,
                    Name = t.Product.Name,
                    Price = t.Product.Price,
                    Quantity = t.Quantity
                }).ToList()
            });

            return await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
        }

        public async Task<Result<int>> SaveOrder(CheckoutViewModel model)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };
            if (model.Products.Any())
            {
                try
                {
                    var now = DateTime.UtcNow;
                    if (model.Id > 0)
                    {
                        var order = _unitOfWork.GetRepository<Order>().Table
                            .Include(t => t.Cart)
                            .FirstOrDefault(t => t.Id == model.Id);
                        if(order != null)
                        {
                            order.FullName = model.FullName;
                            order.Email = model.Email;
                            order.StatusId = (short)model.StatusId;
                            order.PhoneNumber = model.PhoneNumber;
                            order.Address = model.Address;
                            order.Note = model.Note;

                            var userId = order.Cart.FirstOrDefault()?.UserId;
                            UpdateCart(order.Id, userId, order.Cart.ToList(), model.Products);
                            //_unitOfWork.GetRepository<Cart>().DeleteRange(order.Cart.ToList());
                            //_unitOfWork.GetRepository<Cart>().AddRange(model.Products.Select(t => new Cart
                            //{
                            //    ProductId = t.ProductId,
                            //    Quantity = t.Quantity,
                            //    UserId = userId,
                            //    CreatedDate = now,
                            //    OrderId = order.Id
                            //}).ToList());
                            _unitOfWork.GetRepository<Order>().Update(order);
                            await _unitOfWork.SaveChangesAsync();

                            result.ResultType = ResultType.Success;
                            result.Message = _messages.Order_Successul;
                            result.Value = order.Id;
                        }
                    }
                    else
                    {
                        //Create order
                        var order = new Order
                        {
                            Email = model.Email,
                            FullName = $"{model.FirstName} {model.LastName}",
                            Address = model.Address,
                            PhoneNumber = model.PhoneNumber,
                            Note = model.Note,
                            StatusId = (short)OrderStatusEnum.New,
                            CreatedDate = now
                        };

                        _unitOfWork.GetRepository<Order>().Add(order);
                        await _unitOfWork.SaveChangesAsync();

                        order.OrderNumber = $"CQH{now.ToString("yyyyMMdd")}{order.Id}";
                        _unitOfWork.GetRepository<Order>().Update(order);
                        await _unitOfWork.SaveChangesAsync();

                        var user = _unitOfWork.GetRepository<ApplicationUser>().Table.FirstOrDefault(t => t.Email == model.Email);
                        var carts = new List<Cart>();
                        if (user != null)
                        {
                            carts = _unitOfWork.GetRepository<Cart>().Table
                                .Where(t => t.UserId == user.Id && !t.OrderId.HasValue)
                                .ToList();

                            var validateCart = (from cart in carts
                                                join prd in model.Products on new { cart.UserId, cart.ProductId } equals new { UserId = user.Id, prd.ProductId }
                                                where !cart.OrderId.HasValue
                                                select cart).ToList();

                            if (validateCart.Count != model.Products.Count)
                            {
                                result.Message = _messages.Email_Have_Been_Exist;
                                return result;
                            }
                        }
                        if (carts.Any())
                        {
                            foreach (var item in carts)
                            {
                                item.OrderId = order.Id;
                            }
                            _unitOfWork.GetRepository<Cart>().UpdateRange(carts);
                        }
                        else
                        {
                            _unitOfWork.GetRepository<Cart>().AddRange(model.Products.Select(t => new Cart
                            {
                                ProductId = t.ProductId,
                                Quantity = t.Quantity,
                                //UserId = user.Id,
                                CreatedDate = now,
                                OrderId = order.Id
                            }).ToList());
                        }
                        await _unitOfWork.SaveChangesAsync();

                        result.ResultType = ResultType.Success;
                        result.Message = _messages.Order_Successul;
                        result.Value = order.Id;
                    }
                }
                catch (Exception e)
                {
                    var temp = e.Message;
                }
            }
            return result;
        }

        public async Task<Result<int>> DeleteAsync(int id)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var order = await _unitOfWork.GetRepository<Order>().GetByIdAsync(id);
            if (order != null)
            {
                order.StatusId = (short)OrderStatusEnum.Cancelled;
                _unitOfWork.GetRepository<Order>().Update(order);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Order_Successful;
                result.Value = order.Id;
            }

            return result;
        }

        private void UpdateCart(int orderId, string userId, List<Cart> carts, List<ProductCartViewModel> products)
        {
            var now = DateTime.UtcNow;
            var newItems = (from newItem in products
                            join oldItem in carts on new { newItem.ProductId } equals new { oldItem.ProductId } into existItems
                            from existItem in existItems.DefaultIfEmpty()
                            where existItem == null
                            select newItem).ToList();

            var updateItems = (from oldItem in carts
                               join newItem in products on new { oldItem.ProductId } equals new { newItem.ProductId }
                               select oldItem).ToList();

            var deleteItems = (from oldItem in carts
                               join newItem in products on new { oldItem.ProductId } equals new { newItem.ProductId } into existItems
                               from existItem in existItems.DefaultIfEmpty()
                               where existItem == null
                               select oldItem).ToList();

            if (newItems.Any())
            {
                _unitOfWork.GetRepository<Cart>().AddRange(newItems.Select(t => new Cart
                {
                    ProductId = t.ProductId,
                    Quantity = t.Quantity,
                    UserId = userId,
                    CreatedDate = now,
                    OrderId = orderId
                }).ToList());
            }

            if (updateItems.Any())
            {
                foreach (var updItem in updateItems)
                {
                    var newItem = products.FirstOrDefault(t => t.ProductId == updItem.ProductId);
                    updItem.Quantity = newItem.Quantity;
                }
                _unitOfWork.GetRepository<Cart>().UpdateRange(updateItems);
            }

            if (deleteItems.Any())
            {
                _unitOfWork.GetRepository<Cart>().DeleteRange(deleteItems);
            }
        }
    }
}
