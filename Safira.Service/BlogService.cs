﻿namespace Safira.Service
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;
    using OMH.API.Core.Common;
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.Caching;
    using Safira.Domain.Const;
    using Safira.Domain.Enums;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Blog;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class BlogService : IBlogService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;
        private readonly IFileService _fileService;
        private readonly ICacheManager _cacheManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public BlogService(
            IUnitOfWork unitOfWork,
            IFileService fileService,
            ICacheManager cacheManager,
            IHostingEnvironment hostingEnvironment,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _fileService = fileService;
            _cacheManager = cacheManager;
            _hostingEnvironment = hostingEnvironment;
            _messages = messages;
        }

        public async Task<List<SelectListItem>> GetSelectListAsync(string defaultVal = "")
        {
            var categories = (await GetCategoriesFromCache())
                .Select(i => new SelectListItem { Text = i.Name, Value = i.Id.ToString() })
                .ToList();
            if (!string.IsNullOrEmpty(defaultVal))
                categories.Insert(0, new SelectListItem(defaultVal, "0"));
            return categories;
        }

        private async Task<List<BlogTypeModel>> GetCategoriesFromCache()
        {
            var categories = await _cacheManager.Get(CacheKeys.BLOG_TYPE_LIST, async () =>
            {
                var result = await _unitOfWork.GetRepository<BlogType>().Table
                    .Select(category => new BlogTypeModel
                    {
                        Id = category.Id,
                        Name = category.Name
                    })
                    .ToListAsync();
                return result;
            });

            return categories;
        }

        public async Task<BlogModel> GetByIdAsync(long id)
        {
            var Blog = await _unitOfWork.GetRepository<Blog>().Table//.GetByIdAsync(id);
                .Include(t => t.BlogImage)
                    .ThenInclude(t=>t.Picture)
                .Include(t => t.BlogReview)
                .FirstOrDefaultAsync(t => t.Id == id);

            if (Blog != null)
            {
                //var images = _unitOfWork.GetRepository<BlogImage>().Table
                //    .Include(t => t.Picture)
                //    .Where(t => t.BlogId == Blog.Id)
                //    .Select(t => new FileModel
                //    {
                //        Id = t.PictureId,
                //        SeoFileName = t.Picture.SeoFileName
                //    }).ToList();
                return new BlogModel
                {
                    Id = Blog.Id,
                    Name = Blog.Name,
                    TitleUrl = Blog.TitleUrl,
                    Description = Blog.Description,
                    StatusId = (StatusEnum)Blog.StatusId,
                    BlogType = Blog.BlogType,
                    CreatedDate = Blog.CreatedDate,
                    Files = Blog.BlogImage.Select(t => new FileModel
                    {
                        Id = t.PictureId
                    }).ToList(),
                    Reviews = Blog.BlogReview.Select(t => new BlogReviewModel
                    {
                        FullName = t.FullName,
                        Email = t.Email,
                        Content = t.Content,
                        Rating = t.Rating,
                        CreatedDate = t.CreatedDate
                    }).ToList(),
                    ImageUrls = Blog.BlogImage.Select(t => GetPictureUrl(t.PictureId, t.Picture.MimeType)).ToList()
                };
            }
            return null;
        }

        public async Task<IPagedList<BlogModel>> GetListAsync(BlogFilteringModel filtering, bool isAdmin = false)
        {
            if (isAdmin)
            {
                var rawQuery = _unitOfWork.GetRepository<Blog>().Table;

                if (filtering.CategoryId > 0)
                {
                    rawQuery = rawQuery.Where(t => t.BlogType == filtering.CategoryId);
                }
                if (!string.IsNullOrEmpty(filtering.SearchKeyword))
                {
                    string keyword = filtering.SearchKeyword.ToLower();
                    rawQuery = rawQuery.Where(t => t.Name.ToLower().Contains(keyword)
                                    || t.Description.ToLower().Contains(keyword));
                }
                rawQuery = rawQuery.OrderByDescending(t => t.ModifiedDate);

                var query = rawQuery.Select(t => new BlogModel
                {
                    Id = t.Id,
                    Name = t.Name,
                    TitleUrl = t.TitleUrl,
                    Description = t.Description,
                    StatusId = (StatusEnum)t.StatusId,
                    BlogType = t.BlogType,
                    CreatedDate = t.CreatedDate
                });

                //Set default sort;
                if (!string.IsNullOrEmpty(filtering.SortBy))
                {
                    query = query.GenericSort(filtering.SortBy, filtering.SortDirection);
                }
                else
                {
                    //query = query.OrderByDescending(s => s.Id);
                }

                var Blogs = await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
                var ids = Blogs.Select(t => t.Id).ToList();
                var images = _unitOfWork.GetRepository<BlogImage>().Table
                    .Where(t => ids.Contains(t.BlogId))
                    .ToList();
                foreach (var item in Blogs)
                {
                    item.Files = images.Where(t => t.BlogId == item.Id)
                        .Select(t => new FileModel
                        {
                            Id = t.PictureId
                        }).ToList();
                }

                return Blogs;
            }
            else
            {
                var rawBlog = await GetBlogsFromCache();
                if (filtering.CategoryId > 0)
                {
                    rawBlog = rawBlog.Where(t => t.BlogType == filtering.CategoryId).ToList();
                }
                
                if (!string.IsNullOrEmpty(filtering.SearchKeyword))
                {
                    string keyword = filtering.SearchKeyword.ToLower();
                    rawBlog = rawBlog.Where(t => t.Name.ToLower().Contains(keyword)
                                    || t.Description.ToLower().Contains(keyword)).ToList();
                }
                rawBlog = rawBlog.OrderByDescending(t => t.ModifiedDate).ToList();
                return rawBlog.ToPagedList(filtering.PageNumber, filtering.PageSize);
            }
        }

        public async Task<List<BlogModel>> GetBriefAllAsync()
        {
            return (await GetBlogsFromCache()).Select(t => new BlogModel
            {
                Id = t.Id,
                Name = t.Name,
                TitleUrl = t.TitleUrl
            }).ToList();
        }

        public async Task<List<BlogModel>> GetAllAsync(int category = 0, string keyword = "")
        {
            var rawBlog = await GetBlogsFromCache();
            if (category != 0)
            {
                rawBlog = rawBlog.Where(t => t.BlogType == category).ToList();
            }

            if (!string.IsNullOrEmpty(keyword))
            {
                rawBlog = rawBlog.Where(t => t.Name.ToLower().Contains(keyword)
                                || t.Description.ToLower().Contains(keyword)).ToList();
            }
            return rawBlog;

            //var rawQuery = _unitOfWork.GetRepository<Blog>().Table
            //    .Include(t => t.BlogImage)
            //    .Where(t => t.StatusId == (short)StatusEnum.Active);

            //var query = rawQuery.Select(t => new BlogModel
            //{
            //    Id = t.Id,
            //    Name = t.Name,
            //    Description = t.Description,
            //    Processing = t.Proccessing,
            //    Useful = t.Useful,
            //    CategoryId = t.CategoryId,
            //    StatusId = (StatusEnum)t.StatusId,
            //    TypeId = (BlogTypeEnum)t.BlogType,
            //    Price = t.Price,
            //    OldPrice = t.OldPrice,
            //    SaleUp = t.SaleUp,
            //    Files = t.BlogImage.Select(i => new FileModel
            //    {
            //        Id = i.PictureId
            //    }).ToList()
            //});

            //if (category != 0)
            //{
            //    query = query.Where(t => t.CategoryId == category);
            //}

            //if (!string.IsNullOrEmpty(keyword))
            //{
            //    query = query.Where(t => t.Name.ToLower().Contains(keyword)
            //                    || t.Description.ToLower().Contains(keyword));
            //}

            //return await query.ToListAsync();
        }

        public async Task<List<BlogModel>> GetRecentAsync()
        {
            var rawBlog = await GetBlogsFromCache();

            rawBlog = rawBlog.OrderByDescending(t => t.CreatedDate).Take(6).ToList();

            return rawBlog;

            //var rawQuery = _unitOfWork.GetRepository<Blog>().Table
            //    .Include(t => t.BlogImage)
            //    .Where(t => t.StatusId == (short)StatusEnum.Active);

            //var query = rawQuery.Select(t => new BlogModel
            //{
            //    Id = t.Id,
            //    Name = t.Name,
            //    Description = t.Description,
            //    Processing = t.Proccessing,
            //    Useful = t.Useful,
            //    CategoryId = t.CategoryId,
            //    StatusId = (StatusEnum)t.StatusId,
            //    TypeId = (BlogTypeEnum)t.BlogType,
            //    Price = t.Price,
            //    OldPrice = t.OldPrice,
            //    SaleUp = t.SaleUp,
            //    Files = t.BlogImage.Select(i => new FileModel
            //    {
            //        Id = i.PictureId
            //    }).ToList()
            //});

            //if (category != 0)
            //{
            //    query = query.Where(t => t.CategoryId == category);
            //}

            //if (!string.IsNullOrEmpty(keyword))
            //{
            //    query = query.Where(t => t.Name.ToLower().Contains(keyword)
            //                    || t.Description.ToLower().Contains(keyword));
            //}

            //return await query.ToListAsync();
        }

        public async Task<Result<long>> SaveAsync(BlogModel model)
        {
            var result = new Result<long>();
            if (model.Id > 0)
            {
                result = await EditAsync(model);
            }
            else
            {
                result = await CreateAsync(model);
            }
            if (result.ResultType == ResultType.Success)
                RefreshCache();
            return result;
        }

        public async Task<Result<long>> DeleteAsync(long id)
        {
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var Blog = await _unitOfWork.GetRepository<Blog>().GetByIdAsync(id);
            if (Blog != null)
            {
                var BlogImages = await _unitOfWork.GetRepository<BlogImage>().Table
                    .Where(t => t.BlogId == Blog.Id)
                    .ToListAsync();
                if (BlogImages.Any())
                {
                    _unitOfWork.GetRepository<BlogImage>().DeleteRange(BlogImages);
                    foreach (var image in BlogImages)
                    {
                        await _fileService.DeleteAsync(image.PictureId);
                    }
                }
                //Blog.Status = (short)StatusEnum.Delete;
                _unitOfWork.GetRepository<Blog>().Delete(Blog);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Blog_Successful;
                result.Value = Blog.Id;
            }

            if (result.ResultType == ResultType.Success)
                RefreshCache();
            return result;
        }

        #region Utilities

        private async Task<Result<long>> CreateAsync(BlogModel model)
        {
            var now = DateTime.UtcNow;
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };
            var Blog = new Blog
            {
                Name = model.Name,
                TitleUrl = model.TitleUrl,
                Description = model.Description,
                BlogType = (short)model.BlogType,
                StatusId = (short)model.StatusId,
                CreatedDate = now,
                ModifiedDate = now
            };
            _unitOfWork.GetRepository<Blog>().Add(Blog);
            _unitOfWork.SaveChanges();

            if (model.Files.Any())
            {
                var producImages = model.Files.Select(t => new BlogImage()
                {
                    BlogId = Blog.Id,
                    PictureId = t.Id
                }).ToList();
                _unitOfWork.GetRepository<BlogImage>().AddRange(producImages);
                await _unitOfWork.SaveChangesAsync();
            }

            result.ResultType = ResultType.Success;
            result.Message = _messages.Added_Blog_Successful;
            result.Value = Blog.Id;
            return result;
        }

        private async Task<Result<long>> EditAsync(BlogModel model)
        {
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };
            var Blog = await _unitOfWork.GetRepository<Blog>().GetByIdAsync(model.Id);
            if (Blog != null)
            {
                var now = DateTime.UtcNow;
                Blog.Name = model.Name;
                Blog.Name = model.Name;
                Blog.TitleUrl = model.TitleUrl;
                Blog.Description = model.Description;
                Blog.BlogType = (short)model.BlogType;
                Blog.StatusId = (short)model.StatusId;
                Blog.ModifiedDate = now;
                _unitOfWork.GetRepository<Blog>().Update(Blog);
                await _unitOfWork.SaveChangesAsync();

                var BlogImages = _unitOfWork.GetRepository<BlogImage>().Table
                    .Where(t => t.BlogId == Blog.Id)
                    .ToList();

                //Update Blog image
                var newItems = (from nItem in model.Files
                                join oItem in BlogImages on nItem.Id equals oItem.PictureId into existItem
                                from eItem in existItem.DefaultIfEmpty()
                                where eItem == null
                                select nItem).ToList();

                //var updateItems = (from oItem in BlogImages
                //                   join nItem in model.Files on oItem.PictureId equals nItem.Id 
                //                   select oItem).ToList();

                var deleteItems = (from oItem in BlogImages
                                   join nItem in model.Files on oItem.PictureId equals nItem.Id into existItem
                                   from eItem in existItem.DefaultIfEmpty()
                                   where eItem == null
                                   select oItem).ToList();

                if (deleteItems.Any())
                {
                    _unitOfWork.GetRepository<BlogImage>().DeleteRange(deleteItems);
                    foreach (var item in deleteItems)
                    {
                        await _fileService.DeleteAsync(item.PictureId);
                    }
                }

                if (newItems.Any())
                {
                    _unitOfWork.GetRepository<BlogImage>().AddRange(newItems.Select(t => new BlogImage
                    {
                        BlogId = Blog.Id,
                        PictureId = t.Id
                    }).ToList());
                }

                if (deleteItems.Any() || newItems.Any())
                {
                    await _unitOfWork.SaveChangesAsync();
                }

                result.ResultType = ResultType.Success;
                result.Message = _messages.Updated_Blog_Successful;
                result.Value = Blog.Id;
            }
            return result;
        }

        private async Task<List<BlogModel>> GetBlogsFromCache()
        {
            var blogs = await _unitOfWork.GetRepository<Blog>().Table
                    .Where(t => t.StatusId == (short)StatusEnum.Active)
                    .OrderBy(t => t.Order)
                    .Select(t => new BlogModel
                    {
                        Id = t.Id,
                        Name = t.Name,
                        TitleUrl = t.TitleUrl,
                        Description = t.Description,
                        StatusId = (StatusEnum)t.StatusId,
                        BlogType = t.BlogType,
                        CreatedDate = t.CreatedDate,
                        ModifiedDate = t.ModifiedDate,
                        //Files = t.BlogImage.Select(i => new FileModel
                        //{
                        //    Id = i.PictureId
                        //}).ToList(),
                        //ImageUrls = t.BlogImage.Select(i => GetPictureUrl(i.Picture)).ToList()
                    }).ToListAsync();
            var images = await _unitOfWork.GetRepository<BlogImage>().Table
                .Include(t => t.Picture)
                .Select(t => new
                {
                    BlogId = t.BlogId,
                    PictureId = t.Picture.Id,
                    MimeType = t.Picture.MimeType
                        //ImgUrl = GetPictureUrl(t.Picture.Id, t.Picture.MimeType)
                    })
                .ToListAsync();
            foreach (var blog in blogs)
            {
                blog.ImageUrls = images.Where(t => t.BlogId == blog.Id).Select(t => GetPictureUrl(t.PictureId, t.MimeType)).ToList();
            }
            return blogs;
            /*
            var Blogs = await _cacheManager.Get(CacheKeys.BLOG_LIST, async () =>
            {
                var blogs = await _unitOfWork.GetRepository<Blog>().Table
                    .Where(t => t.StatusId == (short)StatusEnum.Active)
                    .OrderBy(t => t.Order)
                    .Select(t => new BlogModel
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Description = t.Description,
                        StatusId = (StatusEnum)t.StatusId,
                        BlogType = t.BlogType,
                        CreatedDate = t.CreatedDate,
                        ModifiedDate = t.ModifiedDate,
                        //Files = t.BlogImage.Select(i => new FileModel
                        //{
                        //    Id = i.PictureId
                        //}).ToList(),
                        //ImageUrls = t.BlogImage.Select(i => GetPictureUrl(i.Picture)).ToList()
                    }).ToListAsync();
                var images = await _unitOfWork.GetRepository<BlogImage>().Table
                    .Include(t => t.Picture)
                    .Select(t=>new
                    {
                        BlogId = t.BlogId,
                        ImgUrl = GetFileExtensionFromMimeType(t.Picture.MimeType)
                        //ImgUrl = GetPictureUrl(t.Picture.Id, t.Picture.MimeType)
                    })
                    .ToListAsync();
                foreach(var blog in blogs)
                {
                    blog.ImageUrls = images.Where(t => t.BlogId == blog.Id).Select(t => t.ImgUrl).ToList();
                }
                return blogs;
            });

            return Blogs;
            */
        }

        private string GetPictureUrl(long id, string mineType)
        {
            var lastPart = GetFileExtensionFromMimeType(mineType);
            string fileName = $"{id:0000000}_0.{lastPart}";
            return GetPictureLocalPath(fileName);
        }

        private string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            var parts = mimeType.Split('/');
            var lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }
            return lastPart;
        }

        private string GetPictureLocalPath(string fileName)
        {
            //return Path.Combine(_hostingEnvironment.WebRootPath, "images", fileName);
            return $"/images/{fileName}";
        }

        private void RefreshCache()
        {
            _cacheManager.Remove(CacheKeys.BLOG_LIST);
            _cacheManager.Remove(CacheKeys.PICTURE_LIST);
        }

        #endregion
    }
}
