﻿namespace Safira.Service
{
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.Enums;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Blog;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class BlogReviewService : IBlogReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;

        public BlogReviewService(
            IUnitOfWork unitOfWork,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _messages = messages;
        }

        public async Task<BlogReviewModel> GetByIdAsync(int id)
        {
            var BlogReview = await _unitOfWork.GetRepository<BlogReview>().GetByIdAsync(id);
            if(BlogReview != null)
            {
                return new BlogReviewModel
                {
                    FullName = BlogReview.FullName,
                    Email = BlogReview.Email,
                    Rating = BlogReview.Rating,
                    Content = BlogReview.Content,
                    BlogId = BlogReview.BlogId
                };
            }
            return null;
        }

        public async Task<IPagedList<BlogReviewModel>> GetListAsync(BlogReviewFilteringModel filtering)
        {
            var rawQuery = _unitOfWork.GetRepository<BlogReview>().Table;

            var query = rawQuery.Select(t => new BlogReviewModel
            {
                FullName = t.FullName,
                Email = t.Email,
                Rating = t.Rating,
                Content = t.Content,
                BlogId = t.BlogId,
                CreatedDate = t.CreatedDate
            });

            if(filtering.BlogId > 0)
            {
                query = query.Where(t => t.BlogId == filtering.BlogId);
            }

            return await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
        }

        public async Task<List<BlogReviewModel>> GetListAsync(long id)
        {
            return await _unitOfWork.GetRepository<BlogReview>().Table
                .Where(t => t.BlogId == id)
                .Select(t => new BlogReviewModel
                {
                    FullName = t.FullName,
                    Email = t.Email,
                    Rating = t.Rating,
                    Content = t.Content,
                    BlogId = t.BlogId,
                    CreatedDate = t.CreatedDate
                }).ToListAsync();
        }

        public async Task<List<BlogReviewModel>> GetRecentListAsync()
        {
            return await _unitOfWork.GetRepository<BlogReview>().Table
                .OrderByDescending(t=>t.CreatedDate)
                .Take(10)
                .Select(t => new BlogReviewModel
                {
                    FullName = t.FullName,
                    Email = t.Email,
                    Rating = t.Rating,
                    Content = t.Content,
                    BlogId = t.BlogId,
                    CreatedDate = t.CreatedDate
                }).ToListAsync();
        }

        public async Task<Result<int>> SaveAsync(BlogReviewModel model)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            if (model.Id > 0)
            {
                var BlogReview = await _unitOfWork.GetRepository<BlogReview>().GetByIdAsync(model.Id);
                if (BlogReview != null)
                {
                    BlogReview.FullName = model.FullName;
                    BlogReview.Email = model.Email;
                    BlogReview.Rating = model.Rating;
                    BlogReview.Content = model.Content;
                    BlogReview.Status = (short)model.Status;
                    _unitOfWork.GetRepository<BlogReview>().Update(BlogReview);
                    _unitOfWork.SaveChanges();
                    result.ResultType = ResultType.Success;
                    result.Message = _messages.Updated_Review_Successful;
                    result.Value = BlogReview.Id;
                }
            }
            else
            {
                var BlogReview = new BlogReview
                {
                    FullName = model.FullName,
                    Email = model.Email,
                    Rating = model.Rating,
                    Content = model.Content,
                    Status = (short)StatusEnum.Pending,
                    CreatedDate = DateTime.Now,
                    BlogId = model.BlogId
                };
                try
                {
                    _unitOfWork.GetRepository<BlogReview>().Add(BlogReview);
                    _unitOfWork.SaveChanges();
                }
                catch(Exception e)
                {
                    var temp = e.Message;
                }
                result.ResultType = ResultType.Success;
                result.Message = _messages.Added_Review_Successful;
                result.Value = BlogReview.Id;
            }

            return result;
        }

        public async Task<Result<int>> DeleteAsync(int id)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var BlogReview = await _unitOfWork.GetRepository<BlogReview>().GetByIdAsync(id);
            if (BlogReview != null)
            {
                _unitOfWork.GetRepository<BlogReview>().Delete(BlogReview);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Review_Successful;
                result.Value = BlogReview.Id;
            }

            return result;
        }
    }
}
