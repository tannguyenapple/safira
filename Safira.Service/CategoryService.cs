﻿namespace Safira.Service
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using OMH.API.Core.Common;
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.Caching;
    using Safira.Domain.Const;
    using Safira.Domain.Enums;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Category;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;
        private readonly ICacheManager _cacheManager;

        public CategoryService(
            IUnitOfWork unitOfWork,
            ICacheManager cacheManager,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _cacheManager = cacheManager;
            _messages = messages;
        }

        public async Task<List<SelectListItem>> GetSelectListAsync(string defaultVal = "")
        {
            var categories = (await GetCategoriesFromCache())
                .Select(i => new SelectListItem { Text = i.Name, Value = i.Id.ToString() })
                .ToList();
            if (!string.IsNullOrEmpty(defaultVal))
                categories.Insert(0, new SelectListItem(defaultVal, "0"));
            return categories;
        }

        public async Task<CategoryModel> GetByIdAsync(int id)
        {
            var category = await _unitOfWork.GetRepository<Category>().GetByIdAsync(id);
            if (category != null)
            {
                return new CategoryModel
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description,
                    ParentId = category.ParentId,
                    StatusId = category.Status,
                    Order = category.Order
                };
            }
            return null;
        }

        public async Task<IPagedList<CategoryModel>> GetListAsync(CategoryFilteringModel filtering)
        {
            var rawQuery = _unitOfWork.GetRepository<Category>().Table
                .Where(t => t.Status != (short)StatusEnum.Delete);

            if (!string.IsNullOrEmpty(filtering.SearchKeyword))
            {
                string keyword = filtering.SearchKeyword.ToLower();
                rawQuery = rawQuery.Where(t => t.Name.ToLower().Contains(keyword)
                                || t.Description.ToLower().Contains(keyword));
            }

            var query = rawQuery.Select(t => new CategoryModel
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description,
                StatusId = t.Status,
                Order = t.Order,
                ParentId = t.ParentId
            });

            //Set default sort;
            if (!string.IsNullOrEmpty(filtering.SortBy))
            {
                query = query.GenericSort(filtering.SortBy, filtering.SortDirection);
            }
            else
            {
                query = query.OrderByDescending(s => s.Id);
            }

            return await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
        }

        public async Task<Result<int>> SaveAsync(CategoryModel model)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            if (model.Id > 0)
            {
                var category = await _unitOfWork.GetRepository<Category>().GetByIdAsync(model.Id);
                if (category != null)
                {
                    category.Name = model.Name;
                    category.Description = model.Description;
                    category.Order = model.Order;
                    category.Status = model.StatusId;
                    category.ParentId = model.ParentId;
                    _unitOfWork.GetRepository<Category>().Update(category);
                    _unitOfWork.SaveChanges();
                    result.ResultType = ResultType.Success;
                    result.Message = _messages.Updated_Category_Successful;
                    result.Value = category.Id;
                }
            }
            else
            {
                var category = new Category
                {
                    Name = model.Name,
                    Description = model.Description,
                    Order = model.Order,
                    Status = model.StatusId,
                    ParentId = model.ParentId,
                    CreatedDate = DateTime.UtcNow
                };
                _unitOfWork.GetRepository<Category>().Add(category);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Added_Category_Successful;
                result.Value = category.Id;
            }

            if (result.ResultType == ResultType.Success)
                RefreshCache();
            return result;
        }

        public async Task<Result<int>> DeleteAsync(int id)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var category = await _unitOfWork.GetRepository<Category>().GetByIdAsync(id);
            if (category != null)
            {
                category.Status = (short)StatusEnum.Delete;
                _unitOfWork.GetRepository<Category>().Update(category);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Category_Successful;
                result.Value = category.Id;
            }

            if (result.ResultType == ResultType.Success)
                RefreshCache();
            return result;
        }

        private async Task<List<CategoryModel>> GetCategoriesFromCache()
        {
            var categories = await _cacheManager.Get(CacheKeys.CATEGORIES_LIST, async () =>
            {
                var result = await _unitOfWork.GetRepository<Category>().Table
                    .Where(t => t.ParentId == 0 && t.Status != (short)StatusEnum.Delete)
                    .OrderBy(t => t.Order)
                    .Select(category => new CategoryModel
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Description = category.Description,
                        ParentId = category.ParentId,
                        StatusId = category.Status,
                        Order = category.Order
                    })
                    .ToListAsync();
                return result;
            });

            return categories;
        }

        private void RefreshCache()
        {
            _cacheManager.Remove(CacheKeys.CATEGORIES_LIST);
        }
    }
}
