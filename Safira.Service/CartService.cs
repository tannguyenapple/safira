﻿using Microsoft.EntityFrameworkCore;
using Safira.Data.Entities;
using Safira.Data.UnitOfWork;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Product;
using Safira.Domain.Result;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Safira.Service
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;

        public CartService(
            IUnitOfWork unitOfWork,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _messages = messages;
        }

        public List<ProductCartViewModel> GetCart(string userId)
        {
            var cart = _unitOfWork.GetRepository<Cart>().Table
                .Include(t => t.Product).ThenInclude(t => t.ProductImage)
                .Where(t => t.UserId == userId && !t.OrderId.HasValue)
                .Select(t => new ProductCartViewModel
                {
                    ProductId = t.ProductId,
                    Name = t.Product.Name,
                    Price = t.Product.Price,
                    Quantity = t.Quantity,
                    PictureId = t.Product.ProductImage.FirstOrDefault().PictureId
                }).ToList();

            return cart;
        }

        public Result<long> AddCart(string userId, long productId, int quantity = 1, bool replace = false)
        {
            var cart = _unitOfWork.GetRepository<Cart>().Table
                .FirstOrDefault(t => t.ProductId == productId && t.UserId == userId && !t.OrderId.HasValue);
            if (cart is null)
            {
                cart = new Cart
                {
                    ProductId = productId,
                    Quantity = quantity,
                    UserId = userId,
                    CreatedDate = DateTime.UtcNow
                };
                _unitOfWork.GetRepository<Cart>().Add(cart);
            }
            else
            {
                if (replace)
                    cart.Quantity = quantity;
                else
                    cart.Quantity += quantity;
                _unitOfWork.GetRepository<Cart>().Update(cart);
            }
            _unitOfWork.SaveChanges();

            return new Result<long>
            {
                ResultType = ResultType.Success,
                Value = productId
            };
        }

        public Result<long> RemoveCart(string userId, long productId)
        {
            var cart = _unitOfWork.GetRepository<Cart>().Table
                .FirstOrDefault(t => t.ProductId == productId && t.UserId == userId && !t.OrderId.HasValue);
            if (cart != null)
            {
                _unitOfWork.GetRepository<Cart>().Delete(cart);
                _unitOfWork.SaveChanges();
            }
            return new Result<long>
            {
                ResultType = ResultType.Success,
                Value = productId
            };
        }

        public List<ProductCartViewModel> GetWish(string userId)
        {
            var cart = _unitOfWork.GetRepository<Wish>().Table
                .Include(t => t.Product).ThenInclude(t => t.ProductImage)
                .Where(t => t.UserId == userId)
                .Select(t => new ProductCartViewModel
                {
                    ProductId = t.ProductId,
                    Name = t.Product.Name,
                    Price = t.Product.Price,
                    PictureId = t.Product.ProductImage.FirstOrDefault().PictureId
                }).ToList();

            return cart;
        }

        public Result<long> AddWish(string userId, long productId)
        {
            var cart = _unitOfWork.GetRepository<Wish>().Table
                .FirstOrDefault(t => t.ProductId == productId && t.UserId == userId);
            if (cart is null)
            {
                cart = new Wish
                {
                    ProductId = productId,
                    UserId = userId,
                    CreatedDate = DateTime.UtcNow
                };
                _unitOfWork.GetRepository<Wish>().Add(cart);
                _unitOfWork.SaveChanges();
            }
           

            return new Result<long>
            {
                ResultType = ResultType.Success,
                Value = productId
            };
        }

        public Result<long> RemoveWish(string userId, long productId)
        {
            var cart = _unitOfWork.GetRepository<Wish>().Table
                .FirstOrDefault(t => t.ProductId == productId && t.UserId == userId);
            if (cart != null)
            {
                _unitOfWork.GetRepository<Wish>().Delete(cart);
                _unitOfWork.SaveChanges();
            }
            return new Result<long>
            {
                ResultType = ResultType.Success,
                Value = productId
            };
        }
    }
}
