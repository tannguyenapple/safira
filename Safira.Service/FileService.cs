﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Safira.Data.Entities;
using Safira.Data.UnitOfWork;
using Safira.Domain.Caching;
using Safira.Domain.Const;
using Safira.Domain.Enums;
using Safira.Domain.Helper;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Result;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Safira.Service
{
    public class FileService : IFileService
    {
        private const string IMAGE_FOLDER_PATH = "images/storages";

        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly Messages _messages;

        #endregion

        #region Ctor

        public FileService(
            IUnitOfWork unitOfWork,
            IHostingEnvironment hostingEnvironment,
            IWebHelper webHelper,
            ICacheManager cacheManager,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _hostingEnvironment = hostingEnvironment;
            _webHelper = webHelper;
            _cacheManager = cacheManager;
            _messages = messages;
        }

        #endregion

        #region Methods

        public async Task<FileModel> GetByIdAsync(long id)
        {
            var images = await GetPicturesFromCache();
            var image = images.FirstOrDefault(t => t.Id == id);
            if (image != null)
            {
                return new FileModel
                {
                    Binary = image.Binary,
                    SeoFileName = image.SeoFileName,
                    AltAttribute = image.AltAttribute,
                    MimeType = image.MimeType,
                    TitleAttribute = image.TitleAttribute
                };
            }
            return null;
        }

        public async Task<Result<long>> SaveAsync(FileModel model)
        {
            var result = new Result<long>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            if (model.Id > 0)
            {
                var image = await _unitOfWork.GetRepository<Picture>().GetByIdAsync(model.Id);
                if (image != null)
                {
                    //image.Binary = model.Binary;
                    image.SeoFileName = model.SeoFileName;
                    image.TitleAttribute = model.TitleAttribute;
                    image.MimeType = model.MimeType;
                    image.AltAttribute = model.AltAttribute;
                    _unitOfWork.GetRepository<Picture>().Update(image);
                    _unitOfWork.SaveChanges();

                    SavePictureInFile(image.Id, model.Binary, image.MimeType);

                    result.ResultType = ResultType.Success;
                    result.Message = _messages.Updated_Image_Successful;
                    result.Value = image.Id;
                }
            }
            else
            {
                var image = new Picture
                {
                    //Binary = model.Binary,
                    SeoFileName = model.SeoFileName,
                    AltAttribute = model.AltAttribute,
                    MimeType = model.MimeType,
                    TitleAttribute = model.TitleAttribute
                };
                _unitOfWork.GetRepository<Picture>().Add(image);
                _unitOfWork.SaveChanges();

                SavePictureInFile(image.Id, model.Binary, image.MimeType);

                result.ResultType = ResultType.Success;
                result.Message = _messages.Added_Image_Successful;
                result.Value = image.Id;
            }

            return result;
        }

        public async Task<Result<List<long>>> SaveAsync(List<FileModel> model)
        {
            var result = new Result<List<long>>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var images = model.Select(t => new Picture
            {
                //Binary = t.Binary,
                SeoFileName = t.SeoFileName,
                AltAttribute = t.AltAttribute,
                MimeType = t.MimeType,
                TitleAttribute = t.TitleAttribute
            }).ToList();

            _unitOfWork.GetRepository<Picture>().AddRange(images);
            await _unitOfWork.SaveChangesAsync();

            model.ForEach(t => t.Id = images.FirstOrDefault(i => i.SeoFileName == t.SeoFileName && i.MimeType == t.MimeType).Id);

            SavePicturesInFile(model);

            result.ResultType = ResultType.Success;
            result.Message = _messages.Added_Image_Successful;
            result.Value = images.Select(t => t.Id).ToList();

            return result;
        }

        public async Task<Result<long>> DeleteAsync(long id)
        {
            var result = new Result<long>()
            {
                Value = id,
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var image = await _unitOfWork.GetRepository<Picture>().GetByIdAsync(id);
            if (image != null)
            {
                _unitOfWork.GetRepository<Picture>().Delete(image);
                _unitOfWork.SaveChanges();

                DeletePictureOnFile(image.Id, image.MimeType);
                DeleteThumbnailInFile(image.Id);
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Image_Successful;
            }

            return result;
        }

        public async Task<string> GetUrlAsync(long id, int targetSize = 0)
        {
            var picture = await _unitOfWork.GetRepository<Picture>().GetByIdAsync(id);
            return GetPictureUrl(picture, targetSize);
        }

        public string GetPictureUrl(Picture picture)
        {
            if (picture == null)
                return string.Empty;
            var url = string.Empty;
            var lastPart = GetFileExtensionFromMimeType(picture.MimeType);
            string fileName = $"{picture.Id:0000000}_0.{lastPart}";
            return GetPictureLocalPath(fileName);
        }

        public string GetPictureUrl(Picture picture, int targetSize = 0)
        {
            if (picture == null)
                return string.Empty;
            var url = string.Empty;
            byte[] pictureBinary = LoadPictureBinary(picture);
            var lastPart = GetFileExtensionFromMimeType(picture.MimeType);
            string fileName = $"{picture.Id:0000000}_{targetSize}.{lastPart}";
            string filePath = string.Empty;
            if (targetSize == 0)
                filePath = GetPictureLocalPath(fileName);
            else
                filePath = GetThumbLocalPath(fileName);

            //The named mutext helps to avoid creating the same files in different threads.
            //And does not decrease performace significantly, because the code is blocked only specific file.
            using (var mutex = new Mutex(false, fileName))
            {
                if (!File.Exists(filePath))
                {
                    mutex.WaitOne();
                    if (!File.Exists(filePath))
                    {
                        if (targetSize == 0)
                            SavePictureInFile(picture.Id, pictureBinary, picture.MimeType);
                        else
                        {
                            SaveThumbnailInFile(picture, targetSize);
                        }
                    }
                    mutex.ReleaseMutex();
                }
            }
            return GetPictureUrl(fileName, targetSize != 0);
        }

        #endregion

        #region Utilities        

        protected void SavePictureInFile(long id, byte[] binary, string mimeType)
        {
            var lastPart = GetFileExtensionFromMimeType(mimeType);
            var fileName = $"{id:0000000}_0.{lastPart}";
            File.WriteAllBytes(GetPictureLocalPath(fileName), binary);
        }

        protected void SavePicturesInFile(List<FileModel> pictures)
        {
            foreach (var pic in pictures)
            {
                var lastPart = GetFileExtensionFromMimeType(pic.MimeType);
                var fileName = $"{pic.Id:0000000}_0.{lastPart}";
                File.WriteAllBytes(GetPictureLocalPath(fileName), pic.Binary);
            }
        }

        protected void DeletePictureOnFile(long id, string mimeType)
        {
            var lastPart = GetFileExtensionFromMimeType(mimeType);
            var fileName = $"{id:0000000}_0.{lastPart}";
            var filePath = GetPictureLocalPath(fileName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        protected void DeleteThumbnailInFile(long id)
        {
            var filter = $"{id:0000000}*.*";
            var thumbDirectoryPath = Path.Combine(_hostingEnvironment.WebRootPath, "images\\thumbs");
            var files = Directory.GetFiles(thumbDirectoryPath, filter, SearchOption.AllDirectories);
            foreach (var fileName in files)
            {
                var thumbFilePath = GetThumbLocalPath(fileName);
                File.Delete(thumbFilePath);
            }
        }

        protected void SaveThumbnailInFile(string filePath, int targetSize)
        {
            if (File.Exists(filePath))
            {
                using (var originalImage = File.OpenRead(filePath))
                {
                    using (var inputStream = new SKManagedStream(originalImage))
                    {
                        using (var original = SKBitmap.Decode(inputStream))
                        {
                            var newSize = CalculateDimensions(new Size(original.Width, original.Height), targetSize);
                            using (var resized = original.Resize(new SKImageInfo(newSize.Width, newSize.Height), SKFilterQuality.High))
                            {
                                if (resized == null)
                                    return;

                                var fileExtension = Path.GetExtension(filePath);
                                var thumbFileName = $"{Path.GetFileNameWithoutExtension(filePath)}_{targetSize}{fileExtension}";
                                var thumbFilePath = GetThumbLocalPath(thumbFileName);
                                using (var image = SKImage.FromBitmap(resized))
                                {
                                    using (var output = File.OpenWrite(thumbFilePath))
                                    {
                                        image.Encode(SKEncodedImageFormat.Png, 75).SaveTo(output);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void SaveThumbnailInFile(Picture picture, int targetSize)
        {
            if (picture == null)
                throw new ArgumentNullException(nameof(picture));

            var lastPart = GetFileExtensionFromMimeType(picture.MimeType);
            string fileName = $"{picture.Id:0000000}_{targetSize}.{lastPart}";
            string filePath = GetThumbLocalPath(fileName);
            byte[] pictureBinary = LoadPictureBinary(picture);
            using (var stream = new MemoryStream(pictureBinary))
            {
                using (var inputStream = new SKManagedStream(stream))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        var newSize = CalculateDimensions(new Size(original.Width, original.Height), targetSize);
                        using (var resized = original.Resize(new SKImageInfo(newSize.Width, newSize.Height), SKFilterQuality.High))
                        {
                            if (resized == null)
                                return;

                            using (var image = SKImage.FromBitmap(resized))
                            {
                                using (var output = File.OpenWrite(filePath))
                                {
                                    image.Encode(SKEncodedImageFormat.Png, 75).SaveTo(output);
                                }
                            }
                        }
                    }
                }
            }
        }



        protected byte[] LoadPictureBinary(Picture picture, bool fromDb = false)
        {
            if (picture == null)
                throw new ArgumentNullException(nameof(picture));

            return LoadPictureFromFile(picture.Id, picture.MimeType);

        }

        protected byte[] LoadPictureFromFile(long id, string mimeType)
        {
            var lastPart = GetFileExtensionFromMimeType(mimeType);
            var fileName = $"{id:0000000}_0.{lastPart}";
            var filePath = GetPictureLocalPath(fileName);
            if (File.Exists(filePath))
                return File.ReadAllBytes(filePath);
            return new byte[0];
        }

        protected Size CalculateDimensions(Size originalSize, int targetSize, ResizeTypeEnum type = ResizeTypeEnum.LongestSide, bool ensureSizePositive = true)
        {
            float width, height;

            switch (type)
            {
                case ResizeTypeEnum.Height:
                case ResizeTypeEnum.LongestSide when (originalSize.Height > originalSize.Width):
                    width = originalSize.Width * (targetSize / (float)originalSize.Height);
                    height = targetSize;
                    break;
                case ResizeTypeEnum.Width:
                case ResizeTypeEnum.LongestSide when (originalSize.Height <= originalSize.Width):
                    width = targetSize;
                    height = originalSize.Height * (targetSize / (float)originalSize.Width);
                    break;
                default:
                    throw new Exception("Not supported ResizeTypeEnum");
            }

            if (ensureSizePositive)
            {
                if (width < 1) width = 1;
                if (height < 1) height = 1;
            }

            return new Size((int)Math.Round(width), (int)Math.Round(height));
        }

        protected string GetFileExtensionFromMimeType(string mimeType)
        {
            if (mimeType == null)
                return null;

            var parts = mimeType.Split('/');
            var lastPart = parts[parts.Length - 1];
            switch (lastPart)
            {
                case "pjpeg":
                    lastPart = "jpg";
                    break;
                case "x-png":
                    lastPart = "png";
                    break;
                case "x-icon":
                    lastPart = "ico";
                    break;
            }
            return lastPart;
        }

        protected string GetPictureLocalPath(string fileName)
        {
            return Path.Combine(_hostingEnvironment.WebRootPath, "images", fileName);
        }

        protected virtual string GetThumbLocalPath(string thumbFileName)
        {
            var thumbsDirectoryPath = Path.Combine(_hostingEnvironment.WebRootPath, "images\\thumbs");
            if (!Directory.Exists(thumbsDirectoryPath))
            {
                Directory.CreateDirectory(thumbsDirectoryPath);
            }
            return Path.Combine(thumbsDirectoryPath, thumbFileName);
        }

        protected string GetPictureUrl(string fileName, bool isThumbnail = false)
        {
            if (isThumbnail)
                return Path.Combine(_webHelper.GetHost(), "images", "thumbs", fileName);
            else
                return Path.Combine(_webHelper.GetHost(), "images", fileName);
        }

        private async Task<List<FileModel>> GetPicturesFromCache()
        {
            var products = await _cacheManager.Get(CacheKeys.PICTURE_LIST, async () =>
            {
                var result = await _unitOfWork.GetRepository<Picture>().Table
                    .Select(t => new FileModel
                    {
                        Id = t.Id,
                        //Binary = t.Binary,
                        SeoFileName = t.SeoFileName,
                        AltAttribute = t.AltAttribute,
                        MimeType = t.MimeType,
                        TitleAttribute = t.TitleAttribute
                    })
                    .ToListAsync();
                return result;
            });

            return products;
        }

        #endregion
    }
}
