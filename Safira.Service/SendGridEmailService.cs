﻿using Safira.Domain.Helper;
using Safira.Domain.IServices;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Safira.Service
{
    public class SendGridEmailService : IEmailSender
    {
        private string _apiKey;
        private EmailAddressModel _fromEmail;
        private List<EmailAddress> _ccEmails;

        public SendGridEmailService(string apiKey, string emailFrom, string displayName, string ccEmails, string ccDisplayNames)
        {
            _apiKey = apiKey;
            _fromEmail = new EmailAddressModel(emailFrom, displayName);
            _ccEmails = new List<EmailAddress>();

            if (!string.IsNullOrEmpty(ccEmails))
            {
                var mails = ccEmails.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var displayNames = (ccDisplayNames ?? "").Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                for (var i = 0; i < mails.Length; i++)
                {
                    var name = i < displayNames.Length ? displayNames[i] : mails[i];
                    _ccEmails.Add(new EmailAddress(mails[i], name));
                }
            }
        }

        public async Task<EmailResponse> SendEmailAsync(string toEmail, string subject, string htmlMessage)
        {
            var to = new EmailAddressModel(toEmail);
            return await SendEmailAsync(_fromEmail, to, subject, htmlMessage);
        }

        public async Task<EmailResponse> SendEmailAsync(List<string> toEmails, string subject, string htmlMessage)
        {
            var emailAddress = new List<EmailAddressModel>();
            foreach (var email in toEmails)
            {
                emailAddress.Add(new EmailAddressModel(email));
            }

            return await SendEmailAsync(_fromEmail, emailAddress, subject, htmlMessage);
        }

        public async Task<EmailResponse> SendEmailAsync(string fromEmail, string toEmail, string subject, string htmlMessage)
        {
            var from = new EmailAddressModel(fromEmail);
            var to = new EmailAddressModel(toEmail);
            return await SendEmailAsync(from, to, subject, htmlMessage);
        }

        public async Task<EmailResponse> SendEmailAsync(EmailAddressModel toEmail, string subject, string htmlMessage)
        {
            return await SendEmailAsync(_fromEmail, toEmail, subject, htmlMessage);
        }

        public async Task<EmailResponse> SendEmailAsync(EmailAddressModel fromEmail, EmailAddressModel toEmail, string subject, string htmlMessage)
        {
            var toEmails = new List<EmailAddressModel>() { toEmail };
            return await SendEmailAsync(fromEmail, toEmails, subject, htmlMessage);
        }

        //public async Task<EmailResponse> SendEmailAsync(List<EmailAddressModel> toEmails, string subject, string htmlMessage, List<EmailAttachedFile> attachedFiles = null, List<EmailAddressModel> ccEmails = null)
        //{
        //    return await SendEmailAsync(_fromEmail, toEmails, subject, htmlMessage, attachedFiles, ccEmails);
        //}

        public async Task<EmailResponse> SendEmailAsync(EmailAddressModel fromEmail, List<EmailAddressModel> toEmails, string subject, string htmlMessage)
        {
            var emailAddress = new List<EmailAddress>();
            foreach (var email in toEmails)
            {
                emailAddress.Add(new EmailAddress()
                {
                    Email = email.Email,
                    Name = email.DisplayName
                });
            }
            var plainTextContent = CommonHelper.StripHTML(htmlMessage);
            var from = new EmailAddress(fromEmail.Email, fromEmail.DisplayName);
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, emailAddress, subject, plainTextContent, htmlMessage);
            //if (_ccEmails.Any())
            //{
            //    msg.AddCcs(_ccEmails);
            //}

            var client = new SendGridClient(_apiKey);
            var result = await client.SendEmailAsync(msg);
            return new EmailResponse()
            {
                IsSuccessStatusCode = result.StatusCode == System.Net.HttpStatusCode.Accepted || result.StatusCode == System.Net.HttpStatusCode.OK,
                StatusCode = result.StatusCode,
                Message = result.Body.ReadAsStringAsync().Result
            };
        }

        /*
        public async Task<EmailResponse> SendEmailAsync(EmailAddressModel fromEmail, string toEmail, string subject, EmailAttachedFile attachedFile)
        {
            var to = new EmailAddressModel(toEmail);
            var toEmails = new List<EmailAddressModel>() { to };
            return await SendEmailAsync(fromEmail, toEmails, subject, string.Empty, new List<EmailAttachedFile> { attachedFile });
        }

        public async Task<EmailResponse> SendEmailAsync(EmailAddressModel fromEmail, List<EmailAddressModel> toEmails, string subject, string htmlMessage, List<EmailAttachedFile> attachedFiles = null, List<EmailAddressModel> ccEmails = null)
        {
            var emailAddress = new List<EmailAddress>();
            foreach (var email in toEmails)
            {
                emailAddress.Add(new EmailAddress
                {
                    Email = email.Email,
                    Name = email.DisplayName
                });
            }

            var ccAddress = new List<EmailAddress>();
            if (ccEmails != null && ccEmails.Any())
            {
                foreach (var email in ccEmails)
                {
                    ccAddress.Add(new EmailAddress
                    {
                        Email = email.Email,
                        Name = email.DisplayName
                    });
                }
            }

            var plainTextContent = CommonHelper.StripHTML(htmlMessage);
            var from = new EmailAddress(fromEmail.Email, fromEmail.DisplayName);
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, emailAddress, subject, plainTextContent, htmlMessage);

            if (_ccEmails.Any())
            {
                foreach (var cc in _ccEmails)
                    ccAddress.Add(cc);
            }

            if (ccAddress.Any())
            {
                msg.AddCcs(ccAddress);
            }

            if (attachedFiles != null)
            {
                foreach (var file in attachedFiles)
                {
                    msg.AddAttachment(file.FileName, file.Base64Content);
                }
            }
            var client = new SendGridClient(_apiKey);
            var result = await client.SendEmailAsync(msg);

            return new EmailResponse()
            {
                IsSuccessStatusCode = result.StatusCode == System.Net.HttpStatusCode.Accepted || result.StatusCode == System.Net.HttpStatusCode.OK,
                StatusCode = result.StatusCode,
                Message = result.Body.ReadAsStringAsync().Result
            };
        }

        public async Task<EmailResponse> SendEmailMultipleAsync(List<EmailAddressModel> toEmails, string subject, string htmlMessage, List<EmailAttachedFile> attachedFiles = null, EmailAddressModel fromEmail = null, List<EmailAddressModel> ccEmails = null)
        {
            if (fromEmail == null)
                fromEmail = _fromEmail;
            return await SendEmailAsync(fromEmail, toEmails, subject, htmlMessage, attachedFiles, ccEmails);
        }
        */
    }
}
