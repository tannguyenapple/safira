﻿namespace Safira.Service
{
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Category;
    using Safira.Domain.Result;
    using System;
    using System.Threading.Tasks;

    public class EmailService : IEmailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;

        public EmailService(
            IUnitOfWork unitOfWork,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _messages = messages;
        }

        public async Task<CategoryModel> GetByIdAsync(int id)
        {
            var category = await _unitOfWork.GetRepository<Category>().GetByIdAsync(id);
            if (category != null)
            {
                return new CategoryModel
                {
                    Id = category.Id,
                    Name = category.Name,
                    Description = category.Description,
                    ParentId = category.ParentId,
                    StatusId = category.Status,
                    Order = category.Order
                };
            }
            return null;
        }

        public async Task<Result<int>> SaveAsync(EmailModel model)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            if (model.Id > 0)
            {
                var email = await _unitOfWork.GetRepository<EmailHistory>().GetByIdAsync(model.Id);
                if (email != null)
                {
                    email.Subject = model.Subject;
                    email.Content = model.Content;
                    email.RecipientEmail = model.RecipientEmail;
                    email.RecipientName = model.RecipientName;
                    email.StatusId = (short)model.StatusId;

                    _unitOfWork.GetRepository<EmailHistory>().Update(email);
                    _unitOfWork.SaveChanges();
                    result.ResultType = ResultType.Success;
                    result.Value = email.Id;
                }
            }
            else
            {
                var email = new EmailHistory
                {
                    Subject = model.Subject,
                    Content = model.Content,
                    RecipientName = model.RecipientName,
                    RecipientEmail = model.RecipientEmail,
                    StatusId = (short)EmailStatusEnum.New,
                    CreatedDate = DateTime.UtcNow,
                    OrderId = model.OrderId
                };
                _unitOfWork.GetRepository<EmailHistory>().Add(email);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Value = email.Id;
            }

            return result;
        }

        public async Task<Result<int>> DeleteAsync(int id)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var email = await _unitOfWork.GetRepository<EmailHistory>().GetByIdAsync(id);
            if (email != null)
            {
                _unitOfWork.GetRepository<EmailHistory>().Delete(email);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Value = email.Id;
            }

            return result;
        }
    }
}
