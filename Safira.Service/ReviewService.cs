﻿namespace Safira.Service
{
    using Safira.Data.Entities;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.Enums;
    using Safira.Domain.IServices;
    using Safira.Domain.Models;
    using Safira.Domain.Models.Product;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly Messages _messages;

        public ReviewService(
            IUnitOfWork unitOfWork,
            Messages messages)
        {
            _unitOfWork = unitOfWork;
            _messages = messages;
        }

        public async Task<ReviewModel> GetByIdAsync(int id)
        {
            var review = await _unitOfWork.GetRepository<Review>().GetByIdAsync(id);
            if(review != null)
            {
                return new ReviewModel
                {
                    FullName = review.FullName,
                    Email = review.Email,
                    Rating = review.Rating,
                    Content = review.Content,
                    ProductId = review.ProductId
                };
            }
            return null;
        }

        public async Task<IPagedList<ReviewModel>> GetListAsync(ReviewFilteringModel filtering)
        {
            var rawQuery = _unitOfWork.GetRepository<Review>().Table;

            var query = rawQuery.Select(t => new ReviewModel
            {
                FullName = t.FullName,
                Email = t.Email,
                Rating = t.Rating,
                Content = t.Content,
                ProductId = t.ProductId,
                CreatedDate = t.CreatedDate
            });

            if(filtering.ProductId > 0)
            {
                query = query.Where(t => t.ProductId == filtering.ProductId);
            }

            return await query.ToPagedListAsync(filtering.PageNumber, filtering.PageSize);
        }

        public async Task<List<ReviewModel>> GetListAsync(long id)
        {
            return await _unitOfWork.GetRepository<Review>().Table
                .Where(t => t.ProductId == id)
                .Select(t => new ReviewModel
                {
                    FullName = t.FullName,
                    Email = t.Email,
                    Rating = t.Rating,
                    Content = t.Content,
                    ProductId = t.ProductId,
                    CreatedDate = t.CreatedDate
                }).ToListAsync();
        }

        public async Task<Result<int>> SaveAsync(ReviewModel model)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            if (model.Id > 0)
            {
                var review = await _unitOfWork.GetRepository<Review>().GetByIdAsync(model.Id);
                if (review != null)
                {
                    review.FullName = model.FullName;
                    review.Email = model.Email;
                    review.Rating = model.Rating;
                    review.Content = model.Content;
                    review.Status = (short)model.Status;
                    _unitOfWork.GetRepository<Review>().Update(review);
                    _unitOfWork.SaveChanges();
                    result.ResultType = ResultType.Success;
                    result.Message = _messages.Updated_Review_Successful;
                    result.Value = review.Id;
                }
            }
            else
            {
                var review = new Review
                {
                    FullName = model.FullName,
                    Email = model.Email,
                    Rating = model.Rating,
                    Content = model.Content,
                    Status = (short)StatusEnum.Pending,
                    CreatedDate = DateTime.Now,
                    ProductId = model.ProductId
                };
                try
                {
                    _unitOfWork.GetRepository<Review>().Add(review);
                    _unitOfWork.SaveChanges();
                }
                catch(Exception e)
                {
                    var temp = e.Message;
                }
                result.ResultType = ResultType.Success;
                result.Message = _messages.Added_Review_Successful;
                result.Value = review.Id;
            }

            return result;
        }

        public async Task<Result<int>> DeleteAsync(int id)
        {
            var result = new Result<int>()
            {
                ResultType = ResultType.Error,
                Message = _messages.Process_Failed
            };

            var review = await _unitOfWork.GetRepository<Review>().GetByIdAsync(id);
            if (review != null)
            {
                _unitOfWork.GetRepository<Review>().Delete(review);
                _unitOfWork.SaveChanges();
                result.ResultType = ResultType.Success;
                result.Message = _messages.Deleted_Review_Successful;
                result.Value = review.Id;
            }

            return result;
        }
    }
}
