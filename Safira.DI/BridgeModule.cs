﻿namespace Safira.DI
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Safira.Data;
    using Safira.Data.Repository;
    using Safira.Data.UnitOfWork;
    using Safira.Domain.Models;
    using Safira.Service;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class BridgeModule
    {
        public static void ResolveIoc(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SafiraContext>(c =>
            {
                try
                {
                    // Requires LocalDB which can be installed with SQL Server Express 2016
                    // https://www.microsoft.com/en-us/download/details.aspx?id=54284
                    c.UseSqlServer(configuration.GetConnectionString("Safira_Connection"), x => x.MigrationsAssembly("Safira.Data"));
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            });

            // Identity
            services.AddIdentity<ApplicationUser, IdentityRole>()//(options => options.SignIn.RequireConfirmedAccount = true)
            .AddEntityFrameworkStores<SafiraContext>();
            //.AddDefaultTokenProviders();
           // .AddTokenProvider<DefaultDataProtectorTokenProvider<ApplicationUser>>("doque.com");
            
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IUnitOfWork, UnitOfWork<SafiraContext>>();

            services.RegisterAllServices();
        }

        public static void MigrateService(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SafiraContext>();
                var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                context.Database.Migrate();
                SafiraContextSeed.SeedAsync(context, userManager).Wait();
            }
        }

        private static void RegisterAllServices(this IServiceCollection services)
        {
            Assembly assembly = typeof(BaseService).Assembly;
            IEnumerable<TypeInfo> classTypes = assembly.ExportedTypes.Select(t => t.GetTypeInfo()).Where(t => t.IsClass && !t.IsAbstract && t.Name.EndsWith("Service"));

            foreach (TypeInfo type in classTypes)
            {
                IEnumerable<TypeInfo> interfaces = type.ImplementedInterfaces.Select(i => i.GetTypeInfo());

                foreach (TypeInfo interfaceType in interfaces)
                {
                    services.AddTransient(interfaceType.AsType(), type.AsType());
                }
            }
        }
    }
}
