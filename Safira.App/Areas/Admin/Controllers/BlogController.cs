﻿using Microsoft.AspNetCore.Mvc;
using OMH.API.Core.Extensions;
using Safira.App.Extensions;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Blog;
using System.Threading.Tasks;

namespace Safira.App.Areas.Admin.Controllers
{
    public class BlogController : BaseController
    {
        #region Fields & Ctor

        private readonly IBlogService _blogService;
        private readonly ICategoryService _categoryService;
        private readonly ApplicationSettings _appSettings;

        public BlogController(
            IBlogService BlogService,
            ICategoryService categoryService,
            ApplicationSettings applicationSettings)
        {
            _blogService = BlogService;
            _categoryService = categoryService;
            _appSettings = applicationSettings;
        }

        #endregion

        #region Action Methods

        public async Task<ActionResult> Index()
        {
            var model = await GetListViewModel(new BlogFilteringModel());
            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            var model = new BlogViewModel
            {
                Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
                Categories = await _blogService.GetSelectListAsync(),
            };
            return View(model);
        }

        public async Task<IActionResult> Edit(long id)
        {
            var model = new BlogViewModel
            {
                Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
                Categories = await _blogService.GetSelectListAsync()
            };

            var Blog = await _blogService.GetByIdAsync(id);
            if (Blog != null)
            {
                model.Id = Blog.Id;
                model.Name = Blog.Name;
                model.TitleUrl = Blog.TitleUrl;
                model.TitleUrl = Blog.TitleUrl;
                model.Description = Blog.Description;
                model.BlogType = Blog.BlogType;
                model.CreatedDate = Blog.CreatedDate;
                model.Files = Blog.Files;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(BlogViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.TitleUrl = CommonExtension.FormatTitleUrl(model.Name);
                var result = await _blogService.SaveAsync(model);
                return JsonResult(result);
            }
            return JsonError("Invalid model.");
        }

        public async Task<IActionResult> Delete(int id)
        {
            var result = await _blogService.DeleteAsync(id);
            return JsonResult(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetList(BlogFilteringModel filtering)
        {
            return PartialView("_BlogList", await GetListViewModel(filtering));
        }

        #endregion

        #region Utilities Methods
        private async Task<BlogListViewModel> GetListViewModel(BlogFilteringModel filtering)
        {
            var Blogs = await _blogService.GetListAsync(filtering, true);
            var categories = await _blogService.GetSelectListAsync("Chọn nhóm");
            return new BlogListViewModel(Blogs, filtering)
            {
                Categories = categories,
                PageSizeOptions = _appSettings.PageSizeOptions.ToDicSelectList(filtering.PageSize.ToString(), false),
            };
        }

        #endregion
    }
}