﻿using Microsoft.AspNetCore.Mvc;
using Safira.App.Extensions;
using Safira.Domain.Const;
using Safira.Domain.Enums;
using Safira.Domain.Helper;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Order;
using Safira.Domain.Models.Product;
using Safira.Domain.Result;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Areas.Admin.Controllers
{
    public class OrderController : BaseController
    {
        #region Fields & Ctor

        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly ApplicationSettings _appSettings;

        public OrderController(
            IProductService productService,
            IOrderService orderService,
            ApplicationSettings applicationSettings)
        {
            _productService = productService;
            _orderService = orderService;
            _appSettings = applicationSettings;
        }

        #endregion

        #region Action Methods

        public async Task<ActionResult> Index()
        {
            var model = await GetListViewModel(new OrderFilteringModel());
            return View(model);
        }

        //public async Task<IActionResult> Create()
        //{
        //    var model = new ProductViewModel
        //    {
        //        Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
        //        Categories = await _categoryService.GetSelectListAsync(),
        //        Types = ProductTypeEnum.Trend.ToEnumSelectList(null, string.Empty)
        //    };
        //    return View(model);
        //}

        public async Task<IActionResult> Edit(int id)
        {
            var model = new OrderViewModel
            {
                Status = OrderStatusEnum.New.ToEnumSelectList(null, string.Empty),
                RawProducts = (await _productService.GetBriefAllAsync())
            };

            var order = await _orderService.GetByIdAsync(id);
            if (order != null)
            {
                model.Id = order.Id;
                model.FullName = order.FullName;
                model.Email = order.Email;
                model.PhoneNumber = order.PhoneNumber;
                model.Address = order.Address;
                model.Note = order.Note;
                model.Products = order.Products;
                model.StatusId = order.StatusId;

                SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.ADMIN_CART, order.Products);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(OrderViewModel model)
        {
            model.Products = SessionHelper.GetObjectFromJson<List<ProductCartViewModel>>(HttpContext.Session, SessionKeys.ADMIN_CART);
            var result = await _orderService.SaveOrder(model);
            return JsonResult(result);
        }

        //public async Task<IActionResult> Delete(int id)
        //{
        //    var result = await _productService.DeleteAsync(id);
        //    return JsonResult(result);
        //}

        [HttpGet]
        public async Task<IActionResult> GetList(OrderFilteringModel filtering)
        {
            return PartialView("_OrderList", await GetListViewModel(filtering));
        }

        [HttpPost]
        public async Task<IActionResult> AddToCart(long id, int quantity = 1, bool replace = false)
        {
            var result = new Result<long>
            {
                Message = "Ok",
                ResultType = ResultType.Success,
                Value = id
            };
            var productInCart = SessionHelper.GetObjectFromJson<List<ProductCartViewModel>>(HttpContext.Session, SessionKeys.ADMIN_CART);
            if (productInCart.Any(t => t.ProductId == id))
            {
                var product = productInCart.First(t => t.ProductId == id);
                if (replace)
                    product.Quantity = quantity;
                else
                    product.Quantity += quantity;
            }
            else
            {
                var prd = await _productService.GetByIdAsync(id);
                var product = new ProductCartViewModel
                {
                    ProductId = prd.Id,
                    Name = prd.Name,
                    Price = prd.Price,
                    Quantity = quantity != 0 ? quantity : 1,
                    PictureId = prd.Files.FirstOrDefault().Id
                };
                productInCart.Add(product);
            }

            SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.ADMIN_CART, productInCart);

            return JsonResult(result);
        }

        [HttpPost]
        public IActionResult RemoveOutCart(long id)
        {
            var result = new Result<long>
            {
                ResultType = ResultType.Success,
                Value = id
            };


            var productInCart = SessionHelper.GetObjectFromJson<List<ProductCartViewModel>>(HttpContext.Session, SessionKeys.ADMIN_CART);
            if (productInCart.Any(t => t.ProductId == id))
            {
                var product = productInCart.First(t => t.ProductId == id);
                productInCart.Remove(product);
            }

            SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.ADMIN_CART, productInCart);

            return JsonResult(result);
        }

        [HttpGet]
        public IActionResult GetProductList()
        {
            var model = SessionHelper.GetObjectFromJson<List<ProductCartViewModel>>(HttpContext.Session, SessionKeys.ADMIN_CART);
            return PartialView("_ProductList", model);
        }



        #endregion

        #region Utilities Methods
        private async Task<OrderListViewModel> GetListViewModel(OrderFilteringModel filtering)
        {
            var products = await _orderService.GetListAsync(filtering);
            return new OrderListViewModel(products, filtering)
            {
                Status = OrderStatusEnum.New.ToEnumSelectList(null, "All"),
                PageSizeOptions = _appSettings.PageSizeOptions.ToDicSelectList(filtering.PageSize.ToString(), false),
            };
        }

        #endregion
    }
}