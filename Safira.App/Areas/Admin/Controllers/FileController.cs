﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations;
using Safira.Domain.Extensions;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Areas.Admin.Controllers
{
    public class FileController : BaseController
    {
        #region Fields & Ctor

        private readonly IFileService _fileService;
        private readonly ApplicationSettings _appSettings;

        public FileController(
            IFileService fileService,
            ApplicationSettings applicationSettings)
        {
            _fileService = fileService;
            _appSettings = applicationSettings;
        }

        #endregion

        #region Action Methods

        public async Task<string> Get(long id)
        {
            var url = await _fileService.GetUrlAsync(id);
            return url;
        }

        //public async Task<FileResult> Get(long id)
        //{
        //    var img = await _fileService.GetByIdAsync(id);
        //    return File(img.Binary, img.MimeType);
        //}

        public async Task<IActionResult> Download(long id)
        {
            var img = await _fileService.GetByIdAsync(id);
            return File(new MemoryStream(img.Binary), img.MimeType, img.SeoFileName);
        }

        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            var files = HttpContext.Request.Form.Files;
            
            var models = files.Select(t => new FileModel
            {
                SeoFileName = t.FileName,
                MimeType = t.ContentType,
                Binary = t.GetImageBits()
            }).ToList();
            var result = await _fileService.SaveAsync(models);
            return JsonResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> Remove(long id)
        {
            var result = await _fileService.DeleteAsync(id);
            return JsonResult(result);
        }
        #endregion
    }
}
