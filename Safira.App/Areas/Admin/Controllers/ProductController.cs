﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Safira.App.Extensions;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Product;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        #region Fields & Ctor

        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly ApplicationSettings _appSettings;

        public ProductController(
            IProductService productService,
            ICategoryService categoryService,
            ApplicationSettings applicationSettings)
        {
            _productService = productService;
            _categoryService = categoryService;
            _appSettings = applicationSettings;
        }

        #endregion

        #region Action Methods

        public async Task<ActionResult> Index()
        {
            var model = await GetListViewModel(new ProductFilteringModel());
            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            var model = new ProductViewModel
            {
                Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
                Categories = await _categoryService.GetSelectListAsync(),
                Types = ProductTypeEnum.Trend.ToEnumSelectList(null, string.Empty)
            };
            return View(model);
        }

        public async Task<IActionResult> Edit(long id)
        {
            var model = new ProductViewModel
            {
                Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
                Categories = await _categoryService.GetSelectListAsync(),
                Types = ProductTypeEnum.Trend.ToEnumSelectList(null, string.Empty)
            };

            var product = await _productService.GetByIdAsync(id);
            if (product != null)
            {
                model.Id = product.Id;
                model.Name = product.Name;
                model.Description = product.Description;
                model.Processing = product.Processing;
                model.Useful = product.Useful;
                model.StatusId = product.StatusId;
                model.CategoryId = product.CategoryId;
                model.TypeId = product.TypeId;
                model.SaleUp = product.SaleUp;
                model.CreatedDate = product.CreatedDate;
                model.Price = product.Price;
                model.OldPrice = product.OldPrice;
                model.Files = product.Files;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _productService.SaveAsync(model);
                return JsonResult(result);
            }
            return JsonError("Invalid model.");
        }

        public async Task<IActionResult> Delete(int id)
        {
            var result = await _productService.DeleteAsync(id);
            return JsonResult(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetList(ProductFilteringModel filtering)
        {
            return PartialView("_ProductList", await GetListViewModel(filtering));
        }

        #endregion

        #region Utilities Methods
        private async Task<ProductListViewModel> GetListViewModel(ProductFilteringModel filtering)
        {
            var products = await _productService.GetListAsync(filtering, true);
            var categories = await _categoryService.GetSelectListAsync("Chọn nhóm");
            return new ProductListViewModel(products, filtering)
            {
                Categories = categories,
                PageSizeOptions = _appSettings.PageSizeOptions.ToDicSelectList(filtering.PageSize.ToString(), false),
            };
        }

        #endregion
    }
}