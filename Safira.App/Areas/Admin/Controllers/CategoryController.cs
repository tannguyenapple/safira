﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Safira.App.Extensions;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Category;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Areas.Admin.Controllers
{
    public class CategoryController : BaseController
    {
        #region Fields & Ctor

        private readonly ICategoryService _categoryService;
        private readonly ApplicationSettings _appSettings;

        public CategoryController(
            ICategoryService categoryService,
            ApplicationSettings applicationSettings)
        {
            _categoryService = categoryService;
            _appSettings = applicationSettings;
        }

        #endregion

        #region Action Methods

        public async Task<ActionResult> Index()
        {
            var model = await GetListViewModel(new CategoryFilteringModel());
            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            var model = new CategoryViewModel
            {
                Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
                Categories = await _categoryService.GetSelectListAsync("Chọn nhóm"),
                Orders = GetSelectListOrder()
            };
            return View(model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var model = new CategoryViewModel
            {
                Status = StatusEnum.Active.ToEnumSelectList(null, string.Empty),
                Categories = await _categoryService.GetSelectListAsync("Chọn nhóm"),
                Orders = GetSelectListOrder(),
            };

            var category = await _categoryService.GetByIdAsync(id);
            if (category != null)
            {
                model.Id = category.Id;
                model.Name = category.Name;
                model.Description = category.Description;
                model.StatusId = category.StatusId;
                model.Order = category.Order;
                model.ParentId = category.ParentId;
            }

            return View(model);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var result = await _categoryService.DeleteAsync(id);
            return JsonResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> Save(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _categoryService.SaveAsync(model);
                return JsonResult(result);
            }
            return JsonError("Invalid model.");
        }

        [HttpGet]
        public async Task<IActionResult> GetList(CategoryFilteringModel filtering)
        {
            return PartialView("_CategoryList", await GetListViewModel(filtering));
        }

        #endregion

        #region Utilities Methods
        private async Task<CategoryListViewModel> GetListViewModel(CategoryFilteringModel filtering)
        {
            var categories = await _categoryService.GetListAsync(filtering);
            return new CategoryListViewModel(categories, filtering)
            {
                PageSizeOptions = _appSettings.PageSizeOptions.ToDicSelectList(filtering.PageSize.ToString(), false),
            };
        }

        private List<SelectListItem> GetSelectListOrder()
        {
            var orders = Enumerable.Range(1, 20)
                .Select(t => new SelectListItem { Text = t.ToString(), Value = t.ToString() })
                .ToList();
            return orders;
        }
        #endregion
    }
}