﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Safira.Domain.Const;
using Safira.Domain.Models;
using Safira.Domain.Models.Account;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AccountController : Controller
    {
        #region Fields & Ctor
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        #endregion

        public async Task SendEmail()
        {
            var apiKey = "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";
            //var client = new SendGridClient(apiKey);
            //////send an email message using the SendGrid Web API with a console application.  
            //var msgs = new SendGridMessage()
            //{
            //    From = new EmailAddress("tannguyenapple@gmail.com", "Tân Nguyễn"),
            //    Subject = "Just test send email function",
            //    TemplateId = "fb09a5fb-8bc3-4183-b648-dc6d48axxxxx"  
            //};
            ////if you have multiple reciepents to send mail  
            //msgs.AddTo(new EmailAddress("dinhtana4@gmail.com", "TanDinh"));
            //msgs.SetFooterSetting(true, "<strong>Regards,</strong><b> Pankaj Sapkal", "Pankaj");
            //var responses = await client.SendEmailAsync(msgs);
            //var client = new SendGridClient(apiKey);
            //var from = new EmailAddress("dinhtana4@gmail.com", "Example User");
            //var subject = "Sending with SendGrid is Fun";
            //var to = new EmailAddress("tannguyenapple@gmail.com", "Test User");
            //var plainTextContent = "and easy to do anywhere, even with C#";
            //var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            //var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            //var response = await client.SendEmailAsync(msg);
            //var temp = response.StatusCode;
        }

        public IActionResult Login(string returnUrl = null)
        {
            _signInManager.SignOutAsync();

            ViewData["ReturnUrl"] = returnUrl == "/" ? "" : returnUrl;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewData["ReturnUrl"] = returnUrl;
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            //await SendEmail();
            if (ModelState.IsValid)
            {
                //Check the role of user
                var user = await _userManager.FindByEmailAsync(model.Username);
                if (user != null)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    if (roles.Any(t => t == MembershipRole.Admin))
                    {
                        var loginResult = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, lockoutOnFailure: false);
                        if (loginResult.Succeeded)
                        {
                            //var sign = _signInManager.IsSignedIn(User);
                            //var temp = User.Identity.IsAuthenticated;
                            if (string.IsNullOrEmpty(returnUrl))// || !Url.IsLocalUrl(returnUrl))
                            {
                                return RedirectToAction("index", "order");
                            }
                            else
                            {
                                return Redirect(returnUrl);
                            }
                        }
                    }
                }
            }

            ModelState.AddModelError("", "Username or password incorrect");

            return View(model);
        }

        public IActionResult Logout()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }
    }
}