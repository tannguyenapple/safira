﻿
//ajax paging for page item
$(document).on("change", ".js-pager-pagesize", function (e) {
    //reset page number to 1
    $("input[name='PageNumber']").val(1);
    var form = $(this).closest("form");
    form.submit();
    return;
});

$(document).on("click", "[data-ajax-grid-div]", function (e) {
    e.preventDefault();
    var $a = $(this).find('a');
    var formId = '#' + $a.closest("form")[0].id;
    var sortBy = $a.data("sort-by");
    var sortDirection = $a.data("sort-direction");

    $(formId + " [name='SortBy']").val(sortBy);
    $(formId + " [name='SortDirection']").val(sortDirection);
    $(formId).submit();
});

//ajax filtering for column
$(document).on("click", "[data-ajax-grid-column]", function (e) {
    e.preventDefault();
});



//numeric PageNumber
$(document).on('keypress', 'input[data-pagenumber="true"]', function (e) {
    if (e.keyCode === 13) { //press Enter
        $('#PageNumber').val($(this).val());
        var form = this.closest('form');
        if (form === undefined) {
            var formid = $(this).data("formid");
            form = $(formid);
        }
        $(form).submit();
            
    } else {
        return AppCommon.Utils.CheckOnlyNumber(e);
    }
});

$(document).on('keyup', 'input[data-pagenumber="true"]', function (e) {
    value = $(this).val();
    setTimeout(function () {
        var pageCountEle = $('#PageCount');
        if (pageCountEle.length > 0) {
            var pagecount = pageCountEle.val();
            if (parseInt(value) > parseInt(pagecount))
                value = pagecount;
        }
        $('input[data-pagenumber="true"]').val(value);
    }, 200);
});
