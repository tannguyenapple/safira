﻿
OrderModule = (function ($) {
    var self = {};
    function config(configs) {
        self = this;
        self.products = configs.products;
        self.rawProducts = configs.rawProducts;
        self.addToCartUrl = configs.addToCartUrl || "/Order/AddToCart";
        self.removeOutCartUrl = configs.removeOutCartUrl || "/Order/RemoveOutCart";
        init();
    }

    function init() {
        console.log(self.products);
        console.log(self.rawProducts);
        $('.select2').select2()
        $(document).on('click', '.dev-add-new-product-btn', function () {
            $(".dev-add-new-product-modal").modal();
        });

        $(document).on('click', '.dev-submit-add-product-btn', function () {
            var id = $("#dev-product-id-select").val();
            var quantity = $("#dev-number-of-product-ip").val();
            addToCart(id, quantity, false);
            $(".dev-add-new-product-modal").modal("hide");
        });

        $(document).on('click', '.dev-remove-out-cart', function () {
            var id = $(this).data("product-id");
            removeOutCart(id);
        });

        $(document).on('change', '.dev-quantity-product-ip', function () {
            var id = $(this).data("product-id");
            var quantity = $(this).val();
            addToCart(id, quantity, true);
        });

        $(document).on('click', '.dev-save-order-btn', function () {
            $("#dev-save-order-form").submit();
        });
    }

    function refreshShoppingCart() {
        $("#dev-shopping-cart-form").submit();
    }

    function addToCart(id, quantity, replace) {
        AppCommon.Utils.AjaxCall({
            url: self.addToCartUrl,
            dataType: "json",
            type: "POST",
            data: { id: id, quantity: quantity, replace: replace },
            success: function (_) {
                refreshShoppingCart();
            }
        });

    }

    function removeOutCart(id) {
        AppCommon.Utils.AjaxCall({
            url: self.removeOutCartUrl,
            dataType: "json",
            type: "POST",
            data: { id: id },
            success: function () {
                refreshShoppingCart();
            }
        });
    }

    function search() {
        $("#dev-order-list-form").submit();
    }

    function onSaveBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
    }

    function onSaveComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            AppCommon.Utils.ShowMessage(e.responseJSON);
            if (self.mode === AppCommon.ActionMode.Create) {
                window.location.href = self.viewListUrl;
            }
        }
        else {
            AppCommon.Utils.ShowMessage(e.responseJSON);
        }
    }

    function onGetDataBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
    }

    function onGetDataComplete() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
    }

    function onDeleteBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form-2", true);
    }

    function onDeleteComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form-2", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            AppCommon.Utils.ShowMessage(e.responseJSON);
            $(".dev-ajax-form").find('form').submit();
            $(".dev-confirm-delete-model").modal("hide");
        }
    }

    return {
        init: init,
        config: config,
        Search: search,
        OnSaveBegin: onSaveBegin,
        OnSaveComplete: onSaveComplete,
        OnGetDataBegin: onGetDataBegin,
        OnGetDataComplete: onGetDataComplete,
        OnDeleteBegin: onDeleteBegin,
        OnDeleteComplete: onDeleteComplete
    };
})(jQuery);