﻿
BlogModule = (function ($) {
    var self = {};
    function config(configs) {
        self = this;
        self.files = [];
        self.myDropzone = undefined;
        self.mode = configs.mode || AppCommon.ActionMode.None;
        self.viewListUrl = configs.viewListUrl;
        self.dropzoneUploadUrl = configs.dropzoneUploadUrl || "/File/Upload";
        self.dropzoneRemoveUrl = configs.dropzoneRemoveUrl || "/File/Remove";
        self.dropzoneGetUrl = configs.dropzoneGetUrl || "/File/Get";
        init();
    }

    function init() {
        if (self.mode === AppCommon.ActionMode.View) {
            $(document).on('click', '.dev-delete-item-on-list', function () {
                $(".dev-confirm-delete-modal .dev-delete-item-name").html($(this).data("name"));
                $(".dev-confirm-delete-modal input[name='id']").val($(this).data("id"));
                $(".dev-confirm-delete-modal").modal();
            });
        }
        else {
            initEventForDropzone();
            if (self.mode === AppCommon.ActionMode.Edit) {
                initMockFileInEditMode();
            }
        }
    }

    function renderFile() {
        var html = '';
        for (i = 0; i < self.files.length; i++) {
            var file = self.files[i];
            html += '<input name="Files[' + i + '].Id" type="hidden" value="' + file.id + '" data-name="' + file.name + '">';
        }
        $("#dev-image-container").html(html);
    }

    function initEventForDropzone() {
        Dropzone.options.myAwesomeDropzone = false;
        Dropzone.autoDiscover = false;

        self.myDropzone = new Dropzone("#upload-hotel-image",
            {
                url: self.dropzoneUploadUrl,
                paramName: "image", // The name that will be used to transfer the file
                maxFilesize: 12, // MB
                uploadMultiple: true,
                maxFiles: 100,
                parallelUploads: 100,
                addRemoveLinks: true,
                dictMaxFilesExceeded: "You can only upload up to 100 images",
                dictRemoveFile: "Delete",
                dictCancelUploadConfirmation: "Are you sure you want to cancel upload?",
                acceptedFiles: 'image/*',
                accept: function (file, done) {
                    if ((file.type).toLowerCase() !== "image/jpg" &&
                        (file.type).toLowerCase() !== "image/jpeg" &&
                        (file.type).toLowerCase() !== "image/png"
                    ) {
                        done("Invalid file");
                    } else {
                        done();
                    }
                    $('#divSave').attr("data-type", "disable");
                },
                successmultiple: function (files, response) {
                    for (var i = 0; i < files.length; i++) {
                        self.files.push({
                            id: response.value[i],
                            name: files[i].name
                        });
                    }

                    renderFile();
                    AppCommon.Utils.ShowMessage(response);
                },
                removedfile: function (file) {
                    var id = "";
                    $("#dev-image-container input").each(function () {
                        if ($(this).data("name") === file.name) {
                            id = $(this).val();
                        }
                    });

                    AppCommon.Utils.AjaxCall({
                        url: self.dropzoneRemoveUrl,
                        data: { id: id },
                        type: "POST",
                        success: function (data) {
                            if (data.status === AppCommon.SubmitStatus.Success) {
                                file.previewElement.innerHTML = "";
                                for (var i = 0; i < self.files.length; i++) {
                                    if (self.files[i].id == id) {
                                        self.files.splice(i, 1);
                                        break;
                                    }
                                }
                                renderFile();
                            }
                            AppCommon.Utils.ShowMessage(data);
                        },
                        error: function (data) {
                            alert("Error");
                        }
                    });
                },
                init: function () {
                    this.on("addedfile",
                        function (file) {
                            var _this = this;
                            if ($.inArray(file.type, ['image/jpeg', 'image/jpg', 'image/png', 'application/pdf']) == -1) {
                                _this.removeFile(file);
                                toastr.warning("Removed " + file.name + " due not supported.", 'Warning!');
                            }
                        });
                }
            });
    }

    function initMockFileInEditMode() {
        var mockFiles = [];

        $("#dev-image-container input").map(function () {
            self.files.push({
                id: this.value,
                name: $(this).data("name")
            });
            mockFiles.push({ name: $(this).data("name"), type: "image/jpg", accepted: true, url: self.dropzoneGetUrl + "/" + this.value });
        });

        for (var i = 0; i < mockFiles.length; i++) {

            self.myDropzone.files.push(mockFiles[i]);
            self.myDropzone.emit('addedfile', mockFiles[i]);
            self.myDropzone.createThumbnailFromUrl(mockFiles[i], mockFiles[i].url, function () {
            }, "anonymous");
            self.myDropzone.emit('complete', mockFiles[i]);
        }
    }

    function search() {
        $("#dev-Blog-list-form").submit();
    }

    function onSaveBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
    }

    function onSaveComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            AppCommon.Utils.ShowMessage(e.responseJSON);
            if (self.mode === AppCommon.ActionMode.Create) {
                window.location.href = self.viewListUrl;
            }
        }
        else {
            AppCommon.Utils.ShowMessage(e.responseJSON);
        }
    }

    function onGetDataBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
    }

    function onGetDataComplete() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
    }

    function onDeleteBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form-2", true);
    }

    function onDeleteComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form-2", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            AppCommon.Utils.ShowMessage(e.responseJSON);
            $(".dev-ajax-form").find('form').submit();
            $(".dev-confirm-delete-model").modal("hide");
        }
    }

    return {
        init: init,
        config: config,
        Search: search,
        OnSaveBegin: onSaveBegin,
        OnSaveComplete: onSaveComplete,
        OnGetDataBegin: onGetDataBegin,
        OnGetDataComplete: onGetDataComplete,
        OnDeleteBegin: onDeleteBegin,
        OnDeleteComplete: onDeleteComplete
    };
})(jQuery);