﻿
CategoryModule = (function ($) {
    var self = {};
    function config(configs) {
        self.mode = configs.mode || AppCommon.ActionMode.None;
        self.getListUrl = configs.getListUrl;
        self.viewListUrl = configs.viewListUrl;
        init();
    }
    
    function init() {
       
        $(document).on('click', '.dev-delete-item-on-list', function () {
            $(".dev-confirm-delete-modal .dev-delete-item-name").html($(this).data("name"));
            $(".dev-confirm-delete-modal input[name='id']").val($(this).data("id"));
            $(".dev-confirm-delete-modal").modal();
        });
    }

    function loadTotalByDataType() {
        AppCommon.Utils.ShowLoading(".dev-total-by-datatype", true);
        AppCommon.Utils.AjaxCall({
            url: self.getTotalByDataTypeUrl,
            dataType: "html",
            type: "GET",
            data: {},
            success: function (resultData) {
                $("#dev-total-by-datatype").html(resultData);
                AppCommon.Utils.ShowLoading(".dev-total-by-datatype", false);
            }
        });
    }

    function onSaveBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
    }

    function onSaveComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            AppCommon.Utils.ShowMessage(e.responseJSON);
            if (self.mode === AppCommon.ActionMode.Create) {
                window.location.href = self.viewListUrl;
            }
        }
    }

    function onGetDataBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
    }

    function onGetDataComplete() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
    }

    function onDeleteBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form-2", true);
    }

    function onDeleteComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form-2", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            AppCommon.Utils.ShowMessage(e.responseJSON);
            $(".dev-ajax-form").find('form').submit();
            $(".dev-confirm-delete-model").modal("hide");
        }
    }

    return {
        init: init,
        config: config,
        OnSaveBegin: onSaveBegin,
        OnSaveComplete: onSaveComplete,
        OnGetDataBegin: onGetDataBegin,
        OnGetDataComplete: onGetDataComplete,
        OnDeleteBegin: onDeleteBegin,
        OnDeleteComplete: onDeleteComplete
    };
})(jQuery);