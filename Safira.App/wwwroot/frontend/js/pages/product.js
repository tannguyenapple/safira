﻿
ProductModule = (function ($) {
    var self = {};
    function config(configs) {
        init();
    }
    
    function init() {
        $("#dev-related-product-form").submit();
    }

    function onAjaxBegin() {
        AppCommon.Utils.ShowLoading("#dev-setting-all-box", true);
    }

    function onAjaxComplete() {
        var $porductColumn5 = $('.product_column5');
        if ($porductColumn5.length > 0) {
            $porductColumn5.owlCarousel({
                autoplay: false,
                loop: true,
                nav: true,
                autoplayTimeout: 8000,
                //items: 5,
                margin: 20,
                dots: false,
                navText: ['<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>'],
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    576: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    992: {
                        items: 4
                    },
                    1200: {
                        items: 5
                    }
                }
            });

            $('.action_links ul li a,.add_to_cart a,.footer_social_link ul li a').tooltip({
                animated: 'fade',
                placement: 'top',
                container: 'body'
            });
        }
    }

    return {
        init: init,
        config: config,
        OnAjaxBegin: onAjaxBegin,
        OnAjaxComplete: onAjaxComplete
    };
})(jQuery);