﻿
HomeModule = (function ($) {
    var self = {};
    function config(configs) {
        init();
    }
    
    function init() {
        $('[data-countdown]').each(function () {
            var $this = $(this), finalDate = $(this).data('countdown');
            $this.countdown(finalDate, function (event) {
                $this.html(event.strftime('<div class="countdown_area"><div class="single_countdown"><div class="countdown_number">%D</div><div class="countdown_title">ngày</div></div><div class="single_countdown"><div class="countdown_number">%H</div><div class="countdown_title">giờ</div></div><div class="single_countdown"><div class="countdown_number">%M</div><div class="countdown_title">phút</div></div><div class="single_countdown"><div class="countdown_number">%S</div><div class="countdown_title">giây</div></div></div>'));

            });
        });	
        refreshList();
    }

    function refreshList() {
        $('.shop_toolbar_btn > button').on('click', function (e) {

            e.preventDefault();

            $('.shop_toolbar_btn > button').removeClass('active');
            $(this).addClass('active');

            var parentsDiv = $('.shop_wrapper');
            var viewMode = $(this).data('role');


            parentsDiv.removeClass('grid_3 grid_4 grid_5 grid_list').addClass(viewMode);

            if (viewMode == 'grid_3') {
                parentsDiv.children().addClass('col-lg-4 col-md-4 col-sm-6').removeClass('col-lg-3 col-cust-5 col-12');

            }

            if (viewMode == 'grid_4') {
                parentsDiv.children().addClass('col-lg-3 col-md-4 col-sm-6').removeClass('col-lg-4 col-cust-5 col-12');
            }

            if (viewMode == 'grid_list') {
                parentsDiv.children().addClass('col-12').removeClass('col-lg-3 col-lg-4 col-md-4 col-sm-6 col-cust-5');
            }

        });
    }

    function loadTotalByDataType() {
        AppCommon.Utils.ShowLoading(".dev-total-by-datatype", true);
        AppCommon.Utils.AjaxCall({
            url: self.getTotalByDataTypeUrl,
            dataType: "html",
            type: "GET",
            data: {},
            success: function (resultData) {
                $("#dev-total-by-datatype").html(resultData);
                AppCommon.Utils.ShowLoading(".dev-total-by-datatype", false);
            }
        });
    }

    function onAjaxBegin() {
        AppCommon.Utils.ShowLoading("#dev-setting-all-box", true);
    }

    function onAjaxComplete() {
        AppCommon.Utils.ShowLoading("#dev-setting-all-box", false);
        refreshSetting(true);
        var msg = {
            status: AppCommon.SubmitStatus.Success,
            message: 'Update setting successfully.'
        };
        AppCommon.Utils.ShowMessage(msg);
    }

    function onContactUsBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
        console.log("Begin");
    }

    function onContactUsComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            $("input[name='Subject']").val("");
            $("textarea[name='Content']").val("");
            var message = e.responseJSON.message;
            $.toast({
                text: message,
                showHideTransition: 'slide', // fade, slide or plain
                allowToastClose: false,
                hideAfter: 2000,
                position: 'bottom-right',
                bgColor: '#40A944',
                textColor: '#fff',
                loader: false
            });
        }
        else {
            $.toast({
                text: "Lỗi hệ thống",
                showHideTransition: 'slide', // fade, slide or plain
                allowToastClose: false,
                hideAfter: 2000,
                position: 'bottom-right',
                bgColor: '#40A944',
                textColor: '#fff',
                loader: false
            });
        }
    }

    function onGetProductListBegin() {
    }

    function onGetProductListComplete(e) {
        refreshList();
    }

    return {
        init: init,
        config: config,
        OnAjaxBegin: onAjaxBegin,
        OnAjaxComplete: onAjaxComplete,
        OnContactUsBegin: onContactUsBegin,
        OnContactUsComplete: onContactUsComplete,
        OnGetProductListBegin: onGetProductListBegin,
        OnGetProductListComplete: onGetProductListComplete,
    };
})(jQuery);