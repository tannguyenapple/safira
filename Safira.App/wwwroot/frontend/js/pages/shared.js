﻿
GeneralModule = (function ($) {
    var self = {};
    function config(configs) {
        self.quickViewUrl = configs.quickViewUrl || "Product/QuickView";
        self.addToCartUrl = configs.addToCartUrl || "/Product/AddToCart";
        self.goToOrderUrl = configs.goToOrderUrl || "/Product/GoToOrder";
        self.goToCheckoutUrl = configs.goToCheckoutUrl || "/Order/CheckOut";
        self.removeOutCartUrl = configs.removeOutCartUrl || "/Product/RemoveOutCart";
        self.addToFavouriteUrl = configs.addToFavouriteUrl || "/Product/AddToFavourite";
        self.removeOutFavouriteUrl = configs.removeOutFavouriteUrl || "/Product/RemoveOutFavourite";
        self.getFavouriteUrl = configs.getFavouriteUrl || "/Product/GetFavourite";
        init();
    }
    
    function init() {
        refreshFavourite();
        refreshShoppingCart();

        //Remove popover after click the button
        $(document).on('click', '.dev-popover-button', function () {
            $(this).blur(); 
        });

        $(document).on('click', '.dev-share-to-facebook', function (e) {
            e.preventDefault();
            var id = $(this).data("product-id");
            var fileId = $(this).data("file-id");
            FB.ui(
                {
                    method: 'share',
                    href: 'https://www.choquehuong.vn/product/index/' + id,
                    picture: 'https://www.choquehuong.vn/file/get/' + fileId
                }, function (response) {
                    console.log(response);
                    if (response !== undefined) {
                        $.toast({
                            text: "Đã chia sẽ lên facebook thành công.",
                            showHideTransition: 'slide', // fade, slide or plain
                            allowToastClose: false,
                            hideAfter: 2000,
                            position: 'bottom-right',
                            bgColor: '#40A944',
                            textColor: '#fff',
                            loader: false
                        });
                    }
                });
        });

        $(document).on('click', '.dev-add-to-cart', function () {
            var id = $(this).data("product-id");
            var quantity = $(this).data("quantity") ?? $("#dev-add-to-cart-number").val();
            var title = $(this).data("title") ?? "";
            addToCart($(this),id, quantity, title, false);
        });

        $(document).on('click', '.dev-go-to-order', function () {
            var id = $(this).data("product-id");
            var quantity = $(this).data("quantity") ?? $("#dev-add-to-cart-number").val();
            var title = $(this).data("title") ?? "";
            goToOrder($(this), id, quantity, title);
        });

        $(document).on('click', '.dev-remove-out-cart', function () {
            var id = $(this).data("product-id");
            var reloadSum = $(this).data("reload-summary") ?? "0";
            removeOutCart(id, reloadSum);
        });

        $(document).on('change', '.dev-quantity-product-ip', function () {
            var id = $(this).data("product-id");
            var quantity = $(this).val();
            var title = $(this).data("title") ?? "";
            addToCart(null, id, quantity, title, true);
        });

        $(document).on('click', '.dev-add-to-favourite', function () {
            var id = $(this).data("product-id");
            var noReplace = $(this).data("no-replace") ?? "0";
            addToFavourite(id, noReplace);
        });

        $(document).on('click', '.dev-remove-out-favourite', function () {
            var id = $(this).data("product-id");
            removeOutFavourite(id);
        });

        $(document).on('click', '.dev-quick-view-product', function () {
            $("div[role='tooltip']").removeClass("show");
            var id = $(this).data("product-id");
            quickViewProduct(id);
            //$(this).tooltip();
        });

        $(document).on('click', '.dev-show-large-image', function () {
            var content = '<img src="' + $(this).data("picture-url")+'" alt="">';
            $(".dev-large-image-container").html(content);
        });
    }

    function refreshShoppingCart() {
        $("#dev-shopping-cart-form").submit();
    }

    function addToCart(e, id, quantity, title, replace) {
        if (e != null) {
            $(e).html('<i class="fa fa-spinner fa-spin"></i>' + title);
        }
        AppCommon.Utils.AjaxCall({
            url: self.addToCartUrl,
            dataType: "json",
            type: "POST",
            data: { id: id, quantity: quantity, replace: replace },
            success: function (_) {
                refreshShoppingCart();
                if (e != null) {
                    $(e).html('<span class="lnr lnr-cart"></span>' + title);
                }
                var message = "";
                if (replace === true) {
                    message = "Cập nhật sản phẩm giỏ hàng thành công."
                    $("#dev-shopping-cart-summary-form").submit();
                }
                else {
                    message = "Thêm sản phẩm vào giỏ hàng thành công";
                }
                $.toast({
                    text: message,
                    showHideTransition: 'slide', // fade, slide or plain
                    allowToastClose: false,
                    hideAfter: 2000,
                    position: 'bottom-right',
                    bgColor: '#40A944',
                    textColor: '#fff',
                    loader: false
                });
            }
        });

    }

    function goToOrder(e, id, quantity, title) {
        if (e != null) {
            $(e).html('<i class="fa fa-spinner fa-spin"></i>' + title);
        }
        AppCommon.Utils.AjaxCall({
            url: self.goToOrderUrl,
            dataType: "json",
            type: "POST",
            data: { id: id, quantity: quantity },
            success: function (_) {
                if (e != null) {
                    $(e).html('<span class="lnr lnr-cart"></span>' + title);
                }
                window.location.href = self.goToCheckoutUrl;
            }
        });
    }

    function removeOutCart(id, reloadSum) {
        AppCommon.Utils.AjaxCall({
            url: self.removeOutCartUrl,
            dataType: "json",
            type: "POST",
            data: { id: id },
            success: function () {
                refreshShoppingCart();
                if (reloadSum == 1) {
                    $("#dev-shopping-cart-summary-form").submit();
                }
            }
        });
    }

    function refreshFavourite() {
        AppCommon.Utils.AjaxCall({
            url: self.getFavouriteUrl,
            dataType: "json",
            type: "GET",
            data: {},
            success: function (rs) {
                console.log(rs);
                $("#dev-favourite-total").html(rs.value);
            }
        });
    }

    function addToFavourite(id, noReplace) {
        if (noReplace == 0) {
            $(".dev-add-to-favourite").html('<i class="fa fa-spinner fa-spin"></i>');
        }
        AppCommon.Utils.AjaxCall({
            url: self.addToFavouriteUrl,
            dataType: "json",
            type: "POST",
            data: { id: id },
            success: function (rs) {
                $("#dev-favourite-total").html(rs.value);
                if (noReplace == 0) {
                    $(".dev-add-to-favourite").html('<span class="lnr lnr-heart"></span>');
                }
                $.toast({
                    text: "Thêm sản phẩm vào danh sách yêu thích",
                    showHideTransition: 'slide', // fade, slide or plain
                    allowToastClose: false,
                    hideAfter: 2000,
                    position: 'bottom-right',
                    bgColor: '#40A944',
                    textColor: '#fff',
                    loader: false
                });
            }
        });
    }

    function removeOutFavourite(id) {
        console.log("remove out favourite: " + self.removeOutFavouriteUrl);
        AppCommon.Utils.AjaxCall({
            url: self.removeOutFavouriteUrl,
            dataType: "json",
            type: "POST",
            data: { id: id },
            success: function (rs) {
                $("#dev-favourite-total").html(rs.value);
                $("#dev-wish-list-form").submit();
            }
        });
    }

    function quickViewProduct(id) {
        $(".dev-quick-view-product").html('<i class="fa fa-spinner fa-spin"></i>');
        AppCommon.Utils.AjaxCall({
            url: self.quickViewUrl,
            dataType: "html",
            type: "GET",
            data: {id: id},
            success: function (content) {
                $(".dev-quick-view-product").html('<span class="lnr lnr-magnifier"></span>');
                $("#dev-quick-view-container").html(content);
                refreshSlider();
                
                $("#dev-quick-view-modal-box").modal();
                $(".dev-quick-view-product").blur();
            }
        });
    }

    function refreshSlider() {
        var $productNavactive = $('.product_navactive');
        if ($productNavactive.length > 0) {
            $('.product_navactive').owlCarousel({
                autoplay: true,
                loop: true,
                nav: true,
                autoplayTimeout: 8000,
                items: 4,
                dots: false,
                navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 1,
                    },
                    250: {
                        items: 2,
                    },
                    480: {
                        items: 3,
                    },
                    768: {
                        items: 4,
                    },

                }
            });
        } 
    }

    function showLargeImage(url) {
        var content = '<img src="' + url + '" alt="">';
        $(".dev-large-image-container").html(content);
    }

    function onAddReviewBegin() {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", true);
        console.log("Begin");
    }

    function onAddReviewComplete(e) {
        AppCommon.Utils.ShowLoading(".dev-ajax-form", false);
        if (e.responseJSON.status === AppCommon.SubmitStatus.Success) {
            $("textarea[name='Content']").val("");
            $("#dev-review-list-form").submit();
            var message = e.responseJSON.message;
            $.toast({
                text: message,
                showHideTransition: 'slide', // fade, slide or plain
                allowToastClose: false,
                hideAfter: 2000,
                position: 'bottom-right',
                bgColor: '#40A944',
                textColor: '#fff',
                loader: false
            });
        }
    }

    function onGetReviewListComplete() {
        var review = $("input[name='dev-review-number']").val();
        $("#dev-review-count-spn").html("(" + review + ")");
    }

    function onGetBlogListBegin() {
    }

    function onGetBlogListComplete() {
    }

    //dev-review-count-spn

    return {
        init: init,
        config: config,
        ShowLargeImage: showLargeImage,
        OnAddReviewBegin: onAddReviewBegin,
        OnAddReviewComplete: onAddReviewComplete,
        OnGetReviewListComplete: onGetReviewListComplete,
        OnGetBlogListBegin: onGetBlogListBegin,
        OnGetBlogListComplete: onGetBlogListComplete
    };
})(jQuery);