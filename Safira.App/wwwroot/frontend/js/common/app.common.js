﻿
var AppCommon = AppCommon || {};

AppCommon.SubmitStatus = {
    Success: "success",
    Error: "error",
    Info: "info",
    Warning: "warning"
};

AppCommon.Configs = {
    StandardEmailPatern: "^[A-Za-z0-9._%+-]*@[A-Za-z0-9.-]*\\.[A-Za-z0-9-]{2,}$",
    SpecialCharacter: /^[^\<\>]*$/,
    SummaryMessageId: "#Generic_SummaryMessage",
    MessageSuccessClassName: "alert-success",
    MessageErrorClassName: "alert-danger",
    ON_KEYUP_AFTER_CALBACK: "OnKeyPressAfterCallback",
    DateTimeFormat: "",
    LoginUrl: "/account/login",
    VAT: 10
};

AppCommon.Utils = (function ($) {

    function ajaxCall(options) {
        if (options.isLocked === undefined)
            options.isLocked = false;

        if (options.isLocked) {
            var target = "body";
            if (options.target === undefined) {
                options.target = target;
            }

            showLoading(options.target, true);
            doCallAjax(options);

        } else {
            doCallAjax(options);
        }
    }

    function doCallAjax(options) {
        var reloadWhenError = true;
        if (options.IsReloadWhenError !== undefined) {
            reloadWhenError = options.isReloadWhenError;
        }

        $.ajax({
            url: options.url,
            type: options.type || "POST",
            contentType: options.contentType || "application/x-www-form-urlencoded; charset=UTF-8",
            dataType: options.dataType || "json",
            data: options.data,
            cache: false,
            async: options.async === false ? false : true,
            beforeSend: function (xhr) {
                if (options.beforeSend && typeof options.beforeSend == "function") {
                    options.beforeSend(xhr);
                } else {
                    //xhr.setRequestHeader("ZOZO-XSRF-TOKEN",
                    //$('input:hidden[name="__RequestVerificationToken"]').val());
                }
            },
            success: function (resultData, status, xhr) {
                if (options.success && typeof options.success == "function") {
                    options.success(resultData);
                }
            },
            error: function (xhr, status, errorThrown) {
                var message = "";
                switch (xhr.status) {
                    case 401:
                        message = '401 Unauthorized access detected. Please check the credentials you entered.' + errorThrown;
                        window.location.replace(AppCommon.Configs.LoginUrl);
                        break;
                    case 500:
                        message = '500 Internal Server Error. Please check the service code.' + errorThrown;
                        break;
                    default:
                        message = 'Unexpected error: ' + errorThrown;
                        break;
                }

                var result = { success: false, message: message };
                if (options.error && typeof options.error == "function") {
                    options.error(result);
                }
            },
            complete: function () {
                if (options.complete && typeof options.complete == "function") {
                    options.complete();
                }
            }
        });
    }

    function showLoading(container, flag) {
        if (flag) {
            var loading = $('<div class="overlay dev-loading-panel"><i class= "fa fa-spinner fa-spin" ></i ></div>');
            $(container).append(loading);
        }
        else {

            $(container).find(".dev-loading-panel").each(function () {
                $(this).remove();
            });
        }
    };

    function showLoadingIcon(container, flag) {
        var $ele = $(container);
        var cssClass = 'loading';

        if ($(container).is('select')) {
            $ele = $(container).next();
            cssClass = 'loading-data';
        }
        if (flag) {
            $ele.addClass(cssClass);
        } else {
            $ele.removeClass(cssClass);
        }
    };

    function validationChecker(elem) {
        var id = $(elem).attr('id');
        var parentId = $(elem).parent().attr('id');
        var selVal = $('#select2-' + id.replace('.', '') + '-container').text();
        if (selVal.indexOf('select') > 0) {
            $('#' + parentId + ' .field-validation-error').show();
        } else {
            $('#' + parentId + ' .field-validation-error').hide();
        }
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };

    function targetPace(el) {
        Pace.restart();
        if (el == 'body') {
            return;
        }

        $(el).addClass("fadeIn fadeOut");
        $('body').addClass('pace-obj');
        var wth;
        if ($(el).width() > 1000)
            wth = ($(el).width() / 2) - 142;
        else
            wth = ($(el).width() / 2) - 100;

        var lp = $(el).offset().left + wth;
        $('.pace').css('left', lp + 'px');
    }

    function activeLeftMenu(url) {
        $('[data-menu]').each(function () {
            var item = '/' + $(this).data('menu');
            if (url.includes(item)) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }

    //Check only enter number only

    function checkOnlyNumberWithDot(e, iCountAfterDot, isformat) {
        if (isformat === undefined) {
            isformat = true;
        }

        if ((e.which >= "48" && e.which <= "57" || e.which == "8" || e.which == "0" || e.which == "13" || e.which == "46")) {
            var decimalIndex = e.target.value.indexOf('.');

            if (e.which == "46") {
                if (decimalIndex > -1)
                    return false;
                else
                    return true;
            }
            if (iCountAfterDot !== undefined) {
                if (decimalIndex > -1) {
                    //check whether user put cursor before decimalIndex or not
                    if (typeof e.target.selectionStart == 'number') {
                        if (e.target.selectionStart <= decimalIndex || isTextSelected(e.target)) { //before
                            return true;
                        }
                    }
                    var decimals = e.target.value.substring(decimalIndex, e.target.value.length);
                    if (decimals.length > iCountAfterDot) {
                        return false;
                    }
                }
            }

            if (typeof e.target.selectionStart == 'number') {
                if (e.target.selectionStart <= decimalIndex || isTextSelected(e.target)) { //before
                    return true;
                }
            }

            if (isformat === true) {
                setTimeout(function () {
                    var temp = format_number(e.target.value);
                    e.target.value = (temp == 0) ? "" : temp;
                }, 10);
            }
            return true;
        }
        return false;
    }

    function checkOnlyNumber(e) {
        if ((e.which >= "48" && e.which <= "57" || e.which == "8" || e.which == "0" || e.which == "13")) {
            return true;
        }
        return false;
    }

    //check paste data is numeric
    function onPasteTextboxNumeric(sender, iCount) {
        setTimeout(function () {
            var value = parseFloat(sender.value);
            if (iCount > 0) {
                if (isNaN(value)) {
                    sender.value = '';
                } else {
                    sender.value = value.toString();
                    var decimalIndex = sender.value.indexOf('.');
                    if (decimalIndex > -1) {
                        var decimals = sender.value.substring(decimalIndex, sender.value.length);
                        if (decimals.length > (iCount + 1)) {
                            sender.value = sender.value.substring(0, decimalIndex) + decimals.substring(0, (iCount + 1));
                        }
                    }
                }
            } else {
                if (isNaN(value)) {
                    sender.value = '';
                } else {
                    sender.value = value.toString();
                    if (sender.value.indexOf('.') > -1) {
                        sender.value = sender.value.replace('.', '');
                    }
                }
            }
        }, 4);
    }

    function isTextSelected(input) {
        if (typeof input.selectionStart == "number") {
            return input.selectionStart != input.selectionEnd && input.selectionStart != input.value.length;
        }
        return false;
    }

    function onInitControlStyle(parentId) {
        parentId = parentId || '';
        initEventForNumbericInput();
        initSingleDatePicker(parentId);
        initSelectUI(parentId);
    };

    $.fn.makeCapitalizing = function () {
        $(this).each(function () {
            $(this).val($(this).val().capitalize());
            $(this).on("change paste", function () {
                $(this).val($(this).val().capitalize());
            });
        });
    };

    window.makeCapitalizing = function (sender) {
        var value = $(sender).val();
        if (value) {
            $(sender).val(value.capitalize());
        }
    };

    return {
        AjaxCall: ajaxCall,
        ShowLoading: showLoading,
        ShowLoadingIcon: showLoadingIcon,
        ValidationChecker: validationChecker,
        ActiveLeftMenu: activeLeftMenu,
        MakeCapitalizing: function (selector) {
            if (!selector) selector = ".text-captialize";
            $(selector).makeCapitalizing();
        },
        LockUI: function ($ele, isLocked) {
            var $lockedEle = $ele && $ele.length > 0 ? $ele : $("html");
            if (!$("html").is($lockedEle) && $lockedEle.css("position") !== "relative") {
                $lockedEle.css("position", "relative");
            }
        },
        Guid: guid,
        CheckOnlyNumberWithDot: checkOnlyNumberWithDot,
        CheckOnlyNumber: checkOnlyNumber,
        OnPasteTextboxNumeric: onPasteTextboxNumeric,
        OnInitControlStyle: onInitControlStyle,
    }
})(jQuery);

function OnAjaxPopupBegin(target) {
    $(target).parent(".modal").addClass("loading-obj");
}

function OnAjaxPopupComplete(target) {
    AppCommon.Utils.ShowMessage(target.responseJSON);
}

function OnAjaxBeginCommon() {
    if ($('#hidLoadingBtn').val() !== undefined && $('#hidLoadingBtn').val() !== "") {
        var btnClass = $('#hidLoadingBtn').val();
        AppCommon.Utils.ShowLoadingIcon('.' + btnClass, true);
    } else {
        AppCommon.Utils.ShowLoadingIcon('.btn-action', true);
    }
}

function OnAjaxCompleteCommon(target) {
    var responseJson = target.responseJSON;
    AppCommon.Utils.ShowMessage(responseJson);
    AppCommon.Utils.ShowLoadingIcon('.btn-action', false);
    if (responseJson.status === AppCommon.SubmitStatus.Success) {
        var linkToGo = $("#hidGoto").val();
        if (linkToGo.indexOf("parametervalue") > 0) {
            linkToGo = linkToGo.replace("parametervalue", responseJson.data);
        }
        if (linkToGo !== "") {
            setTimeout(function () { location.href = linkToGo; }, 1000);
        }
        // for hotel edit page
        if ($('#StatusId').val() !== undefined) {
            var hotelStatusId = $('#StatusId').val();
            if ($('#hidHotelStatusId').val() !== undefined) {
                $('#hidHotelStatusId').val(hotelStatusId);
                $('#hidCurrentHotelStatusId').val(hotelStatusId);
            }
        }
    }
    else if (responseJson.status === AppCommon.SubmitStatus.Error) {
        if ($('#StatusId').val() !== undefined) {
            var currentStatusId = $('#hidCurrentHotelStatusId').val();
            if (currentStatusId !== undefined) {
                //$("#StatusId option[value='" + currentStatusId + "']").prop('selected', true);
                $('#StatusId').val(currentStatusId).trigger('change');
            }
        }
    }
}

function dismissProver() {
    $('body').on('click', function (e) {
        //did not click a popover toggle or popover
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });
}

function format_number(number, decimals) {
    if (number === undefined || number === "") {
        return number;
    }

    if (decimals == undefined) {
        decimals = 5;
    }

    var dec_point = '.';
    var thousands_sep = ',';
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length > prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    // Add this number to the element as text.
    //this.text(s.join(dec));
    var temp = s.join(dec)
    if (temp == "0")
        temp = "";
    return temp;
    //return s.join(dec);
}

function initEventForNumbericInput() {
    //money
    //$(document).on('keypress', '[data-type="money"]', function (e) {
    //    return AppCommon.Utils.CheckOnlyNumberWithDot(e, 5);
    //});

    //$(document).on('paste', '[data-type="money"]', function (e) {
    //    $(window).trigger(AppCommon.Configs.ON_KEYUP_AFTER_CALBACK, { e });
    //    return AppCommon.Utils.OnPasteTextboxNumeric(e.target, 5);
    //});

    ////percent
    //$(document).on('keypress', '[data-type="percent"]', function (e) {
    //    return AppCommon.Utils.CheckOnlyNumberWithDot(e, 2, false);
    //});

    //$(document).on('paste', '[data-type="percent"]', function (e) {
    //    return AppCommon.Utils.OnPasteTextboxNumeric(e.target, 2, false);
    //});

    //number
    //$(document).on('keypress', '[data-type="number"]', function (e) {
    //    return AppCommon.Utils.CheckOnlyNumber(e);
    //});

    //$(document).on('paste', '[data-type="number"]', function (e) {
    //    $(window).trigger(AppCommon.Configs.ON_KEYUP_AFTER_CALBACK, { e });
    //    return AppCommon.Utils.OnPasteTextboxNumeric(e.target, 0);
    //});

    setTimeout(function () {
        $('[data-type="money"]').each(function (i) {
            var formated = format_number($(this).val(), 5);
            $(this).val(formated);
        });
    }, 50);

    $('[data-type="money"]').inputmask({
        alias: 'decimal',
        groupSeparator: ',',
        autoGroup: true,
        digits: 5,
        rightAlign: false,
        digitsOptional: true
    });

    $('[data-type="percent"]').inputmask({
        alias: 'decimal',
        groupSeparator: '',
        autoGroup: true,
        digits: 2,
        rightAlign: false,
        digitsOptional: true
    });
    $('[data-type="number"]').inputmask({
        alias: 'decimal',
        groupSeparator: '',
        autoGroup: true,
        digits: 0,
        rightAlign: false,
        digitsOptional: true
    });

    Vue.directive('mask', getInputMask());
}

function initSingleDatePicker(parentId) {
    $(parentId + ' .date-picker').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        locale: {
            format: 'DD/MM/YYYY',
            cancelLabel: 'Clear'
        }
    });

    $(parentId + ' .date-picker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });

    $(document).on('change', parentId + ' .date-picker', function () {
        if (isValidDate($(this).val(), "DD/MM/YYYY") === false)
            $(this).val('');
    });
}

function initDigitNumber() {
    $(document).on('keypress', '[data-type="digit"]', function (e) {
        return AppCommon.Utils.CheckOnlyNumber(e);
    });

    $(document).on('paste', '[data-type="digit"]', function (e) {
        return AppCommon.Utils.OnPasteTextboxNumeric(e.target, 0);
    });
}

function initSelectUI(parentId) {
    parentId = parentId || '';

    iconFormat = function (icon) {
        var originalOption = icon.element;
        return '<i class="' + $(originalOption).data('icon') + '"></i>' + icon.text;
    };

    $(parentId + ' .select-icons').select2({
        minimumResultsForSearch: -1,
        templateSelection: iconFormat,
        templateResult: iconFormat,
        escapeMarkup: function (markup) {
            return markup;
        }
    });

    $(parentId + ' .select').select2({
        minimumResultsForSearch: -1,
        dropdownAutoWidth: true
    });

    $(parentId + ' .selectwithsearch').select2({
        dropdownAutoWidth: true
    });

    //$('select').select2({}).on("change", function (e) {
    //    $(this).valid()
    //});
}

function getInputMask() {
    return {
        bind: function (el, binding) {

            var maskOpts = binding.value;

            maskOpts.showMaskOnHover = false;
            maskOpts.autoUnmask = true;
            maskOpts.clearIncomplete = true;
            maskOpts.alias = maskOpts.alias || 'currency';
            maskOpts.prefix = maskOpts.prefix || '';
            maskOpts.digitsOptional = maskOpts.digitsOptional || true;
            maskOpts.digits = maskOpts.digits || 5;
            maskOpts.rightAlign = maskOpts.rightAlign || false;
            maskOpts.placeholder = maskOpts.placeholder || '';
            Inputmask(maskOpts).mask(el);
        },
        unbind: function (el) {
            Inputmask.remove(el);
        }
    };
}

function isValidDate(dateStr, format) {
    format = format || "DD/MM/YYYY";

    //delimiter = /[^MDY]/.exec(format)[0];
    var formats = format.split("/");
    var values = dateStr.split("/");

    var m, d, y;
    for (var i = 0; i < formats.length; i++) {
        if (/M/.test(formats[i])) m = values[i];
        if (/D/.test(formats[i])) d = values[i];
        if (/Y/.test(formats[i])) y = values[i];
    }
    var date = new Date(y + '/' + m + '/' + d);
    if (date.toString() === "Invalid Date")
        return false;
    return true;
    //return (m > 0 && m < 13 && y && y.length === 4 && d > 0 && d <= (new Date(y, m, 0)).getDate());
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});