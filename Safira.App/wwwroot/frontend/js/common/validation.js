﻿$.validator.methods.range = function (value, element, param) {
    var globalizedValue = value.replace(/\,/g, "");
    return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
}

$.validator.methods.number = function (value, element) {
    var globalizedValue = value.replace(/\,/g, "");
    return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(globalizedValue);
}

$.validator.addMethod('requiredifanotherhasvalue',
    function (value, element, params) {
        //Detect whether this element is in a list mode or not, id yes, will get the ref property based its name
        //If in list the name should be like this ObjectOfList[0].Name
        var $refProperty = undefined;
        var regex = /\[+[0-9]+\]+\./g;
        var name = element.name;
        var isInList = regex.test(name);
        var temp = params.refproperty;
        //if (!isInList) {
        //if (1 !== 1) {
        //    $refProperty = GetControlProperty(temp);
        //} else {
            temp = name.split('.')[0] + '.' + params.refproperty;
            $refProperty = $('input[name="' + temp + '"]');
        //}

        if ($refProperty.length > 0) {
            if ($refProperty.val() != undefined && $refProperty.val().trim().length > 0) {
                return value.trim().length > 0;
            }
        }
        return true;
    });

$.validator.unobtrusive.adapters.add('requiredifanotherhasvalue', ['refproperty'], function (options) {
    var params = {
        refproperty: options.params.refproperty
    };
    options.rules['requiredifanotherhasvalue'] = params;
    options.messages['requiredifanotherhasvalue'] = options.message;
});

//
$.validator.addMethod('requiredifanotherhasspecificvalue',
    function (value, element, params) {
        //Detect whether this element is in a list mode or not, id yes, will get the ref property based its name
        //If in list the name should be like this ObjectOfList[0].Name
        var $refProperty = undefined;
        var regex = /\[+[0-9]+\]+\./g;
        var name = element.name;
        var isInList = regex.test(name);
        var temp = params.refproperty;
        //if (!isInList) {
        //    $refProperty = GetControlProperty(temp);
        //} else {
            temp = name.split('.')[0] + '.' + params.refproperty;
            $refProperty = $('input[name="' + temp + '"]');
        //}

        if ($refProperty.length > 0) {
            var obj = $refProperty[0];
            if (obj.type === "checkbox") {
                var isTrue = params.refvalue === 'true';
                if (obj.checked !== isTrue)
                    return true;
                else {
                    return value.trim().length > 0;
                }
            }
            else if ($refProperty.val().length > 0 && $refProperty.val().toLowerCase() == params.refvalue) {
                return value.trim().length > 0;
            }
        }
        return true;
    });

$.validator.unobtrusive.adapters.add('requiredifanotherhasspecificvalue', ['refproperty', 'refvalue'], function (options) {
    var params = {
        refproperty: options.params.refproperty,
        refvalue: options.params.refvalue
    };
    options.rules['requiredifanotherhasspecificvalue'] = params;
    options.messages['requiredifanotherhasspecificvalue'] = options.message;
});

//
$.validator.addMethod('requiredifanothergreaterthanspecificvalue',
    function (value, element, params) {
        //Detect whether this element is in a list mode or not, id yes, will get the ref property based its name
        //If in list the name should be like this ObjectOfList[0].Name
        var $refProperty = undefined;
        var regex = /\[+[0-9]+\]+\./g;
        var name = element.name;
        var isInList = regex.test(name);
        if (!isInList) {
            $refProperty = $("#" + params.refproperty);
        } else {
            var temp = name.split('.')[0] + '.' + params.refproperty;
            $refProperty = $('input[name="' + temp + '"]');
        }

        if ($refProperty.length > 0) {
            if ($refProperty.val() > params.refvalue)
                return value.trim().length > 0;
        }
        return true;
    });

$.validator.unobtrusive.adapters.add('requiredifanothergreaterthanspecificvalue', ['refproperty', 'refvalue'], function (options) {
    var params = {
        refproperty: options.params.refproperty,
        refvalue: options.params.refvalue
    };
    options.rules['requiredifanothergreaterthanspecificvalue'] = params;
    options.messages['requiredifanothergreaterthanspecificvalue'] = options.message;
});

function GetControlProperty(temp) {
    var $el = $('input[name="' + temp + '"]');

    if ($el.length === 0) {
        el = $('#' + temp);
    }

    var type = $el.attr('type');

    if (type === 'text' || type === 'number') {
        return $el;
    }
    else if (type === 'radio')
    {
        return $('input[name="' + temp + '"]:checked');
    }
    return $el;
}
