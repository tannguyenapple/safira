﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Safira.App.Extensions;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Blog;

namespace Safira.App.Controllers
{
    public class BlogController : BaseController
    {
        #region Fields & Ctor

        private readonly IBlogReviewService _reviewService;
        private readonly IBlogService _blogService;
        private readonly ApplicationSettings _appSettings;

        public BlogController(
            IBlogReviewService blogReviewService,
            IBlogService blogService,
            ApplicationSettings applicationSettings)
        {
            _reviewService = blogReviewService;
            _blogService = blogService;
            _appSettings = applicationSettings;
        }


        #endregion
        public async Task<IActionResult> Index(int id)
        {
            var filtering = new BlogFilteringModel
            {
                CategoryId = id,
                PageSize = 12
            };
            var model = await GetListViewModel(filtering);
            return View(model);
        }

        public async Task<IActionResult> Detail(int id = 1, string name = "")
        {
            var model = await _blogService.GetByIdAsync(id);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetBlogList(BlogFilteringModel filtering)
        {
            return PartialView("_BlogList", await GetListViewModel(filtering));
        }

        [HttpPost]
        public async Task<IActionResult> AddReview(BlogReviewModel model)
        {
            var result = new Domain.Result.Result<int>()
            {
                ResultType = Domain.Result.ResultType.Error
            };
            if (ModelState.IsValid)
            {
                result = await _reviewService.SaveAsync(model);
            }
            return JsonResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetReviewList(long id)
        {
            var model = await _reviewService.GetListAsync(id);
            return PartialView("_ReviewList", model);
        }

        [HttpPost]
        public async Task<IActionResult> GetRecentReviewList()
        {
            var model = await _reviewService.GetRecentListAsync();
            return PartialView("_RecentReviewList", model);
        }

        [HttpPost]
        public async Task<IActionResult> GetRelatedBlogList(int id, int categoryId)
        {
            var products = await _blogService.GetAllAsync(categoryId);
            if (products.Any())
            {
                //products = products.Where(t => t.Id != id).OrderByDescending(t => t.ModifiedDate).Take(6).ToList();
                products = products.OrderByDescending(t => t.ModifiedDate).Take(6).ToList();
            }
            return PartialView("_RelatedBlogList", products);
        }

        [HttpPost]
        public async Task<IActionResult> GetRecentBlogList()
        {
            var products = await _blogService.GetRecentAsync();
            
            return PartialView("_RecentBlogList", products);
        }

        private async Task<BlogListViewModel> GetListViewModel(BlogFilteringModel filtering)
        {
            filtering.PageSize = _appSettings.DefaultPageSize;
            var products = await _blogService.GetListAsync(filtering);
            return new BlogListViewModel(products, filtering)
            {
                PageSizeOptions = _appSettings.PageSizeOptions.ToDicSelectList(filtering.PageSize.ToString(), false),
            };
        }
    }
}
