﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Safira.Domain.Const;
using Safira.Domain.Models;
using Safira.Domain.Models.Account;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Controllers
{
    public class AccountController : BaseController
    {
        #region Fields & Ctor
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly Messages _message;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            Messages messages)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _message = messages;
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            if (!_signInManager.IsSignedIn(User))
                return RedirectToAction(nameof(Login));
            return View();
        }

        public IActionResult Login(string returnUrl = null)
        {
            _signInManager.SignOutAsync();

            ViewData["ReturnUrl"] = returnUrl == "/" ? "" : returnUrl;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewData["ReturnUrl"] = returnUrl;
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //Check the role of user
                var user = await _userManager.FindByEmailAsync(model.Username);
                if (user != null)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    if (roles.Any(t => t == MembershipRole.Customer))
                    {
                        var loginResult = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, lockoutOnFailure: false);
                        if (loginResult.Succeeded)
                        {
                            var sign = _signInManager.IsSignedIn(User);
                            var temp = User.Identity.IsAuthenticated;
                            if (string.IsNullOrEmpty(returnUrl))// || !Url.IsLocalUrl(returnUrl))
                            {
                                return RedirectToAction("index", "home");
                            }
                            else
                            {
                                return Redirect(returnUrl);
                            }
                        }
                    }
                }
            }

            ModelState.AddModelError("", _message.Username_Or_Password_Invalid);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Username);
                if (user is null)
                {
                    var now = DateTime.UtcNow;
                    var customer = new ApplicationUser
                    {
                        UserName = model.Username,
                        Email = model.Username,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PhoneNumber = model.PhoneNumber,
                        Address = model.Address,
                        CreateDate = now,
                        ModifyDate = now,
                        StatusId = 1
                    };
                    var result = await _userManager.CreateAsync(customer, model.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(customer, MembershipRole.Customer);
                        var loginResult = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, lockoutOnFailure: false);
                        if (loginResult.Succeeded)
                        {
                            return JsonResult(new Domain.Result.Result<bool>
                            {
                                ResultType = Domain.Result.ResultType.Success,
                            });
                            //if (string.IsNullOrEmpty(returnUrl))// || !Url.IsLocalUrl(returnUrl))
                            //{
                            //    return RedirectToAction("index", "home");
                            //}
                            //else
                            //{
                            //    return Redirect(returnUrl);
                            //}
                        }
                    }
                }
            }

            return JsonResult(new Domain.Result.Result<bool>
            {
                ResultType = Domain.Result.ResultType.Error,
                Message = _message.Email_Have_Been_Exist
            });
        }

        public IActionResult Logout()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction(nameof(Login));
        }

        #endregion
    }
}
