﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safira.App.Extensions;
using Safira.App.Models;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Home;
using Safira.Domain.Models.Product;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Safira.App.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger _logger;
        private readonly IProductService _productService;
        private readonly IMessageService _messageService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationSettings _appSettings;

        public HomeController(
            ILogger<HomeController> logger,
            IProductService productService,
            IMessageService messageService,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationSettings appSettings)
        {
            _logger = logger;
            _productService = productService;
            _messageService = messageService;
            _signInManager = signInManager;
            _userManager = userManager;
            _appSettings = appSettings;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Menu = MenuEnum.Home;
            var model = await _productService.GetAllAsync();
            return View(model);
        }

        //public async Task<IActionResult> Category(int id)
        //{
        //    var filtering = new ProductFilteringModel
        //    {
        //        CategoryId = id,
        //        SearchKeyword = string.Empty,
        //        PageSize = 12
        //    };
        //    var model = await GetListViewModel(filtering);
        //    return View(model);
        //}

        public async Task<IActionResult> Category(int id, string keyword = "")
        {
            var filtering = new ProductFilteringModel
            {
                CategoryId = id,
                SearchKeyword = keyword,
                PageSize = 12
            };
            var model = await GetListViewModel(filtering);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> GetProductList(ProductFilteringModel filtering)
        {
            return PartialView("_ProductList", await GetListViewModel(filtering));
        }

        public async Task<IActionResult> ContactUs()
        {
            var model = new MessageViewModel();
            if (_signInManager.IsSignedIn(User))
            {
                var user = await _userManager.GetUserAsync(User);
                model.Email = user.Email;
                model.FullName = $"{user.FirstName} {user.LastName}";
            }
            ViewBag.Menu = MenuEnum.ContactUs;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ContactUs(MessageViewModel model)
        {
            var result = new Domain.Result.Result<int>()
            {
                ResultType = Domain.Result.ResultType.Error
            };
            if (ModelState.IsValid)
            {
                result = await _messageService.SaveAsync(model);
            }
            return JsonResult(result);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task<ProductListViewModel> GetListViewModel(ProductFilteringModel filtering)
        {
            var products = await _productService.GetListAsync(filtering, true);
            return new ProductListViewModel(products, filtering)
            {
                PageSizeOptions = _appSettings.PageSizeOptions.ToDicSelectList(filtering.PageSize.ToString(), false),
            };
        }
    }
}
