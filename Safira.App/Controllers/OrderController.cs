﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Order;
using Safira.Domain.Models.Product;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safira.App.Controllers
{
    public class OrderController : ShoppingController
    {
        #region Fields & Ctor

        private readonly IOrderService _orderService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailService _emailService;
        private readonly IEmailSender _emailSender;
        private readonly Messages _message;
        private readonly ApplicationSettings _appSettings;

        public OrderController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IProductService productService,
            ICartService cartService,
            IOrderService orderService,
            IEmailService emailService,
            IEmailSender emailSender,
            Messages messages,
            ApplicationSettings appSettings) 
            : base(userManager, signInManager, productService, cartService)
        {
            _orderService = orderService;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
            _emailSender = emailSender;
            _message = messages;
            _appSettings = appSettings;
        }

        #endregion

        #region Methods

        public IActionResult Cart()
        {
            var model = GetCart();
            return View(model);
        }

        [HttpGet]
        public IActionResult GetCartList()
        {
            var model = GetCart();
            return PartialView("_CartList", model);
        }

        public async Task<IActionResult> Checkout()
        {
            var product = GetCart();
            var model = new CheckoutViewModel()
            {
                Products = product
            };
            if (_signInManager.IsSignedIn(User))
            {
                var user = await _userManager.GetUserAsync(User);
                model.Email = user.UserName;
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                model.PhoneNumber = user.PhoneNumber;
                model.Address = user.Address;
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Checkout(CheckoutViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Products = GetCart();
                var result = await _orderService.SaveOrder(model);
                if (result.ResultType == Domain.Result.ResultType.Success)
                {
                    var order = await _orderService.GetByIdAsync(result.Value);
                    await SendEmail(order);
                    return RedirectToAction(nameof(Thankyou));
                }
                ModelState.AddModelError("", result.Message);
            }
            else
                ModelState.AddModelError("", _message.Email_Have_Been_Exist);

            return View(model);
        }

        public IActionResult Thankyou()
        {
            ClearCart();
            return View();
        }

        public IActionResult WishList()
        {
            var model = GetFavourites();
            return View(model);
        }

        [HttpGet]
        public IActionResult GetWishList()
        {
            var model = GetFavourites();
            return PartialView("_WishList", model);
        }

        #endregion

        private async Task SendEmail(CheckoutViewModel model)
        {
            var content = GetEmailContent(model.Products);
            var email = new EmailModel
            {
                Subject = $"Đơn hàng {model.OrderNumber}",
                Content = content,
                RecipientEmail = model.Email,
                RecipientName = model.FullName,
                OrderId = model.Id
            };
            var result = await _emailService.SaveAsync(email);
            if(result.ResultType == Domain.Result.ResultType.Success)
            {
                //Send email to customer
                await _emailSender.SendEmailAsync(email.RecipientEmail, email.Subject, email.Content);

                //Send email to admin
                await _emailSender.SendEmailAsync(_appSettings.AdminEmails.Split(",").ToList(), email.Subject, email.Content);
            }
        }

        private string GetEmailContent(List<ProductCartViewModel> products)
        {
            var content = new StringBuilder();
            foreach(var item in products)
            {
                var sub = @$"<tr style='page-break-inside: avoid;'>
							<td width='204' align='center' style='border-top: 1px solid #000; padding: 13px 0;'>
								<strong style='font-family: Arial, sans-serif; font-size: 13px;'>
									[PRODUCT_NAME]
								</strong>
							</td>
							<td colspan='2' style='border-left: 1px solid #000; border-top: 1px solid #000; padding: 13px 15px; '>
								<span style='font-family: Arial, sans-serif; font-size: 13px;'>
									<strong>[PRODUCT_PRICE]</strong> x <strong>[PRODUCT_QUANTITY]</strong>
								</span>
							</td>
							<td colspan='3' style='border-left: 1px solid #000; border-top: 1px solid #000; padding: 13px 15px; '>
								<span style='font-family: Arial, sans-serif; font-size: 13px; white-space: nowrap;'>
									<strong>[PRODUCT_TOTAL]</strong>
								</span>
							</td>
						</tr>";
                content.Append(sub.Replace("[PRODUCT_NAME]", item.Name)
                    .Replace("[PRODUCT_PRICE]", item.Price.ToString("C0", System.Globalization.CultureInfo.CurrentCulture))
                    .Replace("[PRODUCT_QUANTITY]", item.Quantity.ToString())
                    .Replace("[PRODUCT_TOTAL]", (item.Quantity * item.Price).ToString("C0", System.Globalization.CultureInfo.CurrentCulture)));
            }

            var subTotal = @"<tr style='page-break-inside: avoid;'>
							<td colspan='3' width='104' align='center' style='border-top: 1px solid #000; padding: 13px 0;'>
								<strong style='font-family: Arial, sans-serif; font-size: 13px;'>
									Tổng cộng
								</strong>
							</td>
							<td colspan='3' style='border-left: 1px solid #000; border-top: 1px solid #000; padding: 13px 15px; '>
								<span style='font-family: Arial, sans-serif; font-size: 13px; white-space: nowrap;'>
									<strong>[ORDER_TOTAL]</strong>
								</span>
							</td>
						</tr>";
            content.Append(subTotal.Replace("[ORDER_TOTAL]", products.Sum(t => t.Price * t.Quantity).ToString("C0", System.Globalization.CultureInfo.CurrentCulture)));
            var body = @"<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html charset=UTF-8' />
</head>
<body style='margin: 0;'>
    <div style='font-family: Arial, sans-serif; font-size: 13px; color: #000;width: 650px; margin: 0 auto;'>
        <table border='0' align='center' cellpadding='0' cellspacing='0' style='width: 650px; margin: 0 auto; border-spacing: 0; background-color: white;'>
            <tr>
                <td>
                    <p style='font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; margin: 0 0 20px 0;'>
                        <strong>Xin chào,</strong>
                    </p>
                    <p style='font-family: Arial, sans-serif; font-size: 14px;line-height: 20px; margin: 0 0 10px 0;'>
                        Cảm ơn bạn đã tin tưởng và ủng hộ chúng tôi. <br />
                        Chúng tôi sẽ liên hệ với bạn để xác nhận đơn hàng.
                    </p>
                    <p style='font-family: Arial, sans-serif; font-size: 14px;line-height: 20px; margin: 0 0 35px 0;'>
                        Nếu bạn có gì muốn tìm hiểu thêm, hãy liên hệ tới chúng tôi:
                        <a href='mailto:operationvn@ohmyhotel.com' style='color: #000; text-decoration: none'><span style='text-decoration: underline;'>admin@choquehuong.vn</span></a>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
					<table border='0' cellpadding='0' cellspacing='0' style='height: 100%; width: 100%; border-spacing: 0; padding: 0; border-collapse: collapse; border: 1px solid #000;'>
						<tr style='page-break-inside: avoid;'>
							<td colspan='2' bgcolor='#f2f2f2' align='left' style='border-top: 1px solid #000; height: 40px; padding-left: 15px;'>
								<strong style='font-family: Arial, sans-serif; font-size: 14px;'>
								Đơn hàng
								</strong>
							</td>
							<td bgcolor='#f2f2f2' colspan='4' align='right' style='border-top: 1px solid #000; padding-right: 15px; '>
								
							</td>
						</tr>
						[CONTENT_EMAIL]
					</table>
                                        
				</td>
            </tr>
			<tr>
                <td align='center' style='font-family: Arial, sans-serif; color: #555; padding-bottom: 20px'></td>
            </tr>
            <tr>
                <td align='center' style='font-family: Arial, sans-serif; color: #555; padding-bottom: 10px'>Copyright &copy; choquehuong.vn</td>
            </tr>
        </table>
    </div>
</body>
</html>";
            return body.Replace("[CONTENT_EMAIL]", content.ToString());
        }
    }
}
