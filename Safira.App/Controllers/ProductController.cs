﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Safira.Domain.Enums;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Product;
using Safira.Domain.Result;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Controllers
{
    public class ProductController : ShoppingController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IProductService _productService;
        private readonly IReviewService _reviewService;

        public ProductController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IProductService productService,
            IReviewService reviewService,
            ICartService cartService) : base(userManager,signInManager, productService, cartService)
        {
            _productService = productService;
            _userManager = userManager;
            _signInManager = signInManager;
            _reviewService = reviewService;
        }

        public async Task<IActionResult> Index(long id, string name="")
        {
            ViewBag.Menu = MenuEnum.Cuisine;
            var model = await _productService.GetByIdAsync(id);
            return View(model);
        }

        #region Shopping Cart

        [HttpGet]
        public async Task<IActionResult> QuickView(long id)
        {
            var model = await _productService.GetByIdAsync(id);
            return PartialView("_QuickView", model);
        }

        [HttpGet]
        public IActionResult GetShoppingCart()
        {
            var model = GetCart();
            return PartialView("_ShoppingCart", model);
        }

        [HttpPost]
        public async Task<IActionResult> AddToCart(long id, int quantity = 1, bool replace = false)
        {
            var result = await AddCart(id, quantity, replace);
            return JsonResult(result);
        }

        [HttpPost]
        public IActionResult RemoveOutCart(long id)
        {
            var result = RemoveCart(id);
            return JsonResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> GoToOrder(long id, int quantity = 1)
        {
            var result = new Result<bool>()
            {
                ResultType = ResultType.Success
            };
            var productInCart = GetCart();
            if (!productInCart.Any(t => t.ProductId == id))
            {
                await AddCart(id, quantity);
            }
            return JsonResult(result);
        }

        #endregion

        #region Favourite

        [HttpGet]
        public IActionResult GetFavourite()
        {
            var model = GetFavourites();
            return JsonResult(new Domain.Result.Result<int>
            {
                ResultType = Domain.Result.ResultType.Success,
                Value = model.Count
            });
            //return PartialView("_ShoppingCart", model);
        }

        [HttpPost]
        public async Task<IActionResult> AddToFavourite(long id)
        {
            var result = await AddFavourite(id);
            return JsonResult(result);
        }

        [HttpPost]
        public IActionResult RemoveOutFavourite(long id)
        {
            var result = RemoveFavourite(id);
            return JsonResult(result);
        }

        #endregion

        [HttpPost]
        public async Task<IActionResult> AddReview(ReviewModel model)
        {
            var result = new Domain.Result.Result<int>()
            {
                ResultType = Domain.Result.ResultType.Error
            };
            if (ModelState.IsValid)
            {
                result = await _reviewService.SaveAsync(model);
            }
            return JsonResult(result);
        }

        [HttpPost]
        public async Task<IActionResult> GetReviewList(long id)
        {
            var model = await _reviewService.GetListAsync(id);
            return PartialView("_ReviewList", model);
        }

        public async Task<IActionResult> GetRelatedProduct(int id)
        {
            var products = await _productService.GetAllAsync(id);
            return PartialView("_RelatedProductList", products);
        }
    }
}