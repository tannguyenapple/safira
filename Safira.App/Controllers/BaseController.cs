﻿using Microsoft.AspNetCore.Mvc;
using Safira.Domain.Result;

namespace Safira.App.Controllers
{
    public abstract class BaseController : Controller
    {
        public virtual JsonResult JsonResult<T>(Result<T> result)
        {
            switch (result.ResultType)
            {
                case ResultType.Error:
                    return JsonError(result.Message, result.Value);
                case ResultType.Success:
                    return JsonSuccess(result.Message, result.Value);
                case ResultType.Warning:
                    return JsonWarning(result.Message, result.Value);
            }
            return JsonError("Something wrong!");
        }
        public virtual JsonResult JsonError(string message, object data = null)
        {
            var obj = new
            {
                status = "error",
                message = message,
                value = data != null ? data : 0
            };
            return Json(obj);
        }
        public virtual JsonResult JsonSuccess(string message, object data = null)
        {
            var obj = new
            {
                status = "success",
                message = message,
                value = data != null ? data : 1
            };
            return Json(obj);
        }
        public virtual JsonResult JsonInfo(string message, object data = null)
        {
            var obj = new
            {
                status = "info",
                message = message,
                value = data
            };
            return Json(obj);
        }
        public virtual JsonResult JsonWarning(string message, object data = null)
        {
            var obj = new
            {
                status = "warning",
                message = message,
                value = data
            };
            return Json(obj);
        }
    }
}
