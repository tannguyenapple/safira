﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Safira.Domain.IServices;
using Safira.Domain.Models;

namespace Safira.App.Controllers
{
    public class FileController : Controller
    {
        #region Fields & Ctor

        private readonly IFileService _fileService;

        public FileController(IFileService fileService) => _fileService = fileService;

        #endregion

        #region Action Methods

        public async Task<FileResult> Get(long id)
        {
            var img = await _fileService.GetByIdAsync(id);
            return File(img.Binary, img.MimeType);
        }

        public async Task<IActionResult> Download(long id)
        {
            var img = await _fileService.GetByIdAsync(id);
            return File(new MemoryStream(img.Binary), img.MimeType, img.SeoFileName);
        }

        #endregion
    }
}
