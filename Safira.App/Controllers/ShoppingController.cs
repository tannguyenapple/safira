﻿using Microsoft.AspNetCore.Identity;
using Safira.Domain.Const;
using Safira.Domain.Helper;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Domain.Models.Product;
using Safira.Domain.Result;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Safira.App.Controllers
{
    public class ShoppingController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IProductService _productService;
        private readonly ICartService _cartService;

        public ShoppingController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IProductService productService,
            ICartService cartService)
        {
            _productService = productService;
            _cartService = cartService;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        protected List<ProductCartViewModel> GetCart()
        {
            var model = new List<ProductCartViewModel>();
            if (_signInManager.IsSignedIn(User))
            {
                var userId = _userManager.GetUserId(User);
                model = _cartService.GetCart(userId);
            }
            else
            {
                model = SessionHelper.GetObjectFromJson<List<ProductCartViewModel>>(HttpContext.Session, SessionKeys.CART);
            }
                
            if (model == null)
                model = new List<ProductCartViewModel>();

            return model;
        }

        protected async Task<Result<long>> AddCart(long id, int quantity = 1, bool replace = false)
        {
            var result = new Result<long>
            {
                Message = "Ok",
                ResultType = ResultType.Success,
                Value = id
            };

            if (_signInManager.IsSignedIn(User))
            {
                var userId = _userManager.GetUserId(User);
                result = _cartService.AddCart(userId, id, quantity, replace);
            }
            else
            {
                var productInCart = GetCart();
                if (productInCart.Any(t => t.ProductId == id))
                {
                    var product = productInCart.First(t => t.ProductId == id);
                    if (replace)
                        product.Quantity = quantity;
                    else
                        product.Quantity += quantity;
                }
                else
                {
                    var prd = await _productService.GetByIdAsync(id);
                    var product = new ProductCartViewModel
                    {
                        ProductId = prd.Id,
                        Name = prd.Name,
                        Price = prd.Price,
                        Quantity = quantity != 0 ? quantity : 1,
                        PictureId = prd.Files.FirstOrDefault().Id
                    };
                    productInCart.Add(product);
                }

                SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.CART, productInCart);
            }
            return new Result<long>
            {
                Message = "Ok",
                ResultType = ResultType.Success,
                Value = id
            };
        }
        
        protected Result<long> RemoveCart(long id)
        {
            var result = new Result<long>
            {
                ResultType = ResultType.Success,
                Value = id
            };

            if (_signInManager.IsSignedIn(User))
            {
                var userId = _userManager.GetUserId(User);
                result = _cartService.RemoveCart(userId, id);
            }
            else
            {
                var productInCart = GetCart();
                if (productInCart.Any(t => t.ProductId == id))
                {
                    var product = productInCart.First(t => t.ProductId == id);
                    productInCart.Remove(product);
                }

                SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.CART, productInCart);
            }
            return new Result<long>
            {
                ResultType = ResultType.Success,
                Value = id
            };
        }

        protected Result<long> ClearCart()
        {
            var result = new Result<long>
            {
                ResultType = ResultType.Success,
                Value = 0
            };

            SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.CART, new List<ProductCartViewModel>());
            return result;
        }

        protected List<ProductCartViewModel> GetFavourites()
        {
            var model = new List<ProductCartViewModel>();

            if (_signInManager.IsSignedIn(User))
            {
                var userId = _userManager.GetUserId(User);
                model = _cartService.GetWish(userId);
            }
            else
            {
                model = SessionHelper.GetObjectFromJson<List<ProductCartViewModel>>(HttpContext.Session, SessionKeys.FAVOURITE);
            }
            if (model == null)
                model = new List<ProductCartViewModel>();

            return model;
        }

        protected async Task<Result<int>> AddFavourite(long id)
        {
            if (_signInManager.IsSignedIn(User))
            {
                var userId = _userManager.GetUserId(User);
                _cartService.AddWish(userId, id);
            }
            else
            {
                var productInCart = GetFavourites();
                if (!productInCart.Any(t => t.ProductId == id))
                {
                    var prd = await _productService.GetByIdAsync(id);
                    var product = new ProductCartViewModel
                    {
                        ProductId = prd.Id,
                        Name = prd.Name,
                        Price = prd.Price,
                        PictureId = prd.Files.FirstOrDefault().Id
                    };
                    productInCart.Add(product);
                }

                SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.FAVOURITE, productInCart);
            }

            return new Result<int>
            {
                ResultType = ResultType.Success,
                Value = GetFavourites().Count
            };
        }

        protected Result<int> RemoveFavourite(long id)
        {
            if (_signInManager.IsSignedIn(User))
            {
                var userId = _userManager.GetUserId(User);
                _cartService.RemoveWish(userId, id);
            }
            else
            {
                var productInCart = GetFavourites();
                if (productInCart.Any(t => t.ProductId == id))
                {
                    var product = productInCart.First(t => t.ProductId == id);
                    productInCart.Remove(product);
                }

                SessionHelper.SetObjectAsJson(HttpContext.Session, SessionKeys.FAVOURITE, productInCart);
            }
            return new Result<int>
            {
                ResultType = ResultType.Success,
                Value = GetFavourites().Count
            };
        }

    }
}
