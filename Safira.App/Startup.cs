using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Safira.DI;
using Safira.Domain.Caching;
using Safira.Domain.Helper;
using Safira.Domain.IServices;
using Safira.Domain.Models;
using Safira.Service;
using System;

namespace Safira.App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ResolveIoc(Configuration);
            ConfigureIdentity(services, Configuration);

            services.Configure<ApplicationSettings>(Configuration.GetSection("AppSettings"));
            services.AddScoped(cfg => cfg.GetService<IOptionsSnapshot<ApplicationSettings>>().Value);
            services.Configure<Messages>(Configuration.GetSection("Messages"));
            services.AddScoped(cfg => cfg.GetService<IOptionsSnapshot<Messages>>().Value);
            services.AddScoped<IEmailSender, SendGridEmailService>(serviceProvider =>
            {
                return new SendGridEmailService(
                    Configuration.GetValue<string>("AppSettings:SendGrid_ApiKey"),
                    Configuration.GetValue<string>("AppSettings:Sender_Email_From"),
                    Configuration.GetValue<string>("AppSettings:Sender_Email_Display"),
                    Configuration.GetValue<string>("AppSettings:Default_CC_Emails"),
                    Configuration.GetValue<string>("AppSettings:Default_CC_Email_Displays")
                    );
            });

            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(Configuration.GetValue<double>("AppSettings:Login_Time_Out_Minute"));
                options.Cookie.HttpOnly = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(Configuration.GetValue<double>("AppSettings:Login_Time_Out_Minute"));

                options.LoginPath = "/admin/account/login";
                options.LogoutPath = "/admin/account/logout";
                options.SlidingExpiration = true;
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture(culture: "vi-VN", uiCulture: "vi-VN");
                options.RequestCultureProviders.Clear();
            });

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.AddMvc();

            services.AddSession();

            services.AddHttpContextAccessor();

            services.AddControllersWithViews();

            services.AddMemoryCache();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IWebHelper, WebHelper>();
            services.AddSingleton<ICacheManager, MemoryCacheManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();
            app.UseRequestLocalization();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "MyArea",
                    pattern: "{area:exists}/{controller=Order}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "trang-chu",
                    pattern: "trang-chu",
                    defaults: new { controller = "home", action = "index" });

                endpoints.MapControllerRoute(
                    name: "danh-muc",
                    pattern: "danh-muc/{id}/{name?}",
                    defaults: new { controller = "home", action = "category" });

                endpoints.MapControllerRoute(
                     name: "san-pham-chi-tiet",
                     pattern: "san-pham/{id}/{name}",
                     defaults: new { controller = "product", action = "index" });

                endpoints.MapControllerRoute(
                    name: "lien-he",
                    pattern: "lien-he",
                    defaults: new { controller = "home", action = "contactus" });

                endpoints.MapControllerRoute(
                     name: "cam-nang",
                     pattern: "cam-nang",
                     defaults: new { controller = "blog", action = "index" });

                endpoints.MapControllerRoute(
                     name: "cam-nang-chi-tiet",
                     pattern: "cam-nang/{id}/{name}",
                     defaults: new { controller = "blog", action = "detail" });

                endpoints.MapControllerRoute(
                    name: "login",
                    pattern: "dang-nhap",
                    defaults: new { controller = "account", action = "login" });

                endpoints.MapControllerRoute(
                    name: "register",
                    pattern: "dang-ky",
                    defaults: new { controller = "account", action = "register" });
            });



            app.MigrateService();
        }
        private void ConfigureIdentity(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            //services.ConfigureApplicationCookie(options =>
            //{
            //    options.Cookie.HttpOnly = true;
            //    options.ExpireTimeSpan = TimeSpan.FromMinutes(configuration.GetValue<double>("AppSettings:Login_Time_Out_Minute"));
            //    options.LoginPath = "/account/login";
            //    options.LogoutPath = "/account/logout";
            //});
        }

    }
}
