﻿namespace Safira.Domain.Enums
{
    public enum MenuEnum
    {
        Home = 1,
        Cuisine = 2,
        Course = 3,
        ContactUs = 4
    }
}
