﻿namespace Safira.Domain.Enums
{
    public enum ProductTypeEnum
    {
        Trend = 1,
        Deal = 2,
        BestSeller = 3,
        Normal = 4
    }
}
