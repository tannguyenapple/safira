﻿namespace Safira.Domain.Enums
{
    public enum StatusEnum
    {
        Active = 1,
        InActive = 2,
        Pending = 3,
        Delete = 4
    }
}
