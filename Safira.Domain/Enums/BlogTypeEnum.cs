﻿namespace Safira.Domain.Enums
{
    using System.ComponentModel.DataAnnotations;

    public enum BlogTypeEnum
    {
        [Display(Name = "Món ngon nhà làm")]
        MonNgonNhaLam = 1,
        [Display(Name = "Kinh nghiệm bếp núc")]
        KinhNghiemBepNuc = 2,
        [Display(Name = "Văn hóa ẩm thực")]
        VanHoaAmThuc = 3
    }
}
