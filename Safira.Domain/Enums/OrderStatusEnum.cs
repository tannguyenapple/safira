﻿namespace Safira.Domain.Enums
{
    using System.ComponentModel.DataAnnotations;

    public enum OrderStatusEnum
    {
        [Display(Name = "New")]
        New = 1,
        [Display(Name = "Ready to be delivered")]
        ReadyToBeDelivered = 2,
        [Display(Name = "Done payment")]
        DonePayment = 3,
        [Display(Name = "Cancelled")]
        Cancelled = 4,
        [Display(Name = "Refunded")]
        Refunded = 5
    }
}
