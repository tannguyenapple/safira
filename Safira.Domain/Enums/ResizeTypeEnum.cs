﻿namespace Safira.Domain.Enums
{
    public enum ResizeTypeEnum
    {
        LongestSide = 1,

        Width = 2,

        Height = 3
    }
}
