﻿namespace Safira.Domain.IServices
{
    using Safira.Domain.Models;
    using Safira.Domain.Result;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFileService
    {
        Task<FileModel> GetByIdAsync(long id);

        Task<Result<long>> SaveAsync(FileModel model);

        Task<Result<List<long>>> SaveAsync(List<FileModel> model);

        Task<Result<long>> DeleteAsync(long id);

        Task<string> GetUrlAsync(long id, int targetSize = 0);
    }
}
