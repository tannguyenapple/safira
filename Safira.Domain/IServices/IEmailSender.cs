﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Safira.Domain.IServices
{
    public interface IEmailSender
    {
        Task<EmailResponse> SendEmailAsync(string toEmail, string subject, string htmlMessage);
        Task<EmailResponse> SendEmailAsync(List<string> toEmails, string subject, string htmlMessage);
    }

    public class EmailResponse
    {
        public EmailResponse() { }
        public EmailResponse(bool isSuccessStatusCode, int statusCode)
        {
            IsSuccessStatusCode = isSuccessStatusCode;
            StatusCode = (HttpStatusCode)statusCode;
        }

        public EmailResponse(bool isSuccessStatusCode, int statusCode, string message)
        {
            IsSuccessStatusCode = isSuccessStatusCode;
            StatusCode = (HttpStatusCode)statusCode;
            Message = message;
        }
        public bool IsSuccessStatusCode { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }

        public int Total { get; set; }
        public int Count { get; set; }
    }

    public class EmailAddressModel
    {
        public EmailAddressModel(string email)
        {
            Email = email;
            DisplayName = string.Empty;
        }
        public EmailAddressModel(string email, string displayName)
        {
            Email = email;
            DisplayName = displayName;
        }

        public string Email { get; set; }
        public string DisplayName { get; set; }
    }
}
