﻿using Safira.Domain.Models.Order;
using Safira.Domain.PagedList;
using Safira.Domain.Result;
using System.Threading.Tasks;

namespace Safira.Domain.IServices
{
    public interface IOrderService
    {
        Task<IPagedList<CheckoutViewModel>> GetListAsync(OrderFilteringModel filtering);

        Task<CheckoutViewModel> GetByIdAsync(int id);

        Task<Result<int>> SaveOrder(CheckoutViewModel model);

        Task<Result<int>> DeleteAsync(int id);
    }
}
