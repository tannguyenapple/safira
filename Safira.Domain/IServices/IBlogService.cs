﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Safira.Domain.Models.Blog;
using Safira.Domain.PagedList;
using Safira.Domain.Result;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Safira.Domain.IServices
{
    public interface IBlogService
    {
        Task<List<SelectListItem>> GetSelectListAsync(string defaultVal = "");

        Task<IPagedList<BlogModel>> GetListAsync(BlogFilteringModel filtering, bool isAdmin = false);

        Task<List<BlogModel>> GetBriefAllAsync();

        Task<List<BlogModel>> GetAllAsync(int category = 0, string keyword = "");

        Task<List<BlogModel>> GetRecentAsync();

        Task<BlogModel> GetByIdAsync(long id);

        Task<Result<long>> SaveAsync(BlogModel model);

        Task<Result<long>> DeleteAsync(long id);
    }
}
