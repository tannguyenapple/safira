﻿namespace Safira.Domain.IServices
{
    using Safira.Domain.Models;
    using Safira.Domain.Models.Category;
    using Safira.Domain.Result;
    using System.Threading.Tasks;

    public interface IEmailService
    {
        Task<CategoryModel> GetByIdAsync(int id);

        Task<Result<int>> SaveAsync(EmailModel model);

        Task<Result<int>> DeleteAsync(int id);
    }
}
