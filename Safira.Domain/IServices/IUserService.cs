﻿using Safira.Domain.Models;
using Safira.Domain.Result;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Safira.Domain.IServices
{
    public interface IUserService
    {
        Task<Result<List<Claim>>> AuthenticateUserAsync(string userName, string password);

    }
}
