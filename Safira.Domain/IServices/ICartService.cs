﻿using Safira.Domain.Models.Product;
using Safira.Domain.Result;
using System.Collections.Generic;

namespace Safira.Domain.IServices
{
    public interface ICartService
    {
        List<ProductCartViewModel> GetCart(string userId);

        Result<long> AddCart(string userId, long productId, int quantity = 1, bool replace = false);

        Result<long> RemoveCart(string userId, long productId);

        List<ProductCartViewModel> GetWish(string userId);

        Result<long> AddWish(string userId, long productId);

        Result<long> RemoveWish(string userId, long productId);
    }
}
