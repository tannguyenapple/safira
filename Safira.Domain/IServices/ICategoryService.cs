﻿namespace Safira.Domain.IServices
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Safira.Domain.Models.Category;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICategoryService
    {
        Task<List<SelectListItem>> GetSelectListAsync(string defaultVal = "");

        Task<IPagedList<CategoryModel>> GetListAsync(CategoryFilteringModel filtering);

        Task<CategoryModel> GetByIdAsync(int id);

        Task<Result<int>> SaveAsync(CategoryModel model);

        Task<Result<int>> DeleteAsync(int id);
    }
}
