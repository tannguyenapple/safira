﻿namespace Safira.Domain.IServices
{
    using Safira.Domain.Models.Product;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IReviewService
    {
        Task<IPagedList<ReviewModel>> GetListAsync(ReviewFilteringModel filtering);

        Task<List<ReviewModel>> GetListAsync(long id);

        Task<ReviewModel> GetByIdAsync(int id);

        Task<Result<int>> SaveAsync(ReviewModel model);

        Task<Result<int>> DeleteAsync(int id);
    }
}
