﻿namespace Safira.Domain.IServices
{
    using Safira.Domain.Models.Blog;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IBlogReviewService
    {
        Task<IPagedList<BlogReviewModel>> GetListAsync(BlogReviewFilteringModel filtering);

        Task<List<BlogReviewModel>> GetListAsync(long id);

        Task<List<BlogReviewModel>> GetRecentListAsync();

        Task<BlogReviewModel> GetByIdAsync(int id);

        Task<Result<int>> SaveAsync(BlogReviewModel model);

        Task<Result<int>> DeleteAsync(int id);
    }
}
