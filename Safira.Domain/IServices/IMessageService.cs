﻿namespace Safira.Domain.IServices
{
    using Safira.Domain.Models.Home;
    using Safira.Domain.PagedList;
    using Safira.Domain.Result;
    using System.Threading.Tasks;

    public interface IMessageService
    {
        Task<IPagedList<MessageViewModel>> GetListAsync(MessageFilteringModel filtering);

        Task<MessageViewModel> GetByIdAsync(int id);

        Task<Result<int>> SaveAsync(MessageViewModel model);

        Task<Result<int>> DeleteAsync(int id);
    }
}
