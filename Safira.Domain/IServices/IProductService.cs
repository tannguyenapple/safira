﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Safira.Domain.Models.Product;
using Safira.Domain.PagedList;
using Safira.Domain.Result;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Safira.Domain.IServices
{
    public interface IProductService
    {
        Task<IPagedList<ProductModel>> GetListAsync(ProductFilteringModel filtering, bool isAdmin = false);

        Task<List<ProductModel>> GetBriefAllAsync();

        Task<List<ProductModel>> GetAllAsync(int category = 0, string keyword = "");

        Task<ProductModel> GetByIdAsync(long id);

        Task<Result<long>> SaveAsync(ProductModel model);

        Task<Result<long>> DeleteAsync(long id);
    }
}
