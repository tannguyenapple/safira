﻿using System;
using System.Text.RegularExpressions;

namespace Safira.Domain.Helper
{
    public static class CommonHelper
    {
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
    }
}
