﻿using Microsoft.AspNetCore.Mvc;

namespace Safira.Domain.Helper
{
    public interface IWebHelper
    {
        IUrlHelper GetUrlHelper();

        string GetRawUrl();

        string GetHost(bool useSsl = false);
    }
}
