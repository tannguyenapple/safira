﻿namespace Safira.Domain.Helper
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.Routing;
    using System.Linq;

    public class WebHelper : IWebHelper
    {
        #region Fields

        private readonly IUrlHelperFactory _urlHelperFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IActionContextAccessor _actionContextAccessor;

        #endregion

        #region Ctor

        public WebHelper(IUrlHelperFactory urlHelperFactory, IActionContextAccessor actionContextAccessor, IHttpContextAccessor httpContextAccessor)
        {
            _urlHelperFactory = urlHelperFactory;
            _actionContextAccessor = actionContextAccessor;
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Methods
        public IUrlHelper GetUrlHelper()
        {
            return _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
        }

        public string GetRawUrl()
        {
            //First try to get the raw target from request feature
            //note: value has not been UrlDecoded
            var request = _httpContextAccessor.HttpContext.Request;
            var rawUrl = request.HttpContext.Features.Get<IHttpRequestFeature>()?.RawTarget;

            //or compose raw URL manually
            if (string.IsNullOrEmpty(rawUrl))
                rawUrl = $"{request.PathBase}{request.Path}{request.QueryString}";

            return rawUrl;
        }

        public string GetHost(bool useSsl = false)
        {
            var result = string.Empty;

            var hostHeader = _httpContextAccessor.HttpContext.Request.Headers["Host"];
            if (!string.IsNullOrEmpty(hostHeader))
                result = "http://" + hostHeader.FirstOrDefault();

            if (!string.IsNullOrEmpty(result) && useSsl)
                result = result.Replace("http://", "https://");
            return result;
        }

        #endregion
    }
}
