﻿namespace Conto.AdminCenter.Domain.DataAnnotations
{
    using System;
    using System.ComponentModel.DataAnnotations;

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class DateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            string memberName = validationContext.DisplayName;

            switch (value)
            {
                case DateTime dateTime when dateTime == dateTime.Date:
                    return ValidationResult.Success;

                case DateTime dateTime:
                    string errorMessage = $"The field {memberName} should not contain time data.";
                    string[] memberNames = new[] { memberName };
                    return new ValidationResult(errorMessage, memberNames);

                default:
                    return new ValidationResult(
                        $"The field {memberName} should be a System.DateTime.",
                        memberNames: new[] { memberName });
            }
        }
    }
}
