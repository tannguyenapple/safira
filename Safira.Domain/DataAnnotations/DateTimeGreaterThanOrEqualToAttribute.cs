﻿namespace Conto.AdminCenter.Domain.DataAnnotations
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class DateTimeGreaterThanOrEqualToAttribute : ValidationAttribute
    {
        public DateTimeGreaterThanOrEqualToAttribute(string otherProperty)
        {
            OtherProperty = otherProperty ?? throw new ArgumentNullException(nameof(otherProperty));
        }

        public string OtherProperty { get; }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            switch (value)
            {
                case DateTime dateTime:
                    return IsDateTimeValid(dateTime, validationContext, OtherProperty);

                default:
                    return ValueShouldBeDateTime(validationContext.DisplayName);
            }
        }

        private static ValidationResult IsDateTimeValid(
            DateTime value,
            ValidationContext validationContext,
            string otherPropertyName)
        {
            object instance = validationContext.ObjectInstance;

            switch (validationContext.ObjectType.GetProperty(otherPropertyName))
            {
                case null:
                    return OtherPropertyNotFound(otherPropertyName);

                case PropertyInfo otherProperty
                when otherProperty.PropertyType != typeof(DateTime):
                    return NonDateTimeOtherPropertyType(otherPropertyName);

                case PropertyInfo otherProperty
                when ValueIsLessThanOtherProperty(value, otherProperty, instance):
                    return ValueShouldBeGreaterThanOrEqualToOtherProperty(validationContext.DisplayName, otherPropertyName);

                default:
                    return ValidationResult.Success;
            }
        }

        private static bool ValueIsLessThanOtherProperty(
            DateTime value, PropertyInfo otherProperty, object instance)
        {
            return value < (DateTime)otherProperty.GetValue(instance);
        }

        private static ValidationResult OtherPropertyNotFound(string otherPropertyName)
        {
            return new ValidationResult(
                $"Could not find a property with name '{otherPropertyName}'.",
                memberNames: new[] { otherPropertyName });
        }

        private static ValidationResult NonDateTimeOtherPropertyType(string otherPropertyName)
        {
            return new ValidationResult(
                $"The type of the property {otherPropertyName} should be {typeof(DateTime).FullName}.",
                memberNames: new[] { otherPropertyName });
        }

        private static ValidationResult ValueShouldBeGreaterThanOrEqualToOtherProperty(
            string memberName, string otherPropertyName)
        {
            return new ValidationResult(
                $"The field {memberName} should be greater than or equal to the field {otherPropertyName}.",
                memberNames: new[] { memberName });
        }

        private static ValidationResult ValueShouldBeDateTime(string memberName)
        {
            return new ValidationResult(
                $"The field {memberName} should be a System.DateTime.",
                memberNames: new[] { memberName });
        }
    }
}
