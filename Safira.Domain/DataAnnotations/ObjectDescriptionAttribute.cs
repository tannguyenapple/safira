﻿namespace Conto.AdminCenter.Domain.DataAnnotations
{
    using System;
    using System.ComponentModel.DataAnnotations;

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ObjectDescriptionAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            string memberName = validationContext.DisplayName;

            switch (value)
            {
                case string s when s.Length > 1000:
                    return new ValidationResult(
                        errorMessage: "Value is too long.",
                        memberNames: new[] { memberName });

                case string s when s.Length > 0 && string.IsNullOrWhiteSpace(s):
                    return new ValidationResult(
                        errorMessage: "Value is empty or consists only of white-space characters.",
                        memberNames: new[] { memberName });

                case string s:
                    for (int i = 0; i < s.Length; i++)
                    {
                        char c = s[i];
                        if (char.IsWhiteSpace(c))
                        {
                            if (c != ' ' && c != '\n' && c != '\r')
                            {
                                return new ValidationResult(
                                    errorMessage: "Value contains disallowed white-space character(s).",
                                    memberNames: new[] { memberName });
                            }
                        }
                    }

                    return ValidationResult.Success;

                default:
                    return value == null
                        ? new ValidationResult("Value is null.", new[] { memberName })
                        : new ValidationResult("Value is not a string", new[] { memberName });
            }
        }
    }
}
