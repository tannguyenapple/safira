﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Safira.Domain.Models
{
    public class EmailModel
    {
        public int Id { get; set; }

        public string Subject { get; set; }

        public string Content { get; set; }

        public string RecipientName { get; set; }

        public string RecipientEmail { get; set; }

        public DateTime CreatedDate { get; set; }

        public int OrderId { get; set; }

        public EmailStatusEnum StatusId { get; set; }
    }

    public enum EmailStatusEnum
    {
        New = 0,
        Sent = 1,
        Failed = 2
    }
}
