﻿namespace Safira.Domain.Models
{
    public class Messages
    {
        public string Username_Or_Password_Invalid { get; set; }
        public string Email_Have_Been_Exist { get; set; }
        public string Added_Image_Successful { get; set; }
        public string Updated_Image_Successful { get; set; }
        public string Deleted_Image_Successful { get; set; }
        public string Added_Category_Successful { get; set; }
        public string Updated_Category_Successful { get; set; }
        public string Deleted_Category_Successful { get; set; }
        public string Added_Blog_Successful { get; set; }
        public string Updated_Blog_Successful { get; set; }
        public string Deleted_Blog_Successful { get; set; }
        public string Added_Product_Successful { get; set; }
        public string Updated_Product_Successful { get; set; }
        public string Deleted_Product_Successful { get; set; }
        public string Process_Failed { get; set; }
        public string Order_Successul { get; set; }
        public string Order_Failed { get; set; }
        public string Deleted_Order_Successful { get; set; }
        public string Added_Message_Successful { get; set; }
        public string Updated_Message_Successful { get; set; }
        public string Deleted_Message_Successful { get; set; }
        public string Added_Review_Successful { get; set; }
        public string Updated_Review_Successful { get; set; }
        public string Deleted_Review_Successful { get; set; }
    }
}
