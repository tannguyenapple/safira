﻿namespace Safira.Domain.Models.Order
{
    public class OrderFilteringModel : SortingAndPagingModel
    {
        public int StatusId { get; set; }
    }
}
