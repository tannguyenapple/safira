﻿namespace Safira.Domain.Models.Order
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Safira.Domain.PagedList;
    using System.Collections.Generic;

    public class OrderListViewModel
    {
        public OrderListViewModel() { }

        public OrderListViewModel(IPagedList<CheckoutViewModel> item, OrderFilteringModel filtering)
        {
            Items = item;
            CurrentFiltering = filtering;
        }

        public IPagedList<CheckoutViewModel> Items { get; set; }

        public OrderFilteringModel CurrentFiltering { get; set; }

        public ICollection<SelectListItem> PageSizeOptions { get; set; }

        public ICollection<SelectListItem> Status { get; set; }

        public string TotalItems { get; set; }
    }
}
