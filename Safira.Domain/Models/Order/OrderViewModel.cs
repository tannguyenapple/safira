﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Safira.Domain.Models.Product;
using System.Collections.Generic;

namespace Safira.Domain.Models.Order
{
    public class OrderViewModel : CheckoutViewModel
    {
        public ICollection<SelectListItem> Status { get; set; }
        public ICollection<ProductModel> RawProducts { get; set; }
    }
}
