﻿using Safira.Domain.Enums;
using Safira.Domain.Models.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Safira.Domain.Models.Order
{
    public class CheckoutViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Vui lòng nhập địa chỉ email.")]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập số điện thoại liên hệ.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập địa chỉ nhận hàng.")]
        public string Address { get; set; }

        public string Note { get; set; }

        public string OrderNumber { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.Now;

        public OrderStatusEnum StatusId { get; set; }

        public List<ProductCartViewModel> Products { get; set; }
    }
}
