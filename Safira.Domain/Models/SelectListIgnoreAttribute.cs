﻿using System;

namespace Safira.Domain.Models
{
    [AttributeUsage(AttributeTargets.All, Inherited = false)]
    public class SelectListIgnoreAttribute : Attribute
    {
    }
}
