﻿namespace Safira.Domain.Models
{
    using System;
    using Microsoft.AspNetCore.Identity;

    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateById { get; set; }

        public DateTime? ModifyDate { get; set; }

        public string ModifyById { get; set; }

        public short? StatusId { get; set; }

        public int? PhoneCountryId { get; set; }

        public string Address { get; set; }
    }
}
