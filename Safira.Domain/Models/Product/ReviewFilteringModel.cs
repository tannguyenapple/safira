﻿namespace Safira.Domain.Models.Product
{
    public class ReviewFilteringModel : SortingAndPagingModel
    {
        public long ProductId { get; set; }
    }
}
