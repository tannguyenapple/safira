﻿namespace Safira.Domain.Models.Product
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System.Collections.Generic;
    public class ProductViewModel : ProductModel
    {
        public ICollection<SelectListItem> Status { get; set; }

        public ICollection<SelectListItem> Categories { get; set; }

        public ICollection<SelectListItem> Types { get; set; }
    }
}
