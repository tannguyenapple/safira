﻿namespace Safira.Domain.Models.Product
{
    public class ProductFilteringModel : SortingAndPagingModel
    {
        public int CategoryId { get; set; }
    }
}
