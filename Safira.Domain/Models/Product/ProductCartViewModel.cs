﻿namespace Safira.Domain.Models.Product
{
    public class ProductCartViewModel
    {
        public long ProductId { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public long PictureId { get; set; }

        public string ImageUrl
        {
            get
            {
                return $"/images/{PictureId:0000000}_0.jpeg";
            }
        }
    }
}
