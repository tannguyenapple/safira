﻿namespace Safira.Domain.Models.Product
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Safira.Domain.PagedList;
    using System.Collections.Generic;

    public class ProductListViewModel
    {
        public ProductListViewModel() { }

        public ProductListViewModel(IPagedList<ProductModel> item, ProductFilteringModel filtering)
        {
            Items = item;
            CurrentFiltering = filtering;
        }

        public IPagedList<ProductModel> Items { get; set; }

        public ProductFilteringModel CurrentFiltering { get; set; }

        public ICollection<SelectListItem> PageSizeOptions { get; set; }

        public ICollection<SelectListItem> Categories { get; set; }

        public string TotalItems { get; set; }
    }
}
