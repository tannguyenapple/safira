﻿namespace Safira.Domain.Models.Product
{
    using Safira.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ProductModel
    {
        public ProductModel()
        {
            Files = new List<FileModel>();
            Reviews = new List<ReviewModel>();
        }

        public long Id { get; set; }

        [Required(ErrorMessage = "Xin vui lòng nhập tên sản phẩm.")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Xin vui lòng giá bán sản phẩm.")]
        public int Price { get; set; }

        public int? OldPrice { get; set; }

        //[Required(ErrorMessage = "Xin vui lòng nhập thông tin mô tả sản phẩm.")]
        public string Description { get; set; }

        public string Processing { get; set; }

        public string Useful { get; set; }

        public StatusEnum StatusId { get; set; }

        public int CategoryId { get; set; }

        public ProductTypeEnum TypeId { get; set; }

        public DateTime? SaleUp { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public List<FileModel> Files { get; set; }

        public List<string> ImageUrls { get; set; }

        public List<ReviewModel> Reviews { get; set; }
    }
}
