﻿namespace Safira.Domain.Models.Home
{
    using System.ComponentModel.DataAnnotations;

    public class MessageViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên của bạn ")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập địa chỉ email")]
        [EmailAddress(ErrorMessage = "Địa chỉ email không hợp lệ")]
        public string Email { get; set; }

        public string Subject { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập lời nhắn")]
        public string Content { get; set; }
    }
}
