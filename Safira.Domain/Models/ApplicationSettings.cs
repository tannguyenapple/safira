﻿namespace Safira.Domain.Models
{
    using System.Collections.Generic;

    public class ApplicationSettings
    {
        public int DefaultPageSize { get; set; }

        public Dictionary<string, string> PageSizeOptions { get; set; }

        public string AdminEmails { get; set; }
    }
}
