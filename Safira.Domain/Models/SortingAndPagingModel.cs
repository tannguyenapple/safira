﻿namespace Safira.Domain.Models
{
    public class SortingAndPagingModel
    {
        public SortingAndPagingModel()
        {
            PageNumber = 1;
            PageSize = 10;
        }

        public string SearchKeyword { get; set; }

        public string SortDirection { get; set; }

        public string SortBy
        {
            get; set;
        }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }
    }
}
