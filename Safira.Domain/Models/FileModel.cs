﻿namespace Safira.Domain.Models
{
    public class FileModel
    {
        public long Id { get; set; }

        public byte[] Binary { get; set; }

        public string MimeType { get; set; }

        public string SeoFileName { get; set; }

        public string AltAttribute { get; set; }

        public string TitleAttribute { get; set; }
    }
}
