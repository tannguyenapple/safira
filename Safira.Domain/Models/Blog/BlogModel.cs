﻿namespace Safira.Domain.Models.Blog
{
    using Safira.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class BlogModel
    {
        public BlogModel()
        {
            Files = new List<FileModel>();
            Reviews = new List<BlogReviewModel>();
        }

        public long Id { get; set; }

        [Required(ErrorMessage = "Xin vui lòng nhập tên sản phẩm.")]
        public string Name { get; set; }

        public string TitleUrl { get; set; }

        //[Required(ErrorMessage = "Xin vui lòng nhập thông tin mô tả sản phẩm.")]
        public string Description { get; set; }

        public StatusEnum StatusId { get; set; }

        public short BlogType { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public List<FileModel> Files { get; set; }

        public List<string> ImageUrls { get; set; }

        public List<BlogReviewModel> Reviews { get; set; }
    }
}
