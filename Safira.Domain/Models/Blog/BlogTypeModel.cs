﻿namespace Safira.Domain.Models.Blog
{
    public class BlogTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
