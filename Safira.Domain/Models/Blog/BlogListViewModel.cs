﻿namespace Safira.Domain.Models.Blog
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Safira.Domain.PagedList;
    using System.Collections.Generic;

    public class BlogListViewModel
    {
        public BlogListViewModel() { }

        public BlogListViewModel(IPagedList<BlogModel> item, BlogFilteringModel filtering)
        {
            Items = item;
            CurrentFiltering = filtering;
        }

        public IPagedList<BlogModel> Items { get; set; }

        public BlogFilteringModel CurrentFiltering { get; set; }

        public ICollection<SelectListItem> PageSizeOptions { get; set; }


        public ICollection<SelectListItem> Categories { get; set; }

        public string TotalItems { get; set; }
    }
}
