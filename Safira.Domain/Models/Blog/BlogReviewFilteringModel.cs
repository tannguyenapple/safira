﻿namespace Safira.Domain.Models.Blog
{
    public class BlogReviewFilteringModel : SortingAndPagingModel
    {
        public long BlogId { get; set; }
    }
}
