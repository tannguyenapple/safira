﻿namespace Safira.Domain.Models.Blog
{
    public class BlogFilteringModel : SortingAndPagingModel
    {
        public int CategoryId { get; set; }
    }
}
