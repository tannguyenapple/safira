﻿namespace Safira.Domain.Models.Blog
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System.Collections.Generic;
    public class BlogViewModel : BlogModel
    {
        public ICollection<SelectListItem> Status { get; set; }

        public ICollection<SelectListItem> Categories { get; set; }

        public ICollection<SelectListItem> Orders { get; set; }
    }
}
