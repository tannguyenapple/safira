﻿namespace Safira.Domain.Models.Blog
{
    using Safira.Domain.Enums;
    using System;
    using System.ComponentModel.DataAnnotations;

    public class BlogReviewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên của bạn ")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập địa chỉ email")]
        [EmailAddress(ErrorMessage = "Địa chỉ email không hợp lệ")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập lời nhắn")]
        public string Content { get; set; }

        public short Rating { get; set; }

        public DateTime CreatedDate { get; set; }

        public StatusEnum Status { get; set; }

        public long BlogId { get; set; }
    }
}
