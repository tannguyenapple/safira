﻿namespace Safira.Domain.Models.Category
{
    using System.ComponentModel.DataAnnotations;

    public class CategoryModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Xin vui lòng nhập tên nhóm sản phẩm.")]
        public string Name { get; set; }

        public string Description { get; set; }

        public short StatusId { get; set; }

        public int ParentId { get; set; }

        public short Order { get; set; }
    }
}
