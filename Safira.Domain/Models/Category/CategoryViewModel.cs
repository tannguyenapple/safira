﻿namespace Safira.Domain.Models.Category
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using System.Collections.Generic;
    public class CategoryViewModel : CategoryModel
    {
        public ICollection<SelectListItem> Status { get; set; }

        public ICollection<SelectListItem> Categories { get; set; }

        public ICollection<SelectListItem> Orders { get; set; }
    }
}
