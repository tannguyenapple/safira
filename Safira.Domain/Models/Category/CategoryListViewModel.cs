﻿namespace Safira.Domain.Models.Category
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Safira.Domain.PagedList;
    using System.Collections.Generic;

    public class CategoryListViewModel
    {
        public CategoryListViewModel() { }

        public CategoryListViewModel(IPagedList<CategoryModel> item, CategoryFilteringModel filtering)
        {
            Items = item;
            CurrentFiltering = filtering;
        }

        public IPagedList<CategoryModel> Items { get; set; }

        public CategoryFilteringModel CurrentFiltering { get; set; }

        public ICollection<SelectListItem> PageSizeOptions { get; set; }

        public string TotalItems { get; set; }
    }
}
