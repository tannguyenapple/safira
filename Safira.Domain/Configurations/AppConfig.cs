﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Safira.Domain.Configurations
{
    public class AppConfig
    {
        public string API_HOST_URL { get; set; }

        public string Default_Time_Zone { get; set; }

        public string DateFormat { get; set; }

        public string DateTimeFormat { get; set; }

        public string Latest_Version { get; set; }
    }
}
