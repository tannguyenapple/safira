﻿namespace Safira.Domain.Extensions
{
    using Microsoft.AspNetCore.Http;
    using System.IO;

    public static  class ImageExtensions
    {
        public static byte[] GetDownloadBits(this IFormFile file)
        {
            using (var fileStream = file.OpenReadStream())
            {
                using (var ms = new MemoryStream())
                {
                    fileStream.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    return fileBytes;
                }
            }
        }

        public static byte[] GetImageBits(this IFormFile file)
        {
            return GetDownloadBits(file);
        }
    }
}
