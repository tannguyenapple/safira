﻿namespace Safira.Domain.Const
{
    public class MembershipRole
    {
        public const string Admin = "Admin";
        public const string Supplier = "Supplier";
        public const string Customer = "Customer";
    }
}
