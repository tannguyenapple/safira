﻿namespace Safira.Domain.Const
{
    public class SessionKeys
    {
        public const string ADMIN_CART = "MY_DOQUE_ADMIN_CART";
        public const string CART = "MY_DOQUE_CART";
        public const string FAVOURITE = "MY_DOQUE_FAVOURITE";
    }
    public class CacheKeys
    {
        public const string PRODUCTS_LIST = "PRODUCTS_LIST";

        public const string PICTURE_LIST = "PICTURE_LIST";

        public const string CATEGORIES_LIST = "CATEGORIES_LIST";

        public const string BLOG_TYPE_LIST = "BLOG_TYPE_LIST";

        public const string BLOG_LIST = "BLOB_LIST";

    }
}
