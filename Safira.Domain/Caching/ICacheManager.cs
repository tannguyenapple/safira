﻿namespace Safira.Domain.Caching
{
    using System;

    public interface ICacheManager : IDisposable
    {
        T Get<T>(string key, Func<T> acquire, int? cacheTime = null);

        void Set(string key, object data, int? cacheTime = null);

        bool IsSet(string key);

        void Remove(string key);

        void RemoveByPattern(string pattern);

        void Clear();
    }
}
