﻿namespace Safira.Domain.Result
{
    public enum ResultType
    {
        Error = -1,
        Warning,
        Success
    }
}
