﻿namespace Safira.Domain.Result
{
    public class Result<T>
    {
        public ResultType ResultType { get; set; }

        public T Value { get; set; }

        public string Message { get; set; }
    }
}
