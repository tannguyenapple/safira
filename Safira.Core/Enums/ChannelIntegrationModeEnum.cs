﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OMH.API.Core.Enums
{
    public enum ChannelIntegrationModeEnum
    {
        Pull,
        Push
    }
}
