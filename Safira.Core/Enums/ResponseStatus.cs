﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OMH.API.Core.Enums
{
    /// <summary>
    /// This presents Response status
    /// </summary>
    public enum ResponseStatus
    {
        SUCCESS,
        UNAUTHORIZED,
        MALFORMED_REQUEST,
        NO_AVAILABILITY,
        NO_ALLOTMENT,
        BOOKING_FAIL,
        CANCEL_FAIL,
        UNEXPECTED_ERROR,
        RATE_EXCEEDED,
        SUPPLIER_SERVER_ERROR,
        DuplicatedReservation,
        ReservationNotFound,
        DuplicateCancel
    }
}
