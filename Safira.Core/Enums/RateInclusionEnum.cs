﻿namespace OMH.API.Core.Enums
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public enum RateInclusion
    {
        Empty = 0,
        Breakfast = 1,
        Dinner = 2
    }
}
