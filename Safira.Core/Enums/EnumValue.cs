﻿namespace OMH.API.Core.Enums
{
    public class EnumValue : System.Attribute
    {
        public EnumValue(string value)
        {
            Value = value;
        }
        public string Value { get; }
    }
}
