﻿namespace OMH.API.Core.Enums
{
    public enum BookingResultEnum
    {
        Successful = 1,
        Failed = 2
    }
}
