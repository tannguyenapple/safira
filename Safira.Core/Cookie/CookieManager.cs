﻿namespace OMH.API.Core.Cookie
{
    using System;
    using System.Collections.Generic;
    using OMH.API.Core.Common;
    using OMH.API.Core.IUtilities;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;

    public class CookieManager : ICookieManager
    {
        #region Fields

        private static readonly string Purpose = "CookieManager.Token.v1";
        private static readonly int DefaultExpireTimeInDays = 1;
        private readonly HttpContext _httpContext;
        private readonly IDataProtector _dataProtector;

        #endregion

        #region Ctor

        public CookieManager(IHttpContextAccessor httpContextAccessor,  IDataProtectionProvider dataProtectionProvider)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _dataProtector = dataProtectionProvider.CreateProtector(Purpose);
        }

        #endregion

        #region Properties

        public ICollection<string> Keys
        {
            get
            {
                if (_httpContext == null)
                {
                    throw new ArgumentNullException(nameof(_httpContext));
                }

                return _httpContext.Request.Cookies.Keys;
            }
        }

        #endregion

        #region Methods

        public bool Contains(string key)
        {
            if (_httpContext == null)
            {
                throw new ArgumentNullException(nameof(_httpContext));
            }

            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            return _httpContext.Request.Cookies != null && _httpContext.Request.Cookies.ContainsKey(key);
        }

        public T GetOrSet<T>(string key, Func<T> acquirer, int? expireTime = default(int?))
        {
            if (Contains(key))
            {
                return Get<T>(key);
            }
            else
            {
                var value = acquirer();
                Set(key, value, expireTime);
                return value;
            }
        }

        public T GetOrSet<T>(string key, Func<T> acquirer, CookieOptions option)
        {
            if (Contains(key))
            {
                return Get<T>(key);
            }
            else
            {
                var value = acquirer();
                Set(key, value, option);
                return value;
            }
        }

        public T Get<T>(string key)
        {
            var value = Get(key);

            if (string.IsNullOrEmpty(value))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(value);
        }

        public void Set(string key, object value, int? expireTime = default(int?))
        {
            Set(key, JsonConvert.SerializeObject(value), null, expireTime);
        }

        public void Set(string key, object value, CookieOptions option)
        {
            Set(key, JsonConvert.SerializeObject(value), option, null);
        }

        public void Remove(string key)
        {
            // validate input
            if (_httpContext == null)
            {
                throw new ArgumentNullException(nameof(_httpContext));
            }

            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var value = _httpContext.Request.Cookies[key];
            _httpContext.Response.Cookies.Delete(key);
        }

        #endregion

        #region Utilities

        private string Get(string key)
        {
            if (_httpContext == null)
            {
                throw new ArgumentNullException(nameof(_httpContext));
            }

            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            if (Contains(key))
            {
                var encodedValue = _httpContext.Request.Cookies[key];
                var protectedData = string.Empty;

                // allow encryption is optional
                // may change the allow encryption to avoid this first check if cookie value is able to decode than unprotect tha data
                if (Base64TextEncoder.TryDecode(encodedValue, out protectedData))
                {
                    return _dataProtector.Unprotect(protectedData);
                }

                return encodedValue;
            }

            return string.Empty;
        }

        private void Set(string key, string value, CookieOptions option, int? expireTime)
        {
            if (option == null)
            {
                option = new CookieOptions();

                if (expireTime.HasValue)
                {
                    option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
                }
                else
                {
                    option.Expires = DateTime.Now.AddDays(DefaultExpireTimeInDays);
                }
            }

            var protecetedData = _dataProtector.Protect(value);
            var encodedValue = Base64TextEncoder.Encode(protecetedData);
            _httpContext.Response.Cookies.Append(key, encodedValue, option);
        }

        #endregion

    }
}
