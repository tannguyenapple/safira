﻿namespace OMH.API.Core.Common
{
    using Newtonsoft.Json;

    public partial class ErrorModel
    {
        /// <summary>
        /// gets or sets Error code
        /// </summary>
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        /// <summary>
        /// gets or sets error message
        /// </summary>
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// override Tostring
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
