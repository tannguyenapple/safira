﻿namespace OMH.API.Core.Common
{
    using System;
    using System.Security.Cryptography;

    public class Protect
    {
        public static string GenerateRandomSalt()
        {
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            byte[] saltInBytes = new byte[5];
            crypto.GetBytes(saltInBytes);

            return Convert.ToBase64String(saltInBytes);
        }
        
        public static object ToDataType(object pData, Type pType, object pDefaultValue)
        {
            return ToDataType(pData, pType, pDefaultValue, System.Globalization.CultureInfo.CurrentCulture);
        }

        public static object ToDataType(object pData, Type pType, object p_pDefaultValue, IFormatProvider pProvider)
        {
            if (pData == null)
                return p_pDefaultValue;
            try
            {
                return Convert.ChangeType(pData, pType, pProvider);
            }
            catch
            {
                return p_pDefaultValue;
            }
        }

        public static string ToString(object pData)
        {
            return ToDataType(pData, typeof(string), string.Empty).ToString();
        }

        public static int ToInt32(object pData)
        {
            return ToInt32(pData, -1);
        }

        public static int ToInt32(object pData, int pDefaultValue)
        {
            return (int)ToDataType(pData, typeof(int), pDefaultValue);
        }

        public static long ToInt64(object pData, long pDefaultValue)
        {
            return (long)ToDataType(pData, typeof(long), pDefaultValue);
        }

        public static bool ToBoolean(object pData, bool pDefaultValue)
        {
            return (bool)ToDataType(pData, typeof(bool), pDefaultValue);
        }

        public static double ToDouble(object pData, double pDefaultValue)
        {
            return Convert.ToDouble(string.Format("{0:0.00}", (double)ToDataType(pData, typeof(double), pDefaultValue)));
        }

        public static decimal ToDecimal(object pData, decimal pDefaultValue)
        {
            return (decimal)ToDataType(pData, typeof(decimal), pDefaultValue);
        }
        
        public static DateTime ToDateTime(object pData, DateTime pDefaultValue)
        {
            return (DateTime)ToDataType(pData, typeof(DateTime), pDefaultValue);
        }

        public static DateTime ToDateTime(DateTime pBaseDateTime, TimeSpan pOffset)
        {
            DateTime dt = pBaseDateTime;
            dt = dt.AddMilliseconds(pOffset.TotalMilliseconds);

            return dt;
        }


        public static byte ToByte(object pData, byte pDefaultValue)
        {
            return (byte)ToDataType(pData, typeof(byte), pDefaultValue);
        }

        
        private static readonly string[] VietnameseSigns = new string[]
        {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };

        public static string RemoveVNString(string str)
        {
            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi

            for (int i = 1; i < VietnameseSigns.Length; i++)
            {

                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

            }

            return str;
        }
        
    }
}
