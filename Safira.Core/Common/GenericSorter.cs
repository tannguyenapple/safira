﻿namespace OMH.API.Core.Common
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public static class GenericSorte
    {
        /// <summary>
        /// Generic sort 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="sortBy">columnName</param>
        /// <param name="sortDirection">acs | desc</param>
        /// <returns></returns>
        public static IQueryable<T> GenericSort<T>(this IQueryable<T> source, string sortBy, string sortDirection)
        {
            if (string.IsNullOrEmpty(sortDirection))
                sortDirection = "asc";

            if (!string.IsNullOrEmpty(sortBy))
            {
                var param = Expression.Parameter(typeof(T), "item");

                var sortExpression = Expression.Lambda<Func<T, object>>
                    (Expression.Convert(Expression.Property(param, sortBy), typeof(object)), param);

                switch (sortDirection.ToLower())
                {
                    case "asc":
                        return source.AsQueryable<T>().OrderBy<T, object>(sortExpression);
                    default:
                        return source.AsQueryable<T>().OrderByDescending<T, object>(sortExpression);

                }
            }
            else
            {
                return source;
            }
        }
    }
}

