﻿namespace OMH.API.Core.IUtilities
{
    using System;
    using Microsoft.AspNetCore.Http;

    public interface ICookieManager
    {
        T Get<T>(string key);

        T GetOrSet<T>(string key, Func<T> acquirer, int? expireTime = null);

        T GetOrSet<T>(string key, Func<T> acquirer, CookieOptions option);

        void Set(string key, object value, int? expireTime = null);

        void Set(string key, object value, CookieOptions option);

        bool Contains(string key);

        void Remove(string key);
    }
}
