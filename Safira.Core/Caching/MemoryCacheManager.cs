﻿namespace OMH.API.Core.Caching
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using OMH.API.Core.IUtilities;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Primitives;

    public class MemoryCacheManager : ICacheManager
    {
        #region Fields
        private readonly int _defaultCacheTime = 60;
        private readonly IMemoryCache _cache;
        protected static readonly ConcurrentDictionary<string, bool> _allKeys;
        private CancellationTokenSource _cancellationTokenSource;

        #endregion

        #region Ctor

        static MemoryCacheManager()
        {
            _allKeys = new ConcurrentDictionary<string, bool>();
        }

        public MemoryCacheManager(IMemoryCache cache)
        {
            _cache = cache;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        #endregion

        #region Methods

        public virtual T Get<T>(string key, Func<T> acquire, int? cacheTime = null)
        {
            if (_cache.TryGetValue(key, out T value))
            {
                return value;
            }

            var result = acquire();

            if ((cacheTime ?? _defaultCacheTime) > 0)
            {
                Set(key, result, cacheTime ?? _defaultCacheTime);
            }

            return result;
        }

        public virtual void Set(string key, object data, int? cacheTime = null)
        {
            if (data != null)
            {
                cacheTime = cacheTime ?? _defaultCacheTime;
                _cache.Set(AddKey(key), data, GetMemoryCacheEntryOptions(TimeSpan.FromMinutes(cacheTime.Value)));
            }
        }

        public virtual bool IsSet(string key)
        {
            return _cache.TryGetValue(key, out var _);
        }

        public virtual void Remove(string key)
        {
            _cache.Remove(RemoveKey(key));
        }

        public virtual void RemoveByPattern(string pattern)
        {
            // get cache keys that matches pattern
            var regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
            var matchesKeys = _allKeys.Where(p => p.Value).Select(p => p.Key).Where(key => regex.IsMatch(key)).ToList();

            // remove matching values
            foreach (var key in matchesKeys)
            {
                _cache.Remove(RemoveKey(key));
            }
        }

        public virtual void Clear()
        {
            _cancellationTokenSource.Cancel();

            _cancellationTokenSource.Dispose();

            _cancellationTokenSource = new CancellationTokenSource();
        }

        public virtual void Dispose()
        {
            // nothing special
        }

        #endregion

        #region Utilities

        protected MemoryCacheEntryOptions GetMemoryCacheEntryOptions(TimeSpan cacheTime)
        {
            var options = new MemoryCacheEntryOptions()
                .AddExpirationToken(new CancellationChangeToken(_cancellationTokenSource.Token))
                .RegisterPostEvictionCallback(PostEviction);

            // set cache time
            options.AbsoluteExpirationRelativeToNow = cacheTime;

            return options;
        }

        private void PostEviction(object key, object value, EvictionReason reason, object state)
        {
            // if cached item just change, then nothing doing
            if (reason == EvictionReason.Replaced)
            {
                return;
            }

            // try to remove all keys marked as not existing
            ClearKeys();

            // try to remove this key from dictionary
            TryRemoveKey(key.ToString());
        }

        private void ClearKeys()
        {
            foreach (var key in _allKeys.Where(p => !p.Value).Select(p => p.Key).ToList())
            {
                RemoveKey(key);
            }
        }

        protected string AddKey(string key)
        {
            _allKeys.TryAdd(key, true);
            return key;
        }

        protected string RemoveKey(string key)
        {
            TryRemoveKey(key);
            return key;
        }

        protected void TryRemoveKey(string key)
        {
            if (!_allKeys.TryRemove(key, out _))
            {
                _allKeys.TryUpdate(key, false, true);
            }
        }

        #endregion
    }
}
