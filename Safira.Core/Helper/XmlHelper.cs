﻿using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace OMH.API.Core.Helper
{
    public class XmlHelper
    {
        /// <summary>
        /// Deserialize string xml to object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlText"></param>
        /// <returns></returns>
        public static T DeserializeXml<T>(string xmlText)
        {
            using (var stringReader = new StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }

        public static T DeserializeXml<T>(Stream stream)
        {
            var serializer = new XmlSerializer(typeof(T));
            return (T)serializer.Deserialize(stream);
        }

        /// <summary>
        /// Serialize object to string xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeXml<T>(T obj)
        {
            using (var stringwriter = new Utf8StringWriter())
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, obj);
                return stringwriter.ToString();
            }
        }

        public static string SerializeXmlNonNamespace<T>(T obj)
        {
            using (var stringwriter = new Utf8StringWriter())
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add(string.Empty, string.Empty);
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, obj, ns);
                return stringwriter.ToString();
            }
        }
    }

    public class Utf8StringWriter: StringWriter
    {
        public Utf8StringWriter() { }
        public Utf8StringWriter(StringBuilder sb) : base(sb)
        {
        }
        public override Encoding Encoding => Encoding.UTF8;
    }
}
