﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace OMH.API.Core.Helper
{
    public static class HttpContextHelper
    {
        public static async Task<string> GetRequestContentAsync(HttpContext context)
        {
            var requestBodyValue = string.Empty;

            if (context.Request.Body.CanSeek)
            {
                context.Request.Body.Seek(0, SeekOrigin.Begin);
                var stream = new MemoryStream();
                await context.Request.Body.CopyToAsync(stream);
                stream.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(stream))
                {
                    requestBodyValue = reader.ReadToEnd();
                }
            }
            return requestBodyValue;
        }
    }
}
