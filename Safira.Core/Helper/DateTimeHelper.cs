﻿using System;
using System.Globalization;

namespace Utilities.Helper
{
    public static class DateTimeHelper
    {
        //private static Calendar cal = CultureInfo.InvariantCulture.Calendar;
        public static byte GetWeekOfYear(this DateTime date)
        {
            DateTimeFormatInfo i = DateTimeFormatInfo.CurrentInfo;
            return i == null ? (byte)0 : (byte)i.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /*
        public static byte? GetIso8601WeekOfYear(this DateTime time)
        {

            DayOfWeek day = cal.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return (byte)cal.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static byte GetIso8601WeekOfYear(this DateTime? time)
        {
            try
            {
                if (time == null)
                {
                    DateTime today = DateTime.Today;
                    return (byte)today.GetIso8601WeekOfYear();
                }
                DateTime timeNotNull = (DateTime)time;
                return (byte)timeNotNull.GetIso8601WeekOfYear();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int GetIso8601Year(this DateTime time)
        {

            DayOfWeek day = cal.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return time.Year;
        }
        */

        public static DateTime CombineDateWithNewTime(DateTime date, string time)
        {
            return date.Add(TimeSpan.Parse(time));
        }

        private const string SHORT_DATETIME_FORMAT = "dd/MM/yyyy";
        private const string LONG_DATETIME_FORMAT = "dd/MM/yyyy hh:mm:ss";
        private const string FULL_DATETIME_FORMAT = "dd/MM/yyyy hh:mm:ss tt";
        private const string TIME_FORMAT = "hh:mm:ss tt";

        public static string ToStringFromDateTime(this DateTime? pDate, string format)
        {
            try
            {
                if (pDate.HasValue)
                {
                    return pDate.Value.ToString(format);
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ToStringddMMM(this DateTime pDate)
        {
            try
            {
                var monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(pDate.Month);
                monthName = monthName.Substring(0, monthName.Length > 3 ? 3 : monthName.Length);
                return $"{pDate.Day} {monthName}";
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ToStringddd(this DateTime pDate)
        {
            try
            {
                return pDate.ToString("ddd", CultureInfo.CurrentCulture);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static DateTime? ToDateTime(this string pData, DateTime? pDefaultValue)
        {
            try
            {
                return DateTime.ParseExact(pData.ToString(), LONG_DATETIME_FORMAT, CultureInfo.CurrentCulture);
            }
            catch
            {
                return pDefaultValue;
            }
        }

        public static DateTime? ToDateTime(this string pData, string pFormat)
        {
            try
            {
                return DateTime.ParseExact(pData.ToString(), pFormat, CultureInfo.CurrentCulture);
            }
            catch
            {
                return null;
            }
        }

        public static DateTime ToDateTimeNotNull(this string pData, string pFormat)
        {
            try
            {
                return DateTime.ParseExact(pData.ToString(), pFormat, CultureInfo.CurrentCulture);
            }
            catch
            {
                throw new NotSupportedException("Could not convert to DateTime");
            }
        }

        public static DateTime ToDateTimeNotNull(this string pData)
        {
            try
            {
                return DateTime.Parse(pData);
            }
            catch
            {
                throw new NotSupportedException("Could not convert to DateTime");
            }
        }

        public static DateTime? ToDateTime(this string pData, string pFormat, DateTime? pDefaultValue)
        {
            try
            {
                return DateTime.ParseExact(pData.ToString(), pFormat, CultureInfo.CurrentCulture);
            }
            catch
            {
                return pDefaultValue;
            }
        }

        public static TimeSpan? ToTimeSpan(this string pData, string pFormat, TimeSpan? pDefaultValue)
        {
            try
            {
                var datetime = DateTime.ParseExact(pData.ToString(), pFormat, CultureInfo.CurrentCulture);
                return datetime.TimeOfDay;
            }
            catch
            {
                return pDefaultValue;
            }
        }

        public static TimeSpan? ToTimeSpan(this string pData, string pFormat)
        {
            try
            {
                var datetime = DateTime.ParseExact(pData.ToString(), pFormat, CultureInfo.CurrentCulture);
                return datetime.TimeOfDay;
            }
            catch
            {
                return null;
            }
        }

        public static string ToStringFromTimeSpan(this TimeSpan? pDate, string pFormat)
        {
            try
            {
                if (pDate.HasValue)
                {
                    var datetime = new DateTime(pDate.Value.Ticks); // Date part is 01-01-0001
                    return datetime.ToString(pFormat, CultureInfo.CurrentCulture);
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ToStringFromTimeSpan(this TimeSpan pDate, string pFormat)
        {
            try
            {
                var datetime = new DateTime(pDate.Ticks);
                return datetime.ToString(pFormat, CultureInfo.CurrentCulture);
                //return datetime.ToString(pFormat, CultureInfo.InvariantCulture);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ToStringFromTimeSpan(this TimeSpan? pDate)
        {
            try
            {
                if (pDate.HasValue)
                {
                    return pDate.Value.ToString(TIME_FORMAT);
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ToStringFromTimeSpan(this TimeSpan pDate)
        {
            try
            {
                return pDate.ToString(TIME_FORMAT);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Convert the passed datetime into client timezone.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToClientTime(this DateTime dt, double? offset)
        {
            if (offset.HasValue)
            {
                dt = dt.AddMinutes(-1 * offset.Value);

                return dt.ToString();
            }

            // if there is no offset in session return the datetime in server timezone
            return dt.ToLocalTime().ToString();
        }

        public static DateTime ToDateTimeZone(this DateTime pData, string timeZoneId)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            if (timeZone is null)
                return pData;
            else
                return TimeZoneInfo.ConvertTime(pData, timeZone);
        }

        public static DateTime ToDateTimeZoneFromUtc(this DateTime pData, string timeZoneId)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            if (timeZone is null)
                return pData;
            else
                return TimeZoneInfo.ConvertTimeFromUtc(pData, timeZone);
        }

        public static string ToStringDateTime(this DateTime pData, string format, CultureInfo culture = null)
        {
            if (culture is null)
                return pData.ToString(format);
            else
                return pData.ToString(format, culture);
        }

        public static string ToStringDefaultZero(this decimal pData, string format)
        {
            return pData.ToString(pData == 0 ? "0" : format);
        }
    }
}
