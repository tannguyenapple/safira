﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OMH.API.Core.Helper
{
    public class EnumHelper
    {
        public static T ParseEnum<T>(string value, T defaultValue = default(T))
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                return defaultValue;
            }
        }
        public static int ParseEnumToInt<T>(string value, int defaultValue = 0)
        {
            try
            {
                return (int)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
