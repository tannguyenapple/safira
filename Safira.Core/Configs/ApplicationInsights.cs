﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace OMH.API.Core.Configs
{
    public class ApplicationInsights
    {
        private readonly IConfiguration _configuration;

        public ApplicationInsights(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string InstrumentationKey
        {
            get
            {
                return _configuration.GetValue("ApplicationInsights:InstrumentationKey", "{input key}");
            }
        }

        public bool IsEnableRequestReponseAppInsight
        {
            get
            {
                return _configuration.GetValue("ApplicationInsights:IsEnableRequestReponseAppInsight", false);
            }
        }

        public bool IsLogSpecificPage
        {
            get
            {
                return _configuration.GetValue("ApplicationInsights:IsLogSpecificPage", true);
            }
        }

        public ICollection<string> LogAllRequestOnPage
        {
            get
            {
                return _configuration.GetSection("ApplicationInsights:LogAllRequestOnPage").Get<ICollection<string>>();
            }
        }
    }
}
