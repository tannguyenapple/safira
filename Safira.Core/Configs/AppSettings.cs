﻿namespace OMH.API.Core.Configs
{
    using System.Collections.Generic;

    public class AppSettings
    {
        public int DefaultPageSize { get; set; }

        public Dictionary<string, string> PageSizeOptions { get; set; }
    }
}
