﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using OMH.API.Core.Extensions;
using OMH.API.Core.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OMH.API.Core.Filters
{
    /// <summary>
    /// This filter will be used to log request, response, handle exception
    /// </summary>
    public class LogRequestResponseAttribute : TypeFilterAttribute
    {

        #region Ctor

        public LogRequestResponseAttribute() : base(typeof(LogRequestResponseFilter))
        {
        }

        #endregion

        #region Nested filter

        public class LogRequestResponseFilter : IAsyncActionFilter
        {
            #region Fields
            private readonly ILogger _logger;
            private readonly LogSettings _logSettings;
            private readonly IHttpContextAccessor _httpContextAccessor;
            #endregion

            #region Ctor

            public LogRequestResponseFilter(ILogger<LogRequestResponseFilter> logger,
                LogSettings logSettings,
                IHttpContextAccessor httpContextAccessor)
            {
                _logger = logger;
                _logSettings = logSettings;
                _httpContextAccessor = httpContextAccessor;
            }

            #endregion

            /// <summary>
            /// This Method is include before and after an request be processed by action
            /// </summary>
            /// <param name="context"></param>
            /// <param name="next"></param>
            /// <returns></returns>
            public async Task OnActionExecutionAsync(
                ActionExecutingContext context,
                ActionExecutionDelegate next)
            {
                bool enabledLog = false;
                if (_logSettings.LogRequestReponseType != LogRequestResponseTypeEnum.NoLog)
                {
                    var requestPath = string.Empty;
                    if (context.HttpContext.Request.Path.HasValue)
                        requestPath = context.HttpContext.Request.Path.Value;

                    if (IsLogRequestResponse(requestPath))
                    {
                        enabledLog = true;

                        var requestDataJson = string.Empty;
                        if (context.ActionArguments.Values != null)
                        {
                            if (context.HttpContext.Request.Method == "POST")
                            {
                                requestDataJson = context.ActionArguments.Values.ToJson();
                            }
                        }

                        var resultContext = await next();

                        var responseData = resultContext.Result as ObjectResult;

                        if (_logSettings.LogRequestReponseType == LogRequestResponseTypeEnum.NLog || _logSettings.LogRequestReponseType == LogRequestResponseTypeEnum.Both)
                        {
                            _logger.LogInformation($@"
//**URL**: {context.HttpContext.Request.Scheme}://{context.HttpContext.Request.Host}{context.HttpContext.Request.Path}{context.HttpContext.Request.QueryString}
//**REQUEST**: {requestDataJson}
//**RESPONSE**: {(responseData != null ? responseData.Value : resultContext.Exception).ToJson()}");
                        }

                        if (_logSettings.LogRequestReponseType == LogRequestResponseTypeEnum.AppInsight || _logSettings.LogRequestReponseType == LogRequestResponseTypeEnum.Both)
                        {
                            _httpContextAccessor.HttpContext.Items.Add(Constant.ApplicationInsights.REQUESTBODY, requestDataJson);
                            _httpContextAccessor.HttpContext.Items.Add(Constant.ApplicationInsights.RESPONSEBODY, responseData != null ? responseData.Value.ToJson() : resultContext.Exception.ToJson());
                        }
                    }
                }

                if (!enabledLog)
                {
                    await next();
                }
            }

            private bool IsLogRequestResponse(string pagePath)
            {
                return !_logSettings.IsLogSpecificPage || _logSettings.LogOnPages.Any(p => pagePath.EndsWith(p, StringComparison.OrdinalIgnoreCase));
            }
        }

        #endregion
    }
}