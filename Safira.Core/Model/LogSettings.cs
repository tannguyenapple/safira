﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace OMH.API.Core.Model
{
    public class LogSettings
    {
        private readonly IConfiguration _configuration;

        public LogSettings(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public LogRequestResponseTypeEnum LogRequestReponseType
        {
            get
            {
                return _configuration.GetValue("LogSettings:LogRequestResponseType", LogRequestResponseTypeEnum.NoLog);
            }
        }

        public bool IsLogSpecificPage
        {
            get
            {
                return _configuration.GetValue("LogSettings:IsLogSpecificPage", true);
            }
        }

        public ICollection<string> LogOnPages
        {
            get
            {
                return _configuration.GetSection("LogSettings:LogOnPages").Get<ICollection<string>>();
            }
        }
    }

    public enum LogRequestResponseTypeEnum
    {
        NoLog = 0,
        NLog = 1,
        AppInsight = 2,
        Both = 3
    }
}
