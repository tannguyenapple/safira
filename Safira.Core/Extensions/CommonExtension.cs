﻿using Newtonsoft.Json;
using OMH.API.Core.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Xml.Serialization;

namespace OMH.API.Core.Extensions
{
    public static class CommonExtension
    {
        public static string FormatTitleUrl(string title)
        {
            var unis = new Dictionary<char, List<char>>
            {
                { 'a', new List<char> { 'à', 'á', 'ả', 'ã', 'ạ', 'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ', 'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ'} },
                { 'd', new List<char> { 'đ' } },
                { 'e', new List<char> { 'è', 'é', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ' } },
                { 'i', new List<char> { 'ì', 'í', 'ỉ', 'ĩ', 'ị' } },
                { 'o', new List<char> { 'ò', 'ó', 'ỏ', 'õ', 'ọ', 'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ', 'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ' } },
                { 'u', new List<char> { 'ù', 'ú', 'ủ', 'ũ', 'ụ', 'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự' } },
                { 'y', new List<char>{ 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ' } }
            };
            var output = new StringBuilder();
            foreach (char c in title.ToLower().Replace(" ", "-"))
            {
                var replaced = false;
                foreach (var item in unis)
                {
                    if (item.Value.Any(t => t == c))
                    {
                        output.Append(item.Key);
                        replaced = true;
                        break;
                    }
                }
                if (!replaced)
                    output.Append(c);
            }
            return output.ToString();
        }

        public static string ToJson<T>(this T source)
        {
            return JsonConvert.SerializeObject(source);
        }

        public static T ToObject<T>(this string source)
        {
            return JsonConvert.DeserializeObject<T>(source);
        }

        public static string ToXml<T>(this T obj)
        {
            using (var stringwriter = new Utf8StringWriter())
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, obj);
                return stringwriter.ToString();
            }
        }

        /// <summary>
        /// Deserialize string xml to object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlText"></param>
        /// <returns></returns>
        public static T ToObjectFromXml<T>(this string xmlText)
        {
            using (var stringReader = new StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }

        public static void AddBasicAuthentication(this HttpClient httpClient, string userName, string password)
        {
            var account = $"{userName}:{password}";
            var encode = Encoding.UTF8.GetBytes(account);
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(encode));
        }
    }
}
