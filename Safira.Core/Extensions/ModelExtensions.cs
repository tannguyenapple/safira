﻿namespace OMH.API.Core.Extensions
{
    using System.Linq;

    public static class ModelExtensions
    {
        public static CHILD ShallowConvert<PARENT, CHILD>(this PARENT parent)
            where PARENT : class, new()
            where CHILD : class, new()
        {
            var child = new CHILD();
            var parentProperties = parent.GetType().GetProperties();
            foreach (var property in child.GetType().GetProperties())
            {
                var parentProperty = parentProperties.FirstOrDefault(t => t.Name == property.Name && t.PropertyType == property.PropertyType);
                if (property.CanWrite
                        && parentProperty != null)
                {
                    property.SetValue(child, parentProperty.GetValue(parent, null), null);
                }
            }

            return child;
        }
    }
}
