﻿namespace OMH.API.Core.Constant
{
    public class ClaimTypesEnum
    {
        public const string Token = "TOKEN";
        public const string HotelTenantId = "HOTEL_TENANT_ID";
        public const string HotelName = "HOTEL_NAME";
        public const string CurrencyName = "CURRENCY_NAME";
    }
}
