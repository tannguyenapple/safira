﻿namespace OMH.API.Core.Constant
{
    public static class ConstAuthenticationSchemes
    {
        public const string Basic = "Basic";
    }

    public static class Constant
    {
        public const string API_GATEWAY = "apigateway";

        public const string API_HOST = "apihost";

        public const string STATIC_CACHE_PROFILE = "StaticCache";
    }

    public static class EndPointConstant
    {
        public const string PUSH_STATIC_HOTEL = "push/static/hotel";

        public const string PUSH_STATIC_ROOM = "push/static/room-type";

        public const string PUSH_STATIC_RATEPLAN = "push/static/rate-plan";

        public const string NOTIFY_RATE_PLAN = "notify/rate-plan";

        public const string NOTIFY_RESERVATION_STATUS = "notify/reservation-status";

        public const string NOTIFY_HOTEL_CONFIRMATION_NUMBER = "notify/hotel-confirmation-number";

        public const string BOOKING_RESERVATION_CREATE = "reservation/book";

        public const string BOOKING_RESERVATION_SEARCH = "reservations";

        public const string BOOKING_RESERVATION_CANCEL = "reservation/cancel";

        public const string BOOKING_RESERVATION_PRECHECK = "reservation/precheck";
    }
}
