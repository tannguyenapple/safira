﻿namespace OMH.API.Core.Constant
{
    /// <summary>
    /// Defines constant parameters
    /// </summary>
    public static class ApplicationInsights
    {
        /// <summary>
        /// request body key store log in Application Insights
        /// </summary>
        public const string REQUESTBODY = "requestbody";

        /// <summary>
        /// client IP address key store log in Application Insights
        /// </summary>
        public const string CLIENTIP = "client-ip";

        /// <summary>
        /// Channel Code key store log in Application Insights
        /// </summary>
        public const string CHANNEL_CODE = "channelcode";

        /// <summary>
        /// response body key store log in Application Insights
        /// </summary>
        public const string RESPONSEBODY = "responsebody";

        public const string ERROR_FORMAT_RESPONSE = "error_format_response";

        /// <summary>
        /// Json content
        /// </summary>
        //public const string CONTENTTYPEJSON = "application/json";

        public const string REQUEST_TIMEOUT = "request_timeout";

        public const string RATE_LIMIT = "rate_limit";
    }
}
