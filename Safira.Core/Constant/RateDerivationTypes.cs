﻿namespace OMH.API.Core.Constant
{
    public class RateDerivationTypes
    {
        public const string KeepSame = "Keep Same";
        public const string Percentage = "Percentage";
        public const string Amount = "Amount";
        public const string AmountThenPercentage = "Amount then percentage";
        public const string PercentageThenAmount = "Percentage then amount";
    }
}
