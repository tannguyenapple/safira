﻿namespace OMH.API.Core.Constant
{
    public class RateDerivationChanges
    {
        public const string Increase = "Increase";
        public const string Decrease = "Decrease";
    }
}
